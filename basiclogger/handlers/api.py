import logging

from tornado import gen, web, auth
from tornado.escape import json_decode, json_encode
from tornado.options import options
from trombi import BulkError, Document, Database

from basiclogger import handlers
#from basiclogger.decorators import verify_basic_auth
from basiclogger.utils import encode_username


class BaseDocumentAPI(handlers.Browser, handlers.App):
    """Generic API request handler.

    Existing subclasses implement only document-specific logic.

    """
    def check_doc(self, doc, callback, doc_id=None):
        callback(None)

    @web.asynchronous
    def get(self, doc_type, doc_id):
        self.db.get(doc_id, self.doc_received)

    @web.asynchronous
    def delete(self, doc_type, doc_id, rev=None):
        """'rev' argument is not used in URLs"""
        def _delete_candidate_doc_received(response):
            if self._has_error(response):
                return
            self.db.delete({'_id': doc_id, '_rev': response.rev},
                           self.doc_deleted)

        # TODO: decide whether to require _rev from client.
        # check against the rev if it is present in request
        rev = self.get_argument('rev', rev)
        if rev:
            self.db.delete({'_id': doc_id, '_rev': rev},
                           self.doc_deleted)
        else:
            # we can't delete doc without providing a _rev
            self.db.get(doc_id, _delete_candidate_doc_received)

    @web.asynchronous
    @gen.engine
    def put(self, doc_type, doc_id):
        doc = json_decode(self.request.body)

        # get rev from the request (if client explicitly requests this check)
        rev = self.get_argument('rev', None)
        if rev:
            doc['_rev'] = rev
        else:
            rev = doc.get('_rev', None)
        if rev:
            result = yield gen.Task(self.db.get, doc_id)
            if self._has_error(result):
                return
            else:
                # ensure request has latest _rev for the document
                if rev != result.rev:
                    self.set_status(409)
                    self.finish()
                    return

        doc['type'] = doc_type
        result = yield gen.Task(self.check_doc, doc, doc_id=doc_id)
        if result:
            self.finish_with_error(result.args[0], result.args[1])
            return

        self.db.set(doc_id, doc, self.doc_updated)

    @web.asynchronous
    @gen.engine
    def post(self, doc_type):
        doc = json_decode(self.request.body)
        doc.update({'type': doc_type})
        result = yield gen.Task(self.check_doc, doc)
        if result:
            self.finish_with_error(result.args[0], result.args[1])
            return
        self.db.set(doc, self.doc_updated)


class ActivityAPI(BaseDocumentAPI):
    @web.asynchronous
    @gen.engine
    def delete(self, doc_type, doc_id):
        received_doc = yield gen.Task(self.db.get, doc_id)
        if self._has_error(received_doc):
            return
        # Get rev from the request (if client explicitly wants this check).
        rev = self.get_argument('rev', None)
        if rev and rev != received_doc.rev:
            self.set_status(409)
            self.finish()
            return

        # TODO: decide whether to require _rev from client.
        # TODO:
        # Do not compare rev now, as we want to return apropriate code
        # on wrong rev format (which is 400, not the 409).
        # 'db.get()' arguments are escaped in trombi - so no easy way.
        #if rev:
        #    received_doc.rev = rev
        #    # Try to update the document to ensure request has latest _rev.
        #    updated_doc = yield gen.Task(self.db.set, doc_id, received_doc)
        #    if self._has_error(updated_doc):
        #        return
        #    rev = updated_doc.rev

        # get all related sessions
        view_result = yield gen.Task(
            self.db.view, 'session', 'by_activity', key=doc_id)
        if self._view_has_error(view_result):
            return

        # delete related sessions
        docs = []
        for row in view_result:
            doc = {}
            doc['_id'], doc['_rev'] = row.pop('value')
            doc['_deleted'] = True
            docs.append(doc)
        if docs:
            bulk_result = yield gen.Task(
                self.db.bulk_docs, docs, all_or_nothing=True)
            if self._view_has_error(bulk_result):
                return

        # Delete the activity itself.
        # This request may fail with supplied revision due to race
        # condition caused by another parallel request.
        super(ActivityAPI, self).delete(doc_type,
                                                      doc_id, rev)


class SessionAPI(BaseDocumentAPI):
    doc_type = 'session'

    @gen.engine
    def check_doc(self, doc, callback, doc_id=None):
        """
        Check for conflicting sessions.

        Call callback with error message string and status code
        (400 on fundamental errors, 409 on session conflicts) on error, None
        on succcess.
        """
        all_overlapping = []

        if not 'start' in doc or not 'activity' in doc:
            msg = "Missing 'start' or 'activity' value."
            callback(msg, 400)
            return

        # check activity does exist
        act = yield gen.Task(self.db.get, doc['activity'])
        if not act or (act and act.error):
            msg = act.msg if act and act.msg else "Activity not found."
            callback(msg, 404)
            return

        # get already running session for this activity
        running = yield gen.Task(
            self.db.view, self.doc_type, 'by_activity__end_is_null',
            key=doc['activity'])
        if running.error:
            callback(running.msg, 400)
            return
        else:
            running = list(running)

        # try to detect fatal errors earlier
        if len(running) > 1:
            msg = ("Database contains multiple started sessions. "
                   "This should be fixed before saving any session with this "
                   "activity.")
            logging.warning(msg)
            callback(msg, 400)
            return

        if len(running) == 1:
            # check if we are adjusting currently running session
            if doc_id != running[0]['id']:
                if not doc.get('end', None):
                    msg = (
                        "Another session for this activity is already running"
                        ", id='{}'".format(running[0]['id']))
                    callback(msg, 409)
                    return
                elif running[0]['value']['start'] == doc['start']:
                    msg = ("Another session for this activity with the "
                           "same start time is currently running.")
                    callback(msg, 409)
                    return

        if 'end' in doc:  # stop/adjust session
            if not (isinstance(doc['start'], (int, long)) and
                    isinstance(doc['end'], (int, long)) and
                    doc['start'] >= 0 and
                    doc['end'] >= 0):
                msg = "Session 'start' and 'end' should be positive integers."
                # validate document raises 403 in the similar case
                callback(msg, 400)
                return
            elif doc['end'] <= doc['start']:
                msg = ("Session 'end' should be equal to or "
                       "greater than 'start'.")
                callback(msg, 400)
                return
            # get _all_ existing sessions from 'start' till 'end'
            all_overlapping = yield gen.Task(
                self.spatial_view, self.doc_type, 'time_range',
                bbox=(0, doc['start'], 0, doc['end']))
            if all_overlapping.error:
                callback(all_overlapping.msg, 400)
                return
        else:  # start (or move start of the) session
            if not (isinstance(doc['start'], (int, long)) and
                    doc['start'] >= 0):
                msg = "Session 'start' should be positive integer."
                # validate document raises 403 in the similar case
                callback(msg, 400)
                return
            # check 'start' is not inside _any_ existing session
            all_overlapping = yield gen.Task(
                self.spatial_view, self.doc_type, 'time_range',
                bbox=(0, doc['start'], 0, doc['start']))
            if all_overlapping.error:
                callback(all_overlapping.msg, 400)
                return

        # Check for conflicting sessions of the same activity:
        #   'all_overlapping' - arbitrary number of _geospatial_ view rows.
        for r in all_overlapping:
            if (r['value'] == doc['activity'] and
                    doc_id != r['id']):
                msg = ("Another session for this activity within the same "
                       "timeframe exists, id='{}'".format(r['id']))
                callback(msg, 409)
                return

        # success
        callback(None)


class AccountAPI(handlers.Browser, handlers.App):
    @web.asynchronous
    def get(self):
        def _got_info(result):
            if result.error:
                self.set_status(result.errno)
                msg = result.msg
            else:
                msg = 'OK'
            self.write({'status': msg})
            self.finish()

        self.db.info(_got_info)

    def _on_delete(self, result):
        if not result:
            self.set_status(400)
            msg = 'Unable to delete account.'
            logging.warn(msg)
            self.write({'error': msg})
        else:
            self.write({'deleted': 'OK'})
        self.finish()

    @web.asynchronous
    #@verify_basic_auth
    def delete(self):
        def _on_basic_auth(result):
            if result.error:
                self.set_status(401)
                msg = 'Unable to verify BasicAuth credentials.'
                logging.warn(msg + ' ' +  result.msg)
                self.write({'error': msg})
                self.finish()
            else:
                self.application.delete_account(
                    self.basic_auth['username'], self._on_delete,
                    ignore_errors=ignore_errors)

        ignore_errors = False
        if self.get_argument('ignore_errors', None) == 'true':
            ignore_errors = True

        if self.current_user:
            # if user is authenticated through session just delete his account
            self.application.delete_account(
                self.current_user, self._on_delete, ignore_errors=ignore_errors)
        else:
            # try to get user's db with his BasicAuth credentials
            # (to check if they are OK) and delete his account
            self.db.get('', _on_basic_auth)


class PrivilegedAccountAPI(AccountAPI):
    @web.asynchronous
    def get(self):
        raise web.HTTPError(405)

    @web.asynchronous
    def delete(self):
        account = self.get_argument('account', None)
        if not account:
            self.set_status(400)
            msg = 'No account given.'
            logging.warn(msg)
            self.write({'error': msg})
            self.finish()
            return

        ignore_errors = False
        if self.get_argument('ignore_errors', None) == 'true':
            ignore_errors = True

        if (options.couchdb_admin_user ==
                self.basic_auth.get('username', None) and
            options.couchdb_admin_password ==
                self.basic_auth.get('password', None)):
            self.application.delete_account(
                account, self._on_delete, ignore_errors=ignore_errors)
            return

        self.set_status(401)
        msg = 'Wrong privileged user credentials.'
        logging.warn(msg)
        self.write({'error': msg})
        self.finish()


class SettingsAPI(handlers.Browser, handlers.App):
    @gen.coroutine
    @web.asynchronous
    def get(self):
        response = yield gen.Task(self.db.get, 'settings')
        if not response:
            settings = {}
        else:
            settings = response.raw()
        self.set_status(200)
        self.write(settings)
        self.finish()

    @web.asynchronous
    def put(self):
        settings = json_decode(self.request.body)
        rev = self.get_argument('rev', None)
        if rev:
            settings['_rev'] = rev
        settings['type'] = 'settings'
        self.db.set('settings', settings, self.doc_updated)


class BaseBulkAPI(handlers.Browser, handlers.App):
    """Base bulk API handler class, should be subclassed"""
    def prepare(self):
        viewkwargs = {}

        try:
            limit = int(self.get_argument('limit', self.limit))
            if limit <= 0:
                raise ValueError
        except ValueError:
            self.set_status(400)
            self.write(
                {'error': 'Invalid value for positive '
                    'integer parameter: "{}"'.format(
                        self.get_argument('limit'))})
            self.finish()
            return

        if self.get_argument('descending', None) == 'true':
            viewkwargs['descending'] = True
        if self.get_argument('include_docs', None) == 'true':
            viewkwargs['include_docs'] = True
        else:
            viewkwargs['include_docs'] = False
        if limit >= self.limit:
            viewkwargs['limit'] = self.limit
        else:
            viewkwargs['limit'] = limit
        startkey = self.get_argument('startkey', None)
        if startkey:
            viewkwargs['startkey'] = startkey
        endkey = self.get_argument('endkey', None)
        if endkey:
            viewkwargs['endkey'] = endkey
        self.viewkwargs = viewkwargs
        super(BaseBulkAPI, self).prepare()

    #@web.asynchronous
    #def get(self, doc_type):
    #    view = 'all'
    #    self.db.view(doc_type, view, self._view_result_received,
    #                 **self.viewkwargs)

    # TODO: not implemented in subclasses yet, too many side-effects
    def post(self, doc_type):
        raise NotImplementedError

    #@web.asynchronous
    #def post(self, doc_type):
    #    docs = json_decode(self.request.body)
    #    for doc in docs['docs']:
    #        doc['type'] = doc_type
    #    self.db.bulk_docs(docs['docs'], self._bulk_updated)

    def _view_result_received(self, response):
        if self._view_has_error(response):
            return

        bulk_data = dict(rows=[], offset=response.offset,
                         total_rows=response.total_rows)
        for row in response:
            row.pop('key')
            row['id'], row['rev'] = row.pop('value')
            if 'doc' in row:
                row['doc'] = row.pop('doc').raw()
            bulk_data['rows'].append(row)
        self.write(bulk_data)
        self.finish()

    #def _bulk_updated(self, response):
    #    if self._view_has_error(response):
    #        return
    #    bulk_data = dict(rows=[])
    #    for row in response:
    #        if isinstance(row, BulkError):
    #            bulk_data['rows'].append(row.raw)
    #        else:
    #            bulk_data['rows'].append(dict(id=row['id'], rev=row['rev']))
    #    self.write(bulk_data)
    #    self.finish()


class SessionBulkAPI(BaseBulkAPI):
    @web.asynchronous
    def get(self, doc_type):
        view = 'all'

        activity_id = self.get_argument('activity', None)
        if activity_id:
            view = 'by_activity'
            self.viewkwargs['key'] = activity_id
            self.viewkwargs.pop('startkey', None)
            self.viewkwargs.pop('endkey', None)

        self.db.view(doc_type, view, self._view_result_received,
                     **self.viewkwargs)


class ActivityBulkAPI(BaseBulkAPI):
    @web.asynchronous
    @gen.engine
    def get(self, doc_type):
        tags_str = self.get_argument('tags', None)
        if not tags_str:
            self.db.view(doc_type, 'all', self._view_result_received,
                         **self.viewkwargs)
            return

        if self.get_argument('any_tag', None) == 'true':
            any_tag = True
        else:
            any_tag = False
        self.viewkwargs.pop('descending', None)
        self.viewkwargs.pop('limit', None)
        self.viewkwargs.pop('startkey', None)
        self.viewkwargs.pop('endkey', None)
        view_result = yield gen.Task(self.db.view, doc_type, 'by_tag',
                                     **self.viewkwargs)
        if self._view_has_error(view_result):
            return

        tags = set(tags_str.split(','))
        bulk_data = dict(rows=[], offset=0, total_rows=0)

        for row in view_result:
            doc_tags = set(row.pop('key'))
            row['rev'] = row.pop('value')[1]
            if ((any_tag and set(doc_tags).intersection(tags)) or
                (not any_tag and doc_tags >= tags)):
                if 'doc' in row:
                    row['doc'] = row.pop('doc').raw()
                bulk_data['rows'].append(row)

        bulk_data['total_rows'] = len(bulk_data['rows'])
        self.write(bulk_data)
        self.finish()


class TagBulkAPI(BaseBulkAPI):
    @web.asynchronous
    @gen.engine
    def get(self, doc_type):
        view = 'all'
        view_result = yield gen.Task(self.db.view, doc_type, view, group=True,
                                     **self.viewkwargs)
        # the reponse does not have any docs, so we do not use
        # self._view_result_received() but construct the response here:
        if self._view_has_error(view_result):
            return
        bulk_data = dict(rows=[], offset=view_result.offset,
                         total_rows=view_result.total_rows)
        for row in view_result:
            tag = row.pop('key')
            count = row.pop('value')
            bulk_data['rows'].append(dict(tag=tag, count=count))
        self.write(bulk_data)
        self.finish()


class ChangesAPI(handlers.Browser, handlers.App):
    @web.asynchronous
    @gen.engine
    def get(self, doc_type=None):
        try:
            since = int(self.get_argument('since', 0))
            limit = int(self.get_argument('limit', self.limit))
            if since < 0 or limit < 0:
                raise ValueError
        except ValueError:
            self.set_status(400)
            self.write({'error': 'Invalid value for positive integer '
                                 'parameter.'})
            self.finish()
            return
        if limit >= self.limit:
            limit = self.limit
        include_docs = self.get_argument('include_docs', None)
        # trombi translates True to 'True', which coucndb handles as false
        if include_docs != 'true':
            include_docs = False
        if doc_type:
            filter_func = 'userdb/{}'.format(doc_type)
        else:
            filter_func = ''
        changes = yield gen.Task(self.db.changes, filter=filter_func,
                                 since=since, limit=limit,
                                 include_docs=include_docs)
        if changes.error:
            self.set_status(500)
            self.finish()
            return
        self.write(changes.content)
        self.finish()
