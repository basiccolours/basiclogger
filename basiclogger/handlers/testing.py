from tornado import web, gen
from tornado.escape import json_encode

from basiclogger.decorators import api_authenticated
from basiclogger.handlers import Browser, App


class APITest(Browser, App):
    """Test page to load some activities and add/edit em with jQuery.ajax()"""
    @web.authenticated
    @web.asynchronous
    @gen.engine
    def get(self):
        response = yield gen.Task(self.db.view, 'activity', 'all',
                                  include_docs=True)
        if not response.error:
            activities = [json_encode(row['doc'].raw()) for row in response]
        else:
            activities = []
        self.render("test.html", title="test page",
                    activities=activities)


class CORSTest(Browser):
    def prepare(self):
        if 'origin' in self.request.headers:
            self.set_header('Access-Control-Allow-Origin',
                            self.request.headers['origin'])
            self.set_header('Access-Control-Allow-Methods',
                            'GET, POST, PUT, OPTIONS')
            self.set_header('Access-Control-Allow-Headers', 'content-type')
            self.set_header('Access-Control-Allow-Credentials', 'true')
        super(CORSTest, self).prepare()

    def compute_etag(self):
        return

    def options(self):
        return

    def get(self):
        self.write({'status': 'OK'})

    @api_authenticated
    def post(self):
        self.set_status(201)
        self.write({'status': 'OK'})

    @api_authenticated
    def put(self):
        self.set_status(201)
        self.write({'status': 'OK'})
