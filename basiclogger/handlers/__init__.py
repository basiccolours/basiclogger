import base64
import logging
import pdb
import signal

import trombi
from trombi.client import _error_response, _jsonize_params
from raven.contrib.tornado import SentryMixin

from tornado import web
from tornado.escape import json_decode
from tornado.options import options

from basiclogger import SpatialViewResult
from basiclogger.utils import setup_trombi_server, dbname_for_username


def quit_handler(signum, frame):
    if options.pdb:
        pdb.set_trace()

signal.signal(signal.SIGQUIT, quit_handler)


class Base(SentryMixin, web.RequestHandler):
    def _handle_request_exception(self, e):
        web.RequestHandler._handle_request_exception(self, e)
        if options.pdb:
            pdb.post_mortem()

    def set_cookie(self, *args, **kwargs):
        # this prevents xsrf cookies being read by js and send in AJAX
        #kwargs.setdefault('HttpOnly', True)
        if not options.debug:
            kwargs.setdefault('secure', True)
        super(Base, self).set_cookie(*args, **kwargs)


class Browser(Base):
    def check_xsrf_cookie(self):
        if self.application.settings['debug']:
            return
        return super(Browser, self).check_xsrf_cookie()

    def get_current_user(self):
        """Get username from signed cookie value.

        The cookie should be set in one of the previous requests by login
        handler.

        """
        username = self.get_secure_cookie("email")
        # we may try other auth methods here

        # return any username we have in the end
        return username


class App(Base):
    def initialize(self):
        self.limit = 1000
        self.time_max = 99999999999999

    def prepare(self):
        """
        Set self.db.

        Try to get self.current_user from cookie set by any previous call first
        or proxy HTTP basic auth credentials to CouchDB if that fails.
        Every application request must be authenticated in one way or another.

        """
        self.basic_auth = {}
        self.basic_auth['username'] = self.basic_auth['password'] = None

        if self.current_user:
            userdb_name = dbname_for_username(self.current_user)
            self.db = trombi.Database(self.application.server, userdb_name)
            return

        auth = self.request.headers.get('Authorization')
        if auth is None or not auth.startswith('Basic '):
            logging.debug('No basic auth credentials')
            self.set_status(403)
            self.write({'error': 'This API call requires authentification.'})
            self.finish()
            return

        logging.debug('Authorization: {}'.format(auth))
        auth_decoded = base64.decodestring(auth[6:])
        self.basic_auth['username'] = auth_decoded.split(':', 1)[0]
        self.basic_auth['password'] = auth_decoded.split(':', 1)[1]
        userdb_name = dbname_for_username(self.basic_auth['username'])
        user_server = setup_trombi_server(self.basic_auth['username'],
                                          self.basic_auth['password'])
        self.db = trombi.Database(user_server, userdb_name)

    def compute_etag(self):
        return

    # TODO: make this more generic, subclass trombi.Database and move there
    def spatial_view(self, design_doc, viewname, callback, **kwargs):
        def _really_callback(response):
            if response.code == 200:
                body = response.body.decode('utf-8')
                callback(SpatialViewResult(json_decode(body), db=self.db))
            else:
                callback(_error_response(response))

        url = '_design/{}/_spatial/{}'.format(design_doc, viewname)
        kwargs_sep = '?'
        if 'bbox' in kwargs:
            bbox = kwargs.pop('bbox', None)
            if bbox:
                # we'll fail here on non-iterable or non-indexable type
                if not isinstance(bbox, basestring) and len(bbox) == 4:
                    # special case replace() for numbers in exponent form
                    bbox = "{},{},{},{}".format(*bbox).replace('+', '%2B')
            url = '{}?bbox={}'.format(url, bbox)
            kwargs_sep = '&'
        if kwargs:
            url = '{}{}{}'.format(url, kwargs_sep, _jsonize_params(kwargs))
        return self.db._fetch(url, _really_callback)

    # helper methods
    def finish_with_error(self, msg='Generic error', status=400):
        self.set_status(status)
        logging.warn(msg)
        self.write({'error': msg})
        self.finish()

    def _has_error(self, response):
        if response:
            if response.error:
                #logging.warn('{} {}'.format(response.errno, response.msg))
                logging.warn(response.msg)
                self.set_status(response.errno)
                self.write({'error': response.msg})
                self.finish()
                return True
        else:
            self.set_status(404)
            self.finish()
            return True

    def _view_has_error(self, response):
        """Custom check here because bulk operations can return empty
        results"""
        if response.error:
            logging.warn(response.msg)
            self.set_status(response.errno)
            self.write({'error': response.msg})
            self.finish()
            return True

    # common callbacks
    def doc_updated(self, response):
        if self._has_error(response):
            return
        self.set_status(201)
        self.write({'id': response.id, 'rev': response.rev})
        self.finish()

    def doc_received(self, response):
        if self._has_error(response):
            return
        self.set_status(200)
        self.write(response.raw())
        self.finish()

    def doc_deleted(self, response):
        if self._has_error(response):
            return
        self.set_status(200)
        # trombi returns db as response
        # seems to be useless
        #self.write(response.raw())
        self.finish()
