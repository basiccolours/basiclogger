import logging
import sys
from datetime import datetime, timedelta

from tornado import gen, web, auth
from tornado.escape import json_encode, json_decode
from tornado.options import options

from basiclogger.handlers import (
    Browser, App, Base)
from basiclogger.utils import (
    send_email, encode_username, is_valid_internal_username, epoch_time,
    generate_passwd, is_valid_email, dbname_for_username,
    construct_account_data, setup_trombi_server)


class NotFound(web.RequestHandler):
    def check_xsrf_cookie(self):
        return

    def get(self):
        self.set_status(404)

    def post(self):
        self.set_status(404)

    def put(self):
        self.set_status(404)

    def head(self):
        self.set_status(404)

    def options(self):
        self.set_status(404)


class Index(Browser):
    def get(self):
        self.render("index.html")


class UI(Browser):
    def get(self):
        self.render("ui.html")


class AccountSignup(Browser):
    def initialize(self):
        self.template = "signup.html"

    def get(self):
        self.render(self.template, msg=None)

    @web.asynchronous
    @gen.engine
    def post(self):
        msg = None
        email = self.get_argument("email")
        username = encode_username(email)
        if not (is_valid_email(email)
                and is_valid_internal_username(username)):
            self.set_status(400)
            self.render(self.template, msg="Invalid username or Email format!")
            return
        response = yield gen.Task(self._send_activation_email, email)
        if response and not response.error:
            if options.debug:
                # redirect directly to valid activation url in debug mode
                self.redirect(self._construct_activation_url(email))
            else:
                self.redirect(self.reverse_url('activate'))
        else:
            self.set_status(400)
            self.render(self.template, msg="Error sending activation Email!")

    @gen.engine
    def _send_activation_email(self, email, callback, **kwargs):
        url = self._construct_activation_url(email, **kwargs)
        response = yield gen.Task(
            send_email,
            subject="Basic Logger account activation",
            message="Activate during one week, please: " + url,
            html_message="Activate during one week, please: " +
                         '<a href="{0}">{0}</a>'.format(url),
            recipient_list=[{'email': email}])
        callback(response)

    def _construct_activation_url(self, email, **kwargs):
        signed_email = self.create_signed_value('signed_email', email)
        url = (self.request.protocol + '://' +
               self.request.host +
               self.reverse_url('activate') +
               '?signed_email=' + signed_email)
        data = self.get_data()
        if data:
            url += '&signed_data=' + self.create_signed_value(
                'signed_data', json_encode(data))
        for k, v in kwargs.items():
            url += '&{0}={1}'.format(k, v)
        return url

    def get_data(self):
        """
        Subclass this method to add extra data to account dict.

        The 'password' key is treated specailly and is popped() and used as the
        actual user password before updating app-specific json info.
        """
        password = self.get_argument("password", None)
        return dict(password=password) if password else dict()


class PasswordReset(AccountSignup):
    def initialize(self):
        self.template = "password_reset.html"

    @web.asynchronous
    @gen.engine
    def post(self):
        email = self.get_argument("email")
        username = encode_username(email)

        user = yield gen.Task(self.application.server.get_user, username)
        if (not user or user.error):
            logging.error("Error retrieving user {} info".format(username))
            self.set_status(400)
            self.render(self.template, msg="Error retrieving user info")
            return

        response = yield gen.Task(self._send_activation_email, email,
                                  reset='true')
        if response and not response.error:
            self.redirect(self.reverse_url('activate') + '?reset=true')
        else:
            self.set_status(400)
            self.render(
                self.template, msg="Error sending password reset Email!")


class PasswordChange(Browser):
    def initialize(self):
        self.template = "password_change.html"

    @web.authenticated
    def get(self):
        self.render(self.template, msg=None)

    @web.authenticated
    @gen.coroutine
    def post(self):
        password = self.get_argument("password")
        username = encode_username(self.current_user)
        user = yield self.application.update_user_password(
            username, password)
        if user:
            self.set_status(200)
            self.render(
                self.template, msg="Password was successfuly changed!")
        else:
            self.set_status(400)
            self.render(
                self.template, msg="Error changing password!")


class AccountActivation(Browser):
    template = "activate.html"

    @web.asynchronous
    @gen.engine
    def get(self):
        msg = None

        reset = self.get_argument('reset', False)
        if reset:
            msg = ("To reactivate your account please click a link in the "
                   "Email we've just sent to you.")

        email = None
        signed_email = self.get_argument("signed_email", None)

        if signed_email:
            email = self.get_secure_cookie("signed_email", signed_email)
            if not email:
                msg = "Error activating account!"
        if email and reset:
            self.set_secure_cookie("email", email)
            self.redirect(
                self.get_argument('next', self.reverse_url('password_change')))
            return
        elif email:
            data = json_decode(self.get_secure_cookie(
                'signed_data', self.get_argument('signed_data', None)))
            password = data.pop('password', None)
            if not password:
                password = generate_passwd()
            acc_data = construct_account_data(email, data)
            response = yield gen.Task(self.application.bootstrap_user,
                                      encode_username(email),
                                      password, doc=acc_data)
            if response and response.error:
                self.set_status(response.errno)
                if response.errno == 409:
                    msg = "Please, choose another email address."
                else:
                    msg = response.msg
            else:
                self.set_secure_cookie("email", email)
                self.redirect(
                    self.get_argument('next', self.reverse_url('ui')))
                return

        self.render(self.template, msg=msg)


class Logout(Browser):
    def post(self):
        self.clear_cookie('email')
        self.redirect(self.get_argument('next', self.reverse_url('login')))


class AccountRemoval(Browser, App):
    """"""
    @web.authenticated
    @web.asynchronous
    @gen.engine
    def post(self):
        try:
            yield gen.Task(self.application.delete_account,
                           self.current_user, ignore_errors=True)
        except Exception as e:
            self.redirect(self.reverse_url('index'))
            return
        self.clear_cookie('email')
        self.redirect(self.reverse_url('login'))


class Login(Browser):
    """Web interface login page.

    Provides simple form to authenticate user against couchdb-stored
    credentials. Sets secure session cookie "email" and couchdb username based
    on it to use in ownership check all over the API.
    """
    template = "login.html"
    context = {'msg': None}

    def get(self):
        self.render(self.template, **self.context)

    @web.asynchronous
    @gen.engine
    def post(self):
        password = self.get_argument("password")
        email = self.get_argument("email")
        username = encode_username(email)

        user = yield gen.Task(self.application.server.get_user, username)

        if user and not user.error:
            db = yield gen.Task(self.application.get_user_db, username)
            if db.error:
                self.set_status(404)
                self.render(self.template, msg="No such database")
                return

            server = setup_trombi_server(username, password)
            user_db = yield gen.Task(server.get, dbname_for_username(username))
            if not user_db.error:
                self.set_secure_cookie("email", email)
                self.redirect(self.get_argument('next',
                                                self.reverse_url('ui')))
                return
        self.set_status(401)
        self.render(self.template, msg="Login error")


class GoogleAccount(Base, auth.GoogleMixin):
    @web.asynchronous
    def get(self):
        if self.get_argument("openid.mode", None):
            self.get_authenticated_user(self.async_callback(self._on_auth))
            return
        self.authenticate_redirect()

    @gen.engine
    def _on_auth(self, user):
        if not user:
            raise web.HTTPError(500, "Google auth has failed.")
        email = user['email']
        username = encode_username(email)
        user = yield gen.Task(self.application.server.get_user, username)
        if not user or user.error:
            response = yield gen.Task(self.application.bootstrap_user,
                                      username, generate_passwd(),
                                      doc={"bl": {"email": email}})
            if response.error:
                self.render("signup.html", msg=response.msg)
        self.set_secure_cookie("email", email)
        self.redirect(self.get_argument(
            "next", self.application.reverse_url("ui")))


class TwitterAccount(AccountSignup, auth.TwitterMixin):
    @gen.coroutine
    def get(self):
        if not self.get_argument("oauth_token", None):
            # user-initiated oauth link visit
            yield self.authorize_redirect()
            return

        # Got argument(s) on redirect from Twitter
        twitter_user = yield self.get_authenticated_user()
        if not twitter_user:
            raise web.HTTPError(500, "Twitter auth has failed.")

        if self.current_user:
            user = yield gen.Task(self.application.server.get_user,
                encode_username(self.current_user))
            user['bl'].update(
                {'twitter': {'access_token': twitter_user['access_token']}})
            yield gen.Task(self.application.server.update_user,
                           user)
        else:
            # Check if we already have user with this Twitter account in db
            user = yield self.application.get_user_by_twitter_id(
                twitter_user['access_token']['user_id'])
            if user:
                # Update twitter access token with a fresh value
                user['bl']['twitter']['access_token'] = twitter_user[
                    'access_token']
                yield gen.Task(self.application.server.update_user,
                               user)
                # User exists, so we just set secure cookie with an Email
                # and redirect to UI
                self.set_secure_cookie('email', user['bl']['email'])
            else:
                # Save Twitter access token (which includes screen name) in a
                # secure cookie and ask user for his Email (Twitter does not
                # disclose it).
                self.set_secure_cookie(
                    'tw_token_json', json_encode(twitter_user['access_token']))
                self.render("oauth_ask_email.html")
                return

        self.redirect(self.get_argument(
            'next', self.application.reverse_url('ui')))

    def get_data(self):
        twitter_access_token = json_decode(
            self.get_secure_cookie('tw_token_json'))
        if not twitter_access_token:
            raise web.HTTPError(
                500, "Twitter auth has failed: missing access token data.")
        data = super(TwitterAccount, self).get_data()
        data.update(
            {'twitter': {
                'access_token': twitter_access_token
            }})
        return data
