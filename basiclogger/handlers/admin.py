from basiclogger.handlers import Browser, App


class BaseAdmin(Browser, App):
    def prepare(self):
        pass
        #self.db = trombi.Database(self.application.server, userdb_name)


class AdminIndex(BaseAdmin):
    def get(self):
        self.render("admin/index.html", title="Admin index")
