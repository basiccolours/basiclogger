import collections
import functools
import logging
import trombi
from time import strptime, mktime

from tornado import auth, gen, web
from tornado.options import options
from tornado.ioloop import IOLoop, PeriodicCallback

from basiclogger import opts  # NOQA
from basiclogger.utils import (setup_trombi_server, is_valid_internal_username,
                               dbname_for_username, encode_username)


class Application(web.Application):
    def __init__(self, *args, **kwargs):
        def callback(db):
            if not db.error:
                logging.debug('Successfully opened database {}'.format(
                    options.couchdb_database))
            else:
                logging.warn(db.msg)

        web.Application.__init__(self, *args, **kwargs)
        self.server = setup_trombi_server()
        self.priv_server = setup_trombi_server(
            options.couchdb_admin_user,
            options.couchdb_admin_password)

        # Not an essential request, just to check if db is available
        # during app startup.
        self.server.get(options.couchdb_database, callback, create=False)

    @gen.engine
    def bootstrap_user(self, username, password, callback,  # inactive=False,
                       doc=None):
        if not is_valid_internal_username(username):
            callback(trombi.TrombiErrorResponse(400,
                                                "Error creating account: "
                                                "invalid username."))
            return
        userdb_name = dbname_for_username(username)
        user = yield gen.Task(self.add_user, username, password, doc=doc)
        if not user.error:
            userdb = yield gen.Task(self.priv_server.create, userdb_name)
            if userdb.error:
                logging.error("Error creating user db: {}, {}".format(
                    userdb_name, userdb.msg))
                yield gen.Task(self.delete_user, username)
                callback(trombi.TrombiErrorResponse(
                    400, "Error creating  account."))
                return
            else:
                logging.info("Created user db: {}".format(userdb_name))
        else:
            callback(user)
            return

        sec_obj = {
            "admins": {
                "names": [options.couchdb_user],
                "roles": []
            },
            "readers": {
                "names": [username],
                "roles": []
            }
        }
        response = yield gen.Task(userdb.set, '_security', sec_obj)
        # 200 code is returned here on success causing trombi to return
        # TrombiErrorResponse object
        if response.errno == 200:
            logging.info("Created security object for db: {}".format(
                userdb_name))
        else:
            logging.error(
                "Error creating security object for db: {}, {}".format(
                    userdb_name,
                    response.msg))
            yield gen.Task(self.delete_db, userdb_name)
            yield gen.Task(self.delete_user, username)
            callback(
                trombi.TrombiErrorResponse(400, "Error creating account."))
            return

        response = yield gen.Task(self.start_bootsrtap_repl, username)
        if not response.error:
            callback(response)
        else:
            yield gen.Task(self.delete_db, userdb_name)
            yield gen.Task(self.delete_user, username)
            #stop_bootsrtap_repl(server, username, callback)
            callback(
                trombi.TrombiErrorResponse(400, "Error creating account."))


#    @gen.engine
#    def activate_user(self, user, callback):
#        username = encode_username(user['bl']['email'])
#        if user['password_sha'].startswith('!'):
#            user['password_sha'] = user['password_sha'][1:]
#        user['bl'].pop('act_hash')
#        user['bl'].pop('act_expires')
#        response = yield gen.Task(self.server.update_user, user)
#        if not response.error:
#            logging.info("(Re)activated user: {}".format(username))
#            callback(response)
#            return
#        logging.error("Error activating user: {}, {}".format(
#                      username, response.msg))
#        callback(None)
#
#    @gen.engine
#    def deactivate_user():
#        raise NotImplementedError

    @gen.engine
    def add_user(self, username, password, callback, doc={}):  # inactive=F
        userdb = trombi.Database(self.server, '_users')
        if 'roles' not in doc:
            doc['roles'] = []
        if '_id' not in doc:
            doc['_id'] = 'org.couchdb.user:%s' % username
        doc['type'] = 'user'
        doc['name'] = username
        doc['password'] = password

        response = yield gen.Task(userdb.set, doc)

        if not response.error:
            logging.info("Created user: {}".format(username))
        else:
            logging.error("Error creating user: {}, {}".format(username,
                                                               response.msg))
            callback(response)
            return
        # Inactive user creation becomes non-atomic operation,
        # but it does not allow to log in as target users's database is not
        # created at this point yet.
        # We need this separate step to retrieve user's hashed password from
        # the db to easily prefix it with '!'
        # * This code path is actually unused now. *
        #if inactive:
        #    user = yield gen.Task(self.server.get_user, username)
        #    if (user and not user.error
        #            and not user['password_sha'].startswith('!')):
        #        user['password_sha'] = '!{}'.format(user['password_sha'])
        #        response = yield gen.Task(self.server.update_user, user)
        #        if not response.error:
        #            logging.info("Deactivated user: {}".format(username))
        #        else:
        #            logging.info("Error deactivating user: {}, {}".format(
        #                username, response.msg))
        callback(response)

    @gen.engine
    def delete_user(self, username, callback):
        username = encode_username(username)
        user = yield gen.Task(self.server.get_user, username)
        if user and not user.error:
            response = yield gen.Task(self.priv_server.delete_user, user)
            if not response.error:
                logging.info("Deleted user: {}".format(username))
            else:
                logging.error("Error deleting user: {}, {}".format(
                    username,
                    response.msg))
            callback(response)
        else:
            callback(user)

    @gen.coroutine
    def update_user_password(self, username, password):
        user = yield gen.Task(self.server.get_user, username)
        if user.error:
            callback(user_doc)
            logging.error("Error updating user password: {}, {}".format(
                username, response.msg))
            raise gen.Return(None)
        user['password'] = password
        updated = yield gen.Task(self.server.update_user, user)
        raise gen.Return(updated)

    @gen.engine
    def delete_db(self, dbname, callback):
        response = yield gen.Task(self.priv_server.delete, dbname)
        if not response.error:
            logging.info("Deleted database: {}".format(dbname))
        else:
            logging.warn("Unable to delete database: {}, {}".format(
                dbname,
                response.msg))
        callback(response)

    @gen.engine
    def start_bootsrtap_repl(self, username, callback):
        userdb_name = "{}{}".format(options.couchdb_user_db_prefix, username)
        repl_json = {
            "_id": "{}_bootstrap".format(userdb_name),
            "source": options.couchdb_database,
            "target": userdb_name,
            "doc_ids": ["_design/userdb", "_design/activity", "_design/tag",
                        "_design/session"],
            "continuous": True,
            "user_ctx": {
                "name": options.couchdb_user,
                "roles": []
            }
        }
        repl_doc = trombi.Document(None, repl_json)
        response = yield gen.Task(self.start_replication, repl_doc)
        callback(response)

    def stop_bootsrtap_repl(self, username, callback):
        userdb_name = dbname_for_username(username)
        repl_doc_id = "{}_bootstrap".format(userdb_name)
        self.stop_replication(repl_doc_id, callback)

    @gen.engine
    def start_replication(self, repl_doc, callback):
        replicatordb = trombi.Database(self.server, '_replicator')
        response = yield gen.Task(replicatordb.set, repl_doc)
        if not response.error:
            logging.info("Triggered replication {} -> {} by doc {}.".format(
                repl_doc['source'],
                repl_doc['target'],
                repl_doc.id))
        else:
            logging.error("Error triggering replication for "
                          "{} -> {} by doc {}.".format(
                              repl_doc['source'],
                              repl_doc['target'],
                              repl_doc.id))
        callback(response)

    @gen.engine
    def stop_replication(self, repl_doc_id, callback):
        replicatordb = trombi.Database(self.server, '_replicator')
        repl_doc = yield gen.Task(replicatordb.get, repl_doc_id)
        if repl_doc and not repl_doc.error:
            response = yield gen.Task(replicatordb.delete, repl_doc)
            if not response.error:
                logging.info("Stopped replication by doc {}".format(
                    repl_doc_id))
            else:
                logging.error("Unable to stop replication {}. {}".format(
                    repl_doc_id,
                    response.msg))
        else:
            response = trombi.TrombiErrorResponse(404, "Document not found.")
        callback(response)

    @gen.engine
    def restart_replication(self, repl_doc_id, callback):
        replicatordb = trombi.Database(self.server, '_replicator')
        repl_doc = yield gen.Task(replicatordb.get, repl_doc_id)
        if repl_doc and not repl_doc.error:
            del_response = yield gen.Task(replicatordb.delete, repl_doc)
            if not del_response.error:
                logging.info(
                    "Stopped replication by doc {}".format(repl_doc_id))
                for attr in ('rev', 'replication_state',
                             'replication_start_time', 'replication_id'):
                    setattr(repl_doc, attr, None)
                response = yield gen.Task(self.start_replication, repl_doc)
                callback(response)
            msg = "Unable to restart replication."
            logging.error(msg)
            response = trombi.TrombiErrorResponse(400, msg)
            callback(response)
        else:
            msg = "Document not found."
            logging.error(msg)
            response = trombi.TrombiErrorResponse(404, msg)
            callback(response)

    @gen.engine
    def delete_account(self, username, callback, ignore_errors=False):
        if not username:
            logging.warn('Got empty username during account delete attempt.')
            callback(False)
        else:
            response = yield gen.Task(self.stop_bootsrtap_repl, username)
            if not ignore_errors and response and response.error:
                raise ValueError(response.msg)
            response = yield gen.Task(self.delete_user, username)
            if not ignore_errors and response and response.error:
                raise ValueError(response.msg)
            response = yield gen.Task(self.delete_db,
                                      dbname_for_username(username))
            if not ignore_errors and response and response.error:
                raise ValueError(response.msg)
            callback(True)

    @gen.engine
    def get_user_dbs(self, callback):
        user_dbs = []
        all_dbs = yield gen.Task(self.server.list)
        if not isinstance(all_dbs, trombi.TrombiErrorResponse):
            for db in all_dbs:
                if db.name.startswith(options.couchdb_user_db_prefix):
                    user_dbs.append(db)
        callback(user_dbs)

    @gen.engine
    def get_user_db(self, username, callback):
        response = yield gen.Task(self.server.get,
                                  dbname_for_username(username))
        callback(response)

    @gen.engine
    def rereplicate_usrdb_design_docs(self, callback):
        dbs = yield gen.Task(self.get_user_dbs)
        if dbs:
            for db in dbs:
                doc_id = '{}_bootstrap'.format(db.name)
                username = db.name.replace(options.couchdb_user_db_prefix,
                                           '', 1)
                response = yield gen.Task(self.stop_bootsrtap_repl, username)
                if response and response.error:
                    logging.warn(response.msg)
                bootstrap_resp = yield gen.Task(self.start_bootsrtap_repl,
                                                username)
            #callback(bootstrap_resp) - not used
            callback(trombi.TrombiResult("Finished."))
            return
        callback(trombi.TrombiErrorResponse(404, "Document not found."))

    @gen.coroutine
    def get_user_by_twitter_id(self, twitter_id):
        users_db = trombi.Database(self.server, '_users')
        res = yield gen.Task(users_db.view, 'social', 'by_twitter_user_id',
                             key=twitter_id, include_docs=True)
        if not res or res.error:
            raise gen.Return(None)
        else:
            raise gen.Return(res[0]['doc'])


class TwitterWorker(auth.TwitterMixin):
    """Allows to communicate with Twitter.

    This class mimics some RequestHandler attrs and methods to make
    TwitterMixin work. Designed to be called from a separate process. The list
    of copy-pasted RequestHandler members is:
     * async_callback()
     * require_setting()
     * settings() @property
     * application attr
    """

    def __init__(self, app, interval):
        self.application = app
        self.ioloop = IOLoop.current()
        self.interval = interval

    def async_callback(self, callback, *args, **kwargs):
        if callback is None:
            return None
        if args or kwargs:
            callback = functools.partial(callback, *args, **kwargs)

        def wrapper(*args, **kwargs):
            try:
                return callback(*args, **kwargs)
            except Exception as e:
                if self._headers_written:
                    app_log.error("Exception after headers written",
                                  exc_info=True)
                else:
                    self._handle_request_exception(e)
        return wrapper

    def require_setting(self, name, feature="this feature"):
        """Raises an exception if the given app setting is not defined."""
        if not self.application.settings.get(name):
            raise Exception("You must define the '%s' setting in your "
                            "application to use %s" % (name, feature))

    @property
    def settings(self):
        """An alias for `self.application.settings <Application.settings>`."""
        return self.application.settings

    @gen.coroutine
    def fetch(self):
        cb_keys = []
        logging.debug("TwitterWorker.fetch()")
        start = self.ioloop.time()
        users_db = trombi.Database(self.application.server, '_users')
        # TODO: what if we have a HUGE user base? 'results' will be too big.
        results = yield gen.Task(users_db.view, 'social', 'by_twitter_user_id',
                                 include_docs=True)
        for row in results:
            user = row['doc']
            logging.debug("Found user with twitter account connected: "
                          + user['bl']['email'])
            db = trombi.Database(self.application.server,
                                 dbname_for_username(user['name']))
            settings = yield gen.Task(db.get, 'settings')
            if not settings or not settings.get('sync_twitter', None):
                logging.debug("sync_twitter is false for: "
                              + user['bl']['email'])
                continue

            # add_callback() does not allow to wait for specific callback and
            # yielding a list/dict does not allow to add a delay
            key = user['name'] + '_fetch_user_tweets'
            self._fetch_user_tweets(user, callback=(yield gen.Callback(key)))
            cb_keys.append(key)
            # async sleep for some time not to fire all fetch jobs at once
            yield gen.Task(self.ioloop.add_timeout, self.ioloop.time() + 0.3)
        yield gen.WaitAll(cb_keys)
        spent = self.ioloop.time() - start
        if self.interval > spent:
            self.ioloop.add_timeout(
                self.ioloop.time() + (self.interval - spent), self.fetch)
        else:
            logging.info(
                "TwitterWorker: fetching tweets took more time ({}s) than "
                "self.interval ({}s). Consider increasing interval.".format(
                    spent, self.interval))
            self.ioloop.add_callback(self.fetch)

    @gen.coroutine
    def _fetch_user_tweets(self, user):
        access_token = user['bl']['twitter']['access_token']
        username = user['name']
        log_suffix = '{} / @{}'.format(user['name'],
                                       access_token['screen_name'])
        logging.debug("Fetching tweets for " + log_suffix)
        db = trombi.Database(self.application.server,
                             dbname_for_username(username))
        activity = yield self._create_activity_for_tweets(user)
        # get last tweet id from db
        last_saved_session = yield gen.Task(
            db.view, 'session', 'tweets', descending=True, limit=1)
        if len(last_saved_session) == 1:
            last_saved_tweet_id = last_saved_session[0]['value']
        else:
            last_saved_tweet_id = 0
        tweets = yield self.twitter_request(
            "/statuses/user_timeline", access_token=access_token,
            user_id=access_token['user_id'],
            trim_user=True, since_id=last_saved_tweet_id + 1)
        logging.debug("Got " + str(len(tweets)) + " new tweets for " +
                      log_suffix)
        for t in tweets:
            yield gen.Task(self.ioloop.add_timeout, self.ioloop.time() + 0.3)
            logging.debug("Saving tweet " + str(t['id']) + " for " +
                          log_suffix)
            tweet_ts = int(mktime(strptime(
                t['created_at'], '%a %b %d %H:%M:%S +0000 %Y'))) * 1000
            session = {
                'tweet': t, 'type': 'session', 'activity': activity,
                'start': tweet_ts, 'end': tweet_ts + 1}
            r = yield gen.Task(db.set, session)
            if r.error:
                logging.info("Error saving tweet for " + log_suffix +
                             ". " + r.msg)

    @gen.coroutine
    def _create_activity_for_tweets(self, user):
        timeline = user['bl']['twitter'].get('timeline', {})
        activity = timeline.get('activity', None)
        # TODO: we need some method to disable tweets sync for account.
        # Now deleted activity just hides them, but they are get synced.
        if not activity:
            db = trombi.Database(
                self.application.server,
                dbname_for_username(user['name']))
            r = yield gen.Task(
                db.set,
                {'type': 'activity', 'title': 'Twitter timeline'})
            if r and not r.error:
                user['bl']['twitter'].update(
                    {'timeline': {'activity': r.id}})
                yield gen.Task(self.application.server.update_user, user)
            activity = r.id
        raise gen.Return(activity)


# TODO: move to trombi?
class SpatialViewResult(trombi.TrombiObject, collections.Sequence):
    def __init__(self, result, db=None):
        self.db = db
        self.update_seq = result.get('update_seq')
        self.total_rows = len(result['rows'])
        self._rows = result['rows']

    def __len__(self):
        return len(self._rows)

    def __iter__(self):
        return (x for x in self._rows)

    def __getitem__(self, key):
        return self._rows[key]
