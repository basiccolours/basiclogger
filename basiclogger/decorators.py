import functools
import logging

import trombi

from basiclogger.utils import setup_trombi_server, dbname_for_username


# Unused in real API requests, as virtually any such call requires
# authentication now
def api_authenticated(method):
    """Decorate API methods with this to require that the user be logged in.

    tornado.web.authenticated uses login page and redirects and thus is more
    suitable for browser requests.

    """
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        if not self.current_user:
            self.set_status(403)
            self.write({'error': 'This API call requires authentification.'})
            self.finish()
            return
        return method(self, *args, **kwargs)
    return wrapper


# UNUSED
def verify_basic_auth(method):
    """
    Try to GET user's database with provided basic auth credentials.

    This acts as a check before performing user-initiated account-wide actions
    by privileged CouchDB account. One example is an account DELETE request.

    """
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        def _finish_with_error(msg):
            self.set_status(401)
            logging.warn(
                'Basic auth check fail, user: {0}.'.format(username))
            self.write({'error': msg})
            self.finish()

        def _got_db(result):
            if result.error:
                _finish_with_error(result.msg)
            else:
                return method(self, *args, **kwargs)

        username = self.basic_auth.get('username', None)
        password = self.basic_auth.get('password', None)
        if not username or not password:
            _finish_with_error('No username given.')
            return
        server = setup_trombi_server(username, password)
        server.get(dbname_for_username(username), _got_db)

    return wrapper
