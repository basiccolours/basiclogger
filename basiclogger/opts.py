from tornado.options import define

define("couchdb_protocol", default="http", metavar="http|https",
       help="CouchDB database protocol")
define("couchdb_host", default="127.0.0.1", metavar="HOSTNAME",
       help="CouchDB database host")
define("couchdb_port", default=5984, metavar="PORT", type=int,
       help="CouchDB database port")
define("couchdb_database", default="logger", metavar="DBNAME",
       help="CouchDB database name")
define("couchdb_user", default="logger", metavar="USERNAME",
       help="CouchDB database user")
define("couchdb_password", default="", metavar="PASSWORD",
       help="CouchDB database password")
define("couchdb_user_db_prefix", default="usr__",
       help="CouchDB per-user database name prefix")
define("couchdb_admin_user", default="admin", metavar="USERNAME",
       help="CouchDB admin user")
define("couchdb_admin_password", default="", metavar="PASSWORD",
       help="CouchDB admin password")

define("mandrill_api_url", default="https://mandrillapp.com/api/1.0/",
       metavar="MANDRILL_API_URL", help="Mandrill API endpoint URL")
define("mandrill_api_key", default="",
       metavar="MANDRILL_API_KEY", help="Mandrill API key")
define("mandrill_from_email", default="",
       metavar="MANDRILL_FROM_EMAIL", help="Mandrill from Email address")

define("sentry_dsn", default="", metavar="SENTRY_DSN", help="Sentry URL")

define("twitter_consumer_key", default="",
       metavar="TWITTER_API_KEY", help="Twitter API key")
define("twitter_consumer_secret", default="",
       metavar="TWITTER_API_SECRET", help="Twitter API secret")
