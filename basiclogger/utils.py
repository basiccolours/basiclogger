import hashlib
import logging
import re
import string
import trombi
from datetime import datetime
#from hashlib import sha1
from time import mktime
from random import choice

from tornado.httpclient import AsyncHTTPClient
from tornado.escape import json_encode
from tornado.options import options
from tornado import gen

VALID_USERNAME = re.compile(r'^[a-z][a-z0-9_]{2,32}$')
VALID_INTERNAL_USERNAME = re.compile(r'^[a-z][a-z0-9_\-\$]{2,32}$')
VALID_EMAIL = re.compile(r'^[^@]+@[^@]+\.[^@]+$')


def epoch_time(dt=None):
    if not dt:
        dt = datetime.utcnow()
    seconds = mktime(dt.timetuple())
    milliseconds = seconds * 1000 + dt.microsecond // 1000
    return int(milliseconds)


def generate_passwd(length=16):
    chars = '{}{}'.format(string.letters, string.digits)
    return ''.join(choice(chars) for _ in xrange(length))


#def verify_user_password(password, password_sha, salt):
#    if (password_sha == sha1(password + salt).hexdigest()):
#        return True


def is_valid_username(username):
    if VALID_USERNAME.match(username):
        return True


def is_valid_internal_username(username):
    if VALID_INTERNAL_USERNAME.match(username):
        return True


def is_valid_email(email):
    if VALID_EMAIL.match(email):
        return True


def encode_username(username):
    if not username:
        raise ValueError
    return username.replace('@', '$').replace('.', '-')


def dbname_for_username(username):
    if not username:
        raise ValueError
    return "{}{}".format(options.couchdb_user_db_prefix,
                         encode_username(username))


def construct_account_data(email, extra_data={}):
    data = {
        "bl": {
            "email": email
        }
    }
    data['bl'].update(extra_data)
    return data


def setup_trombi_server(username=None, password=None, protocol=None,
                        host=None, port=None):
    username = username or options.couchdb_user
    password = password or options.couchdb_password
    protocol = protocol or options.couchdb_protocol
    host = host or options.couchdb_host
    port = port or options.couchdb_port

    couchdb_url = '{}://{}:{}'.format(protocol, host, port)
    server = trombi.Server(couchdb_url,
                           fetch_args=dict(
                               auth_username=encode_username(username),
                               auth_password=password))
    return server


@gen.engine
def send_email(subject, message, recipient_list, callback, html_message=None,
               from_email=None, async=False, api_key=None):
    """Send email through Mandrill API"""
    if not api_key:
        api_key = options.mandrill_api_key
    if not from_email:
        from_email = options.mandrill_from_email

    http_client = AsyncHTTPClient()
    api_url = options.mandrill_api_url + "messages/send.json"
    mail_data = {
        "key": api_key,
        "async": async,  # async here refers to Mandrill API behaviour
        "message": {
            "subject": subject,
            "text": message,
            "html": html_message,
            "from_email": from_email,
            "from_name": "robot",
            "to": recipient_list
        }
    }
    logging.debug(mail_data)

    if not options.debug:
        response = yield gen.Task(http_client.fetch, api_url, method='POST',
                                  body=json_encode(mail_data))
        logging.debug(response)
        logging.debug(response.body)
        callback(response)
    else:
        result = trombi.TrombiResult({})
        result.error = None
        callback(result)
