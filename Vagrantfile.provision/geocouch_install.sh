#!/bin/bash

set -e -x

name=geocouch
version=1.3
release=1
source=()

#git_version=8c01f93fa6
git_branch=couchdb1.3.x
couchdb_version=1.5.0


PKGMK_SOURCE_DIR=/home/vagrant/tmp/distfiles
PKGMK_PACKAGE_DIR=/home/vagrant/tmp/packages
BUILD=/home/vagrant/tmp/build
SRC=$BUILD/$name/src
PKG=$BUILD/$name/pkg

export COUCH_SRC=$SRC/apache-couchdb-$couchdb_version/src/couchdb
SCRIPT_DIR=$( cd $( dirname "${BASH_SOURCE[0]}" ) && pwd )

prepare(){
    rm -rf $BUILD/$name
    mkdir -p $SRC
    mkdir -p $PKGMK_SOURCE_DIR
    mkdir -p $PKGMK_PACKAGE_DIR
    mkdir -p $PKG
    cd $PKGMK_SOURCE_DIR
    rm -rf apache-couchdb
    wget http://www.eu.apache.org/dist/couchdb/source/$couchdb_version/apache-couchdb-$couchdb_version.tar.gz
    cp apache-couchdb-$couchdb_version.tar.gz $SRC
    cd $SRC
    tar -xf apache-couchdb-$couchdb_version.tar.gz
}

build(){
    cd $PKGMK_SOURCE_DIR
    if cd $name.git; then
        git fetch
        #git checkout $git_version
        git checkout $git_branch
    else
        git clone git://github.com/couchbase/$name.git $name.git -b $git_branch
        #git clone git://github.com/couchbase/$name.git $name.git #-b $git_branch
        cd $name.git
        #git checkout $git_version
    fi
    cp -r $PKGMK_SOURCE_DIR/$name.git $SRC
    cd $SRC/$name.git
    make
    mkdir -p $PKG/etc/couchdb/default.d/
    mkdir -p $PKG/usr/share/couchdb/www/script/test/
    mkdir -p $PKG/usr/lib/couchdb/erlang/lib/$name/
    cp etc/couchdb/default.d/* $PKG/etc/couchdb/default.d/
    cp -r ebin $PKG/usr/lib/couchdb/erlang/lib/$name/
    cp share/www/script/test/* $PKG/usr/share/couchdb/www/script/test/
}

make_pkg(){
    cd $PKG
    sudo chown -R 0:0 *
    tar -c -z -f $name-$version-$release.tar.gz *
    mv $name-$version-$release.tar.gz $PKGMK_PACKAGE_DIR
    sudo rm -rf $PKG/*
}

install_pkg(){
    sudo /bin/bash -c "cd / && tar -xf $PKGMK_PACKAGE_DIR/$name-$version-$release.tar.gz"
    grep spatial /usr/share/couchdb/www/script/couch_tests.js && echo "JS tests are already installed" && return
    sudo /bin/bash -c "cat $SCRIPT_DIR/geocouch_tests.js >>  /usr/share/couchdb/www/script/couch_tests.js"
}

prepare
build
make_pkg
install_pkg
