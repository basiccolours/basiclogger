#!/bin/bash

set -x

DATA_DIR="/vagrant/Vagrantfile.provision"
AS_VAGRANT="/usr/bin/sudo -u vagrant"

export LANGUAGE=en_US.UTF8
export LANG=en_US.UTF8
export LC_ALL=en_US.UTF8
locale-gen en_US.UTF8
dpkg-reconfigure locales
echo "LANG=en_US.UTF8" > /etc/default/locale
echo "LANGUAGE=en_US.UTF8" >> /etc/default/locale
echo "LC_ALL=en_US.UTF8" >> /etc/default/locale

apt-get update -y
apt-get install -y software-properties-common python-software-properties || exit
# too old
#add-apt-repository -y ppa:longsleep/couchdb
add-apt-repository -y ppa:couchdb/stable
apt-get update -y
apt-get remove couchdb couchdb-bin couchdb-common -y -f
# do we really need this on dev box?
#apt-get upgrade -y
apt-get install -y build-essential python python-dev python-setuptools \
  python-pip git mercurial \
  libxml2 libxslt1-dev libjpeg-dev libpng-dev libfreetype6-dev \
  erlang-base \
  mc curl htop
apt-get install -V -y couchdb
apt-get clean

bash  $DATA_DIR/geocouch_install.sh

pip --quiet install virtualenvwrapper

# TODO: download and update to latest VBoxGuestAdditions.iso

#CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# TODO: make sure these files are owned by vagrant
$AS_VAGRANT -s -- cat $DATA_DIR/bashrc_additions >> /home/vagrant/.bashrc
$AS_VAGRANT -s -- cat $DATA_DIR/profile_additions >> /home/vagrant/.profile
$AS_VAGRANT -s -- cat $DATA_DIR/aliases_additions >> /home/vagrant/.bash_aliases

PROJECT_NAME=$1
DB_NAME=$PROJECT_NAME
VIRTUALENV_NAME=$PROJECT_NAME
PROJECT_DIR=/home/vagrant/projects/$PROJECT_NAME
VIRTUALENV_DIR=/home/vagrant/.virtualenvs/$PROJECT_NAME

rm -rf $VIRTUALENV_DIR
$AS_VAGRANT -s -- /usr/local/bin/virtualenv --distribute $VIRTUALENV_DIR
$AS_VAGRANT -s -- echo $PROJECT_DIR > $VIRTUALENV_DIR/.project

echo "workon $VIRTUALENV_NAME" >> /home/vagrant/.bashrc
