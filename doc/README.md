[TOC]

Basiclogger README
==================

Debugging
---------

### Debug mode ###

Debug mode can be set by running app with `--debug` switch or by setting
`debug = True` in settings file. It is not related to logging setting but
affects few important aspects:

 * App does not create/check pid files.
 * XSRF cookie check is bypassed.
 * Signup page redirects directly to the valid activation url, which allows
   to skip Email address verification.

### Using httpie ###

Install HTTPie, set some handy shell variables and aliases:

    pip install httpie
    export APP="127.0.0.1:8081"

Create an authenticated session:

    http -f --session=bob POST $APP/account/login email=bob@example.com password=bbb

Use readonly session like this:

    http -v --session-read-only=bob POST $APP/api/activity title=something

### Cross-site request forgery ###

CSRF protection is on by default, but is disabled in debug mode which makes
testing the API a bit more straightforward.

If you need to test non-debug application instance, follow these steps.

Be sure server set _xsrf cookie and lookup it's value to use in any
POST/PUT/DELETE request (either as '_xsrf' form field or 'X-XSRFToken' header):

    http --print Hh --session=bob $APP/account/login | grep -E "Cookie:\s"
    http -f --session=bob POST $APP/account/login email=bob@example.com password=bbb _xsrf=<val>
    http --session=bob POST $APP/api/activity title=foo X-XSRFToken:<val>

Testing
-------

To run all the feature tests on localhost:

    pip install -r requirements/testsuite.txt
    behave
