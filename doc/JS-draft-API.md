[TOC]

Basiclogger internal JS HTTP API specs
=======================================

*This document briefly describes internal JS HTTP API version DRAFT*

It is used for initial web frontend to standartize JS HTTP calls to the thin
Tornado glue between user's browser and CouchDB API.

General rules
-------------

 * Server address: `http://logger.basiccolours.com.ua`
 * Server speaks JSON.
 * No trailing slash in URI is allowed. Nginx rewrite allows it in production
   environment though.

### Auth ###

Browser-based interface sets secure cookie on login, which is verified on
every API request. If this method fails, a HTTP basic auth credentials must be
present, and are used to make requests to underlying database.

### Payload format ###

 * `<id>` in the URI is the *object id*, which is not present in *POST*
   requests (used to create objects).
 * `<id>` format is 32 digit hex number (unless otherwise noted). Clients are
   allowed to include it in initial *PUT* request, however it is expected to
   mimic CouchDB's `utc_random` uuid format: *time since Jan 1, 1970 UTC with
   microseconds; first 14 characters are the time in hex, last 18 are random*.
   If not provided, server will generate it with the time the doc is synced.
 * The only valid true `<bool>` value is string `true`, anything esle is false.
 * `<date>` is always integer, milliseconds since *1970-01-01 00:00:00 UTC*
   (also suitable for JavaScript Date() constructor). 1 to 14 digits number is
   required.

### Operations ###

 * Typical error reply payload looks like:
   `{"error": "short description text"}`. Apropriate HTTP response codes are
   set accordingly.
 * *PUT* & *POST* success response is of the following format:
   `{"id": <id>, "rev": <rev>}` (note missing underscore). This allows clients
   to update their native objects/cache.
 * *PUT* requests updating existing documents must provide `<rev>` field,
   against which current document revision is checked.
 * Partial *PUT* requests are not supported, the whole document should be
   included in the request.
 * Server reply may contain some redundant data like internal user id or
   other fields unknown to the client. Client must be able to ignore such data.
   Trying to send this data back is permitted, however server will ignore (and
   overwrite) it before saving the document.
 * Successful *DELETE* response has empty payload with __200 OK__ status code.

### Oddness ###

 * *GET*/*DELETE* requests content currently is not checked for doc `type`.
   (Do we need this explicit field in the request?)
 * *PUT*/*POST* requests do not have to supply document `type` (it is taken
   from URI)
 * *DELETE*/*PUT* requests do not require `<rev>`, but server cheks them if
   supplied. (Making this a per-account option might be a better idea.)
 * Current limit for batch requests/results is 1000. This applies to
   multiple *POST*/*GET* number of documents, and `changes` API rows.

### Common response codes ###

 * __200 OK__ indicates successfull operation.
 * __201 Created__ is a success response for POST or PUT request.
 * __404 Not Found__ might be a result of wrong documet `<id>` or failed
   ownership checks.
 * __400 Bad Request__ indicates wrong request format or some generic error.
 * __409 Conflict__ indicates that wrong (old) `_rev` was provided in PUT
   request. Some other client might have updated target document in the
   meanwhile. Client should try to *GET* the last revision of the document.
   Decision on merging changes is up to client.

Working with different resources
--------------------------------

### Account management ###

URI: `api/account`

Methods:

 * *GET*: verify that account exists. Returns __401 Unauthorized__ on missing
   account or wrong credentials.
 * *DELETE*: delete account and all data. Pass `ignore_errors=true` query
   string parameter to ignore errors during account removal (might be usefull
   if account creation was stuck at some point).

### Common document formats ###

`<document_type>` is one of the following:

 * `activity`
 * `session`

It is set by server on POST/PUT request, and is present in any database
retreived document.

#### activity ####

Example `activity` document:

    {
        "_id": <id>,
        "_rev": <rev>,
        "type": "activity", // automatically set
        "title": "title string",     // required
        ...
    }

#### session ####

Example `session` document:

    {
        "_id": <id>,
        "_rev": <rev>,
        "type": "session",  // automatically set
        "activity": <id>,   // required
        "start": <date>,    // required
        "end": <date>,      // required (is null on session creation)
        ...
    }

##### Timerange validation rules  #####

General rules:

 * Session's `<start>` should be less then `<end>`

Sessions with a common activity have the following restrictions:

 * Sessions can not overlap:
    * no session timerange may be within other session timerange
      (including borders)
    * no sessions with equal `<start>` times

### Single document API ###

URI: `api/<document_type>/<id>[?rev=<rev>]`

Methods:

 * *GET*: get the document by `<id>`.
 * *POST*: create a new document (with a server generated `<id>`).
 * *PUT*:
     * update existing document referenced by by `<id>` and `<rev>`. Revision
     can be supplied as a *rev* query argument or as a *_rev* document field.
     * create new document with specified `<id>`
 * *DELETE*:  delete document by `<id>`. Specify an optional `<rev>` query
   argument to be sure you are deleting the latest document revision.

### Multiple documents API ###

__The bulk API is readonly now: only GET requests.__

URI: `api/bulk/<document_type>`

*GET*, common parameters (not all of them are supported for every
`<document_type>`):

 * *descending*: `<bool>`, use to sort documents in descending order
 * *limit*: `<int>`, the number of rows to fetch, up to and defaults to 1000
 * *include_docs*: `<bool>`, whether to include full "doc" in each row, *false*
   by default
 * *startkey*
 * *endkey*

*startkey*/*endkey* format is `<document_type>`-dependent. This is mostly
usefull with rows sorted by date or title. Unless explicitly documented for
specific `<document_type>`, you should not rely on these parameters.

Response format:

    {
        "offset": <int>,     // actual offset
        "total_rows": <int>, // total number of documents of this type
        "rows": [
            {
                "id": <id>,
                "rev": <rev>,
                "doc": {
                        "_id": <id>,
                        "_rev": <rev>,
                        ...
                    },
            },
            {...}
        ]
    }

#### Type-specific notes ####

##### activity #####

 *tags* parameter should be a single tag or a string of tags separated
   by commas:

        bulk/activity?tags=tag_one,tag_two&any_tags=true

   Combined with *any_tags=true* it returns a list of activities tagged with
   any of the provided tags. Without *any_tags* it returns a list of activities
   tagged with all of the provided tags. __Sorting and limiting parameters can
   not be combined with *tags*__.

##### session #####

 * Sessions are sorted by their `end` date (empty date is *null*) so you can
   *GET* `sessions` between *date1* and *date2*:

        bulk/session/?startkey=date1&endkey=date2

 * *activity* parameter can be used to get `sessions` belonging to specific
   `activity`:

        bulk/session/?activity=<id>

Note, these two parameters can not be combined.

##### tag #####

Response format:

    {
        "offset": <int>,
        "total_rows": <int>,
        "rows": [
            {
                "count": <int>,
                "tag": "tagname_string"
            },
            {...}
        ],
    }

 * tags are sorted by `tag` key
 * *include_docs* parameter is not supported (as we store tags within
   `activities`)

### Changes API (No tests yet) ###

Client can get a list of changed documents of particular type. Only simple
polling calls are supported for now. The only allowed method is *GET*.

URI: `api/changes[/<document_type>]`

Supported parameters:

 * *doc_type*: "str", optional document type to get changes for. Values are
   the same as base URIs for different resources. Note that deleted documents
   will be not present in such filtering. (That is by design: filter functions
   check `document.type` which is not really available after documet gets
   deleted.)
 * *since*: `<int>`, sequence number to start with
 * *limit*: `<int>`, maximum number of changes rows to get in this request
   (`"last_seq"` in a reply reflects the specified limit value to make it a
   usefull starting point in the next request).
 * *include_docs*: `<bool>`, whether to include full document along with each
   sequence number, *false* by default.

Sample response:

    {
        "last_seq": 1032,
        "results": [
            {
                "id": <id>,
                "changes": [{"rev": <rev>}],
                "seq": 1031,
                "doc": {
                    "_id": <id>,
                    "_rev": <rev>,
                    "type": "activity",
                    "updated": "1320400934686",
                    "title": "something",
                }
            },
            {
                "id": <id>,
                "changes": [{"rev": <rev>}],
                "seq": 1032,
                "deleted": true,
                "doc": {
                    "_deleted": true,
                    "_id": <id>,
                    "_rev": <rev>
                }
            },
        ]
    }

### Settings API ###

URI: `api/settings`

Supported methods: *GET*, *PUT*

Settings object is an arbitrary JSON. Client is encouraged to put his specific
settings in some unique prefix inside JSON.

Be sure to provide complete settings JSON in *PUT* request payload - we only
save what we recieve from client. Usual revision checking rules apply.
