require.config({
    baseUrl:"./js/app",
    // 3rd party script alias names (Easier to type "jquery" than "libs/jquery, etc")
    // probably a good idea to keep version numbers in the file names for updates checking
    paths:{
        // Core Libraries
        "jquery":"../libs/jquery",
        "jqueryui":"../libs/jqueryui",
        "underscore":"../libs/lodash",
        "backbone":"../libs/backbone",
        "marionette":"../libs/backbone.marionette",
        "handlebars":"../libs/handlebars-v1.3.0",

        // Additional stuff
        "utils":"../app/utils",
        "userConfig":"../app/userConfig",

        // Plugins
        // "backbone.validateAll":"../libs/plugins/Backbone.validateAll",
        "xsrf": "xsrf",
        "backbone.sync": "backbone.sync",
        "backbone.dualstorage": "../libs/plugins/backbone.dualstorage",
        "bootstrap":"../libs/plugins/bootstrap",
        "text":"../libs/plugins/text",
        "tagit":"../libs/plugins/tag-it.min",

        "jasmine":"../libs/jasmine",
        "jasmine-html":"../libs/jasmine-html"
    },
    // Sets the configuration for your third party scripts that are not AMD compatible
    shim:{
        "bootstrap":["jquery"],
        "jqueryui":["jquery"],
        "backbone":{
            "deps":["underscore"],
            // Exports the global window.Backbone object
            "exports":"Backbone"
        },
        "marionette":{
            "deps":["underscore", "backbone", "jquery"],
            // Exports the global window.Marionette object
            "exports":"Marionette"
        },
        "handlebars":{
            "exports":"Handlebars"
        }

        // Backbone.validateAll plugin (https://github.com/gfranko/Backbone.validateAll)
        // ,"backbone.validateAll":["backbone"]
        ,"backbone.sync":["backbone"]
        ,"backbone.dualstorage":["backbone", 'backbone.sync']
        ,"xsrf":["jquery"]
        ,"tagit":["jquery", 'jqueryui'],

        // Jasmine Unit Testing
        "jasmine":{
            // Exports the global 'window.jasmine' object
            "exports":"jasmine"
        },

        // Jasmine Unit Testing helper
        "jasmine-html":{
            "deps":["jasmine"],
            "exports":"jasmine"
        }
    // For easier development, disable browser caching
    // Of course, this should be removed in a production environment
    , urlArgs: 'bust=' +  (new Date()).getTime()  
    }
});

// Include Desktop Specific JavaScript files here (or inside of your Desktop router)
// require(["jquery", "backbone", "marionette", "jasmine-html", ],
//     function ($, Backbone, Marionette, jasmine) {
//         var specs = ['../test/specs/spec'];

//         $(function () {
//             require(specs, function () {
//                 jasmine.getEnv().addReporter(new jasmine.TrivialReporter());
//                 jasmine.getEnv().execute();
//             });
//         });
//     });