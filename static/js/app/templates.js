define(function (require) {
	'use strict';

	return {
		activitiesList: require('text!templates/activitiesList.html'),
		activitiesListItem: require('text!templates/activitiesListItem.html'),
		timebar: require('text!templates/timebar.html'),
		timebarActivity: require('text!templates/timebarActivity.html'),
		tagsList: require('text!templates/tagsList.html'),
		editWidget: require('text!templates/editWidget.html'),
		timerDisplay: require('text!templates/timerDisplay.html')
	};
});

