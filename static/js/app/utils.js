define(['userConfig'], function(config) {
	var	utils = {
		guid: function() {
		  var getRandomHex = function(i) {
		    var chars, string;
		    string = "";
		    chars = "01234567abcdef";
		    while (i > 0) {
		      string += chars.charAt(Math.floor(Math.random() * chars.length));
		      i--;
		    }
		    return string;
		  },
		  hexTime = parseInt(new Date().getTime() * 1000).toString(16);
		  hexTime = "00000000000000".slice(0, 14 - hexTime.length) + hexTime;
		  return hexTime + getRandomHex(18);
		},

		dhms: function(ms, string, includeSeconds){
			var	secs = Math.round(ms / 1000),
				days = Math.floor(secs / 86400),
				secs = secs % 86400,
				time = [0, 0, secs],
				i = 2;

			while (i > 0){
			  time[i - 1] = Math.floor(time[i] / 60);
			  time[i] = time[i] % 60;
			  if (time[i] < 10) time[i] = '0' + time[i];
			  i--;
			};

			if (string) {
	      		// time = time.join(':');
	      		time = time[0]+"h "+time[1]+"m"+(includeSeconds ? ' '+time[2]+'s' : '');
	      		if (days == 1) time = '1 day ' + time
	      		if (days > 1) time = days + ' days ' + time
			} else if (days > 0) {
				time[0] = time[0] + days*24;
			}

			return time;
		},

		color: function(model){
			if (model.get("color")) return model.get("color");
			var ind = model.id.replace(/[^0-9]+/g, '').slice(-6)%config.colorSet.length;
			return config.colorSet[ind];
		},

		time24: function(date){
			var date, hh, m, s;
			if (typeof date !== 'object') date = new Date(date);
      hh = date.getHours();
      m = date.getMinutes();
      s = date.getSeconds();
      m = m<10?"0"+m:m;
      s = s<10?"0"+s:s;

      return [hh, m, s].join(":");
		}
	};

	return utils;
});

