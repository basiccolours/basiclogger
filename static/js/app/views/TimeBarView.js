require(['../libs/iscroll/iscroll']),
define([
  'marionette',
  'templates',
  'utils',
  'views/TimeBarSessionView',
  'jquery.mousewheel'
], function (Marionette, templates, utils, TimeBarSessionView) {

  return Marionette.CompositeView.extend({

    template: Handlebars.compile(templates.timebar),

    itemView: TimeBarSessionView,

    itemViewContainer: '#timebar-sessions',

    events: {
      'mousewheel': 'zoomAction'
    },

    zoomAction: function(event, delta){
      var direction, zoomStep = 0.5;

      event.stopPropagation();
      event.preventDefault();

      if (delta > 0) {
          direction = 'up';
          this.zoomFactor +=zoomStep;
      } else if (delta < 0) {
          if (this.zoomFactor == 0.5) return;
          direction = 'down';
          this.zoomFactor -=zoomStep;
      };

      Backbone.trigger("zoom", this.zoomFactor);

      this.scrollToValue = Math.round(event.clientX-event.offsetX*this.days*24*60*this.zoomFactor/event.currentTarget.clientWidth);

      $(".timescale_hh").width(60*this.zoomFactor)
      $("#timescale").width(this.days*24*60*this.zoomFactor)
      this.renderClock(true);
    },
    
    initialize: function() {
      var days = this.days = 2;
      this.zoomFactor = 1;
      this.activities = this.options.activities;
      this.collection = new Backbone.VirtualCollection(this.options.sessions, {
        filter: function (session) {
          var startPoint = new Date().setHours(0,0,0,0) - (days-1)*86400000;
          return session.get("end") > startPoint || !session.get("end");
        }
      });      
      // at midnight we rebuild timebar:
      this.midRebuild();
    },

    itemViewOptions: function() {
        return {activities: this.activities, days: this.days, zoomFactor: this.zoomFactor}
    },

    serializeData: function(){
      return {
        timescale: this.prerenderedTimescale(),
        timescaleWidth: this.zoomFactor*this.days*24*60
      }
    },

    onDomRefresh: function() {
      // console.log("Timebar onDomRefresh fired");
      this.renderClock();
    },

    midRebuild: function(){
      var dayStart = new Date().setHours(0,0,0,0), self = this;
      
      var checkMidnight = function (){
        if (dayStart !== new Date().setHours(0,0,0,0)) {
          self.render();
          console.log("MIDNIGHT RERENDER: " + new Date(dayStart).toLocaleString() +"passed, "+ new Date().toLocaleString() + " began.");
          dayStart = new Date().setHours(0,0,0,0);
        }
      }; 
      
      clearTimeout(this.clearCheckMidnight);
      this.clearCheckMidnight = setInterval(checkMidnight, 2000);
    },

    prerenderedTimescale: function() {
      var h, days = this.days, nodes = "";

      while(days--){
        for (h = 0; h < 24; h++){
          nodes +="<i class='timescale_hh'>"+(h < 10 ? "0"+h : h)+"</i>";
        };
      }

      return nodes;
    },

    renderClock: function(updWidth){
      if (typeof window.scrl != "undefined") {window.scrl.destroy()};
      // if (typeof scrl != "undefined") {scrl.destroy(); console.log("sx")};
      var self = this, el = $(this.el).find("#clock"), scrl, scrlChecked = false, putClock, checkPosition;

      putClock = function(){
        var d = new Date()
          , _dayStart = new Date().setHours(0,0,0,0)
          // , mins = d.getMinutes()
          // , hours = d.getHours()
          , clockWidth = self.zoomFactor*(d.getTime() - _dayStart + (self.days-1)*86400000)/60000
          // , timebarWidth = self.zoomFactor*Math.round(self.days*86400000/6000)/10
          ;

        el.css("width", clockWidth + 'px')

        // if (mins < 10) mins = "0"+mins;
        // el.find("span").html(mins);

        // $(".timescale_hh").removeClass("curr").eq(hours+(days-1)*24).addClass("curr");

        checkPosition(clockWidth);
      };

      checkPosition = function(width){
        if (scrlChecked) return;
        // IScroll should be initialized after the DOM is ready
        // if (!window.scrl) {
          // console.log(width)
          document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
          window.scrl = scrl = new IScroll('#timebar-wrap', { 
            mouseWheel: false, 
            scrollX: true, 
            scrollY: false,
            bounce: false 
          });
        // };

        $('#timebar').on('mousedown', ".prevent-scroll", function(e){
          e.stopPropagation();
          scrl.disable();
        });

        if (updWidth) {
          var scrollToValue = self.scrollToValue, dur = 0;
          // console.log(scrollToValue)
        } else {
          var scrollToValue = 70-Math.round(width), dur = 200;
        }
        scrl.scrollTo(scrollToValue, 0, dur); // Position timebar to 70 minutes before current time
        scrlChecked = true;
      };      

      putClock();
      clearInterval(this.clearputClock);
      this.clearputClock = setInterval(putClock, 5000);
    }

  });

}); 