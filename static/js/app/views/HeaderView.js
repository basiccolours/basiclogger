define([ 'marionette', 'handlebars', 'templates', "userConfig"],
	function (Marionette, Handlebars, templates, config) {
	    return Marionette.ItemView.extend({
	    	
	    	template: Handlebars.compile('<div></div>'),

	    	el: $("#header-menu"),

	    	initialize: function(){
	    		this.sessions = this.options.sessions;
	    		this.activities = this.options.activities;
	    	},

	        events: {
	        	'click #export-data': 'exportData'
	        },

	        exportData: function(){
				var filename = config.userId()+"_"+config.appName()+"_backup_"+new Date().toJSON()+".txt";
				var data = {Sessions: this.sessions.toJSON(), Activities: this.activities.toJSON(), saved_at: Date.now()};
				var data = JSON.stringify(data);

				var blob = new Blob([data], {type: "text/plain;charset=utf-8"});
				saveAs(blob, filename);				
	        }

	    });
	});