define([
    'utils'
  , 'templates'
  , 'marionette'
  , 'handlebars'
  , 'userConfig'
], function(utils, templates, marionette, Handlebars, config) {
  return Marionette.ItemView.extend({
    tagName: 'div',

    className: 'acts-list-item',

    template: Handlebars.compile(templates.activitiesListItem),

    templateDisplay: Handlebars.compile(templates.timerDisplay),

    ui: {
      display: ".timer-display",
      title: ".acts-list-item_title span",
      colorbar: ".acts-list-item_color-bar",
      tags: ".acts-list-item_tags"
    },    

    events: {
        'click .delete-act': 'deleteActivity'
      , 'click .toggle-timer': 'startStop'
      , 'click .togglefull': 'toggleFullView'
      , 'change .timebar-track-selector select': 'trackSelect'
      , 'click .acts-list-item_tags li': 'switchTag'
      , 'click .edit-act': 'editWidget'
      , 'mouseenter ': 'highlightTimebarSessions'
      , 'mouseleave ': 'unhighlightTimebarSessions'
      , 'change .archive-activity': 'toggleArchive'
    // , 'change .check-task': 'toggle'
    },

    initialize: function(options) {
      this.sessions = this.options.sessions;
      this.sessions.on('all', this.sessionProcessEvent, this);
      this.active = this.options.active;


    },

    modelEvents: {
      "change:title": "changedTitle",
      "change:color": "changedColor",
      "change:tags": "changedTags"
    }, 

    changedTitle: function(){
      this.ui.title.html(this.model.get("title"));
    },

    changedColor: function(){
      this.ui.colorbar.css({"background-color": utils.color(this.model)});
    },

    changedTags: function(){
      var tags = this.model.get("tags");
      tags = tags.map(function(t){return '<li class="+t+">'+t+'</li>'})
      if (tags.length > 0) {
        tags.unshift('<li>#</li>')
      }
      this.ui.tags.html(tags.join(""));
    },

    switchTag: function(){
      var id = $(event.target).text(); 
      $("#"+id).trigger("click");
    },

    toggleArchive: function(e){
      var state = e.target.checked;
      this.model.save({archived: state});
      $(this.el).toggleClass("archived");
    },

    highlightTimebarSessions: function(){
      var els = $("#timebar .act_id-"+ this.model.id);
      els.attr('class', function(index, classNames) {
        if (classNames.match("highlight") !== null) return classNames;
        return classNames + ' highlight';
      });
    },
    
    unhighlightTimebarSessions: function(){
      var els = $("#timebar .act_id-"+ this.model.id);
      els.attr('class', function(index, classNames) {
          return classNames.replace(' highlight', '');
      });
    },
    
    serializeData: function(){
      return {
        model: this.model,
        title: this.model.get("title"),
        tags: this.model.get("tags"),
        archived: this.model.get("archived") || false,
        color: utils.color(this.model)
      }
    },

    onBeforeClose: function(){
      this.collection.off(null, null, this);
      this.sessions.off(null, null, this);
      this.model.off(null, null, this);
      this.toggleTimer(this.running = false)
    },

    onRender: function(){
      if (this.model.attributes.archived) {
        $(this.el).addClass("archived");
      }

      /* If this activity is not in sessions collection active subset
         then render an empty timer.
         Else toggle timer rendering and updating.
      */
      if (!_.contains(_.map(this.active, function(s){return s.get("activity")}), this.model.id)) {
          var today = utils.dhms(this.sessions.today(this.model.id, true), true);
          var total = utils.dhms(this.sessions.allByAct(this.model.id, true), true);
          var res = {hh: "0", mm: "00", ss: "00", today: today, total: total};
          this.ui.display.html(this.templateDisplay(res));
      } else {
        var activeList = this.sessions.active(this.model.id);
        /* Yet we don't handle the situation where simultaneously run several sessions, just a simple log statement. */
        if (activeList.length > 1) {console.debug("There is more than one activity (" +this.model.id+ ") running!")}
        var activeSession = activeList[0];
        this.toggleTimer(this.running = activeSession);
      };

    },

    sessionProcessEvent: function(eventName, session){
      if (session.get("activity") !== this.model.id) return; 
      switch(eventName){
        case 'add':
          if (session.get("end")) return;
          this.toggleTimer(this.running = session);
          break;
        case 'change:end':
        case 'remove':
          if (!(this.running && this.running.id === session.id)) return;
          this.toggleTimer(this.running = false);
          if (eventName === 'remove') this.ui.display.empty();
          break;
      }

    },

    toggleFullView: function(){
      // var self = this;

      $(this.el).toggleClass("opened");

      
    },
    
    editWidget: function(event){

      /* Render widget */
      var widg = $("#edit-widg"), model = this.model;
      if (widg.hasClass("on")) {return} else {widg.addClass("on")};
      $(event.target).addClass("on");
      var tmpl = Handlebars.compile(templates.editWidget);
      // widg.css({left: event.pageX+"px", top: event.pageY-20+"px"});

      Handlebars.registerHelper('thisColor', function(col) {
        if (col.toString().replace(/\D+/g, '') === utils.color(model).replace(/\D+/g, '')) return 'active';
      });

      var track = Math.max(this.model.get("track") || 1, 1);
      Handlebars.registerHelper('selectedTrack', function(ind) {
        if (ind == track) return 'active';
      });      

      screenEl = $('<div/>', {
          css: {position: "fixed", top: 0, left: 0, width: '100%', height: '100%', opacity: 0.6, zIndex: 31, backgroundColor: "white"}
      }).prependTo('body')
          .click(function (e) {
              e.preventDefault();
              e.stopPropagation();
              return false;
          });

      widg.html(tmpl({model: model, colors: config.colorSet, tracks: [1,2,3,4]}));

      var tagbox = $("#tagBox");
      var tagOptions = {"edit-on-delete": false, "forbidden-chars": ["?", " ", "&"], "no-comma": true};
      tagbox.tagging(tagOptions);
      tagbox.tagging("add", model.get("tags"));
      
      var newAttrs = {};

      /* Select color */
      $("#widg-colorset li").on("click", function(){
        $(this).siblings().removeClass("active");
        $(this).addClass("active");
        newAttrs.color = $(this).css("background-color");
      });

      /* Select track */
      $("#trackslist li").on("click", function(){
        $(this).siblings().removeClass("active");
        $(this).addClass("active");
        newAttrs.track = $(this).text();
      });

      /* Delete activity*/
      $("#delete-act").on("click", function(){
        if (confirm("This action can not be undone. Remove activity '"+model.get("title")+"'?")) {
          model.destroy();
          exitWidget();
        }
      });

      /* Save model */
      function exitWidget(){
        $(event.target).removeClass("on");
        widg.removeClass("on");
        widg.find("*").off();
        tagbox.tagging("reset");
        screenEl.remove();
      };

      function equalArrays(a, b) {
          return a.every(function (e, i) {return _.contains(b, e);}) && 
          b.every(function (e, i) {return _.contains(a, e);});
      }

      function saveData(){
        var _title = widg.find("input").val();
        if (_title !== model.get("title")) newAttrs.title = _title;

        var _tags = tagbox.tagging("getTags") || [];
        if (!equalArrays(_tags, (model.get("tags") || []))) {
          newAttrs.tags = _.map(_tags, function(t){return t.trim()});
        }

        if(_.size(newAttrs) > 0) model.save(newAttrs);
        
      };

      $("#save-widget-data").on("click", function(){
        saveData();
        exitWidget();
      });

      /* Close widget */
      var ESCAPE_KEY = 27;
      // var ENTER_KEY = 13;

      widg.on("keyup", function(e){
        if (e.keyCode == ESCAPE_KEY) exitWidget();
        // if (e.keyCode == ENTER_KEY) saveData();
      });

      $("#close-widget").on("click", function(){
        exitWidget();
      })
    },

    toggleTimer: function(session){
      if (session && session.get("end")) return;
      if (!session) {
        clearInterval(this.interval);
        this.interval = false;
        $(this.el).removeClass("running");
        return;
      }
      if (this.interval) return; // Timer is already running
      var time, today, total, res, self = this;
      function pushTime(){
        time = utils.dhms(session.duration());
        today = utils.dhms(self.sessions.today(self.model.id, true), true);
        total = utils.dhms(self.sessions.allByAct(self.model.id, true), true);
        res = {hh: time[0], mm: time[1], ss: time[2], today: today, total: total};
        self.ui.display.html(self.templateDisplay(res));
      }
      pushTime();

      this.interval = setInterval(pushTime, 1000);
      $(this.el).addClass("running");
    },

    startStop: function(){
      var activeSession = this.sessions.active(this.model.id)[0];
      if (activeSession) {
        activeSession.save({end: Date.now()}, {validate:true});
      } else {
        this.sessions.create({activity: this.model.id}, {validate:true});
      }
    },

    deleteActivity: function(){
      // this.close();
      this.toggleTimer(this.running = false)
      this.model.destroy();
    }

  });

});
