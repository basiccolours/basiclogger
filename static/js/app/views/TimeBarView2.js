define([
  'marionette',
  'templates',
  'utils',
  'd3'
], function (Marionette, templates, utils, d3) {

  return Marionette.View.extend({

    initialize: function() {
      this.activities = this.options.activities;
      this.sessions = this.options.sessions;
    },

    sessionRemoveDialog: function(model, title){
        this.notifier = this.notifier || new Backbone.Notifier({
          el: 'body',
          baseCls: 'notifier',
          theme: 'clean',
          types: ['warning', 'error', 'info', 'success'],
          type: null,
          dialog: false,
          modal: false,
          message: '',
          title: undefined,
          closeBtn: false,
          ms: 4000,
          'class': null,
          hideOnClick: true,
          loader: false,
          destroy: false,
          opacity: 1,
          // offsetY: -500,  
          fadeInMs: 500,
          fadeOutMs: 500,
          position: 'top',
          zIndex: 10000,
          screenOpacity: 0.5,
          width: undefined,
          // css: {'text-align': 'left'},
          modules: {}
        });

        var msg = 'Remove session <b>'+title+'</b>?'

        var s = utils.time24(model.get("start")), 
            e = model.get("end") ? utils.time24(model.get("end")) : 'NOW',
            dur = (model.get("end") || Date.now()) - model.get("start");
        
        if (dur > 60*60*24*1000) {
          s = new Date(model.get("start")).toLocaleDateString() + ' ' +s;
          e = (e !== 'NOW' ? new Date(model.get("end")).toLocaleDateString() + ' ' +e : e);
          msg += '<br /><small>started at: '+ s +'<br />till: '+ e +'</small>';
        } else {
          msg += '<br /><small>'+ s +'—'+ e +'</small>';
        }


        return (this.notifier.notify({
            message: msg,
            // 'type': 'info',
            modal: true,
            ms: null,
            destroy: false,
            buttons: [
              {'data-role': 'applyCallback', 'class': 'default', text: 'Sure'},
              {'data-role': 'closeThis', text: 'Cancel'}
            ],
            title: "Confirm session removal"
        }).on('click:closeThis', function() {
            this.destroy()
        }).on('click:applyCallback', function() {
            model.destroy();
            this.destroy();
        }))

    },    

    onDomRefresh: function() {
      // console.log("Timebar onDomRefresh fired");
      // this.renderClock();
    },

    render: function(){
      var self = this, runningSessions = [], a;
      var events = [], sessions = [];
      var pos, now;

      function convertSession(s, a){
        if (!s.get("end")) runningSessions.push(s.id);
        
        return {
              title: a.get("title") || '',
              track: parseInt(a.get("track")) || 1,
              id: s.id,
              start: s.get("start"),
              end: s.get("end"),
              act: s.attributes.activity,
              color: utils.color(a)
          }
      };

      function convertEvent(s, a){
        return {
              tweet: s.get("tweet").text,
              id: s.id,
              start: s.get("start"),
              end: s.get("end"),
              track: parseInt(a.get("track")) || 1,
          }
      };

      function isEvent(s){
        return s.get("tweet");
      }

      function _datasetAdd(s, returnVal){
        if (a = self.activities.get(s.attributes.activity)) {
          if (isEvent(s)) {
            if (returnVal) {
              return convertEvent(s, a) 
            } else {
              return events.push(convertEvent(s, a));
            } 
          } else {
            if (returnVal) {
              return convertSession(s, a) 
            } else {
              return sessions.push(convertSession(s, a));
            } 
          }
        }
      };

      self.sessions.models.forEach(function(s) {
        _datasetAdd(s);
      });
          
      function _datasetChange(model, destroyItem){
        function Fun(arr){
          arr.every(function(s, ind) {
            if (s.id == model.id) {
              if (destroyItem) {
                arr.splice(ind, 1);
              } else {
                var _m = _datasetAdd(model, true);
                for (var attr in _m) {
                    if (!_m.hasOwnProperty(attr)) continue;
                    arr[ind][attr] = _m[attr];
                }
              }
              return false;
            } else {
              return true;
            }
          });
        }

        Fun(isEvent(model) ? events : sessions);

      };

      var procEvents = {
        sessionAdded: function(model){
          _datasetAdd(model);
          redrawSubset();
        },

        sessionRemoved: function(model){
          _datasetChange(model, true);
          svg.select("#id-"+model.id).remove();
        },

        changedColor: function(model){
          (isEvent(model) ? events : sessions).forEach(function(s){
            if (s.act == model.id) {
              s.color = utils.color(model);
              var el = svg.select("#id-"+s.id);
              el.select(".session-body").style("fill", s.color);
            }
          });          
        },
        
        changedTitle: function(model){
          (isEvent(model) ? events : sessions).forEach(function(s){
            if (s.act == model.id) {
              s.title = model.get("title");
            }
          }); 
        },    
            
        changedStart: function(model){
          _datasetChange(model);
          redrawSubset();
        },

        trackChanged: function(model){
          (isEvent(model) ? events : sessions).forEach(function(s){
            if (s.act == model.id) {
              s.track = parseInt(model.get("track"));
            }
          });

          tracksMax = calcTracks();

          (isEvent(model) ? events : sessions).forEach(function(s){
            var el = svg.select("#id-"+s.id);
            el.select(".session-body").attr("y", function(){return s.y = calcSessY(s.track);});
            el.select(".dragleft").attr("y", function() {return s.y+(sessionH-dragbarH)/2; });
            el.select(".dragright").attr("y", function() {return s.y+(sessionH-dragbarH)/2; });
          });
          
          h = calcH();
          svg.attr("height", h-11);
          xaxis.tickSize(h-ticksPadding);
          svg.select("#clock-line-2").attr("height", h - ticksPadding)
          svg.select("g.xaxis").call(xaxis);
        },

        changedEnd: function(model){
          _datasetChange(model);
          if (model.get("end")) runningSessions = _.without(runningSessions, model.id);
          redrawSubset();
        }
      };

      this.listenTo(this.sessions, "add", procEvents.sessionAdded);
      this.listenTo(this.sessions, "change:end", procEvents.changedEnd);
      this.listenTo(this.sessions, "change:start", procEvents.changedStart);
      this.listenTo(this.sessions, "remove", procEvents.sessionRemoved);
      this.listenTo(this.activities, "change:track", procEvents.trackChanged);
      this.listenTo(this.activities, "change:color", procEvents.changedColor);
      this.listenTo(this.activities, "change:title", procEvents.changedTitle);

      var w = $(document).width(),
      dragbarw = 10,
      calcTracks = function(){var sM = d3.max(sessions, function(d){return d.track})  || 1; return events.length>0?sM+1:sM;},
      tracksMax = calcTracks(),
      sessionH = 16,
      dragbarH = sessionH*1.4,
      ticksPadding = 26,
      techSpace = 22,
      calcH = function(){
        var _h = tracksMax*dragbarH*1.2 + ticksPadding + techSpace;
        d3.select("#timebar").style("height", function(){return _h-11+"px"})
        d3.select("#bl-app").style("padding-top", function(){return _h+"px"})
        return _h;
      },
      h = calcH(),
      calcSessY = function(track){return (1 + tracksMax - track)*dragbarH*1.2 + track*(dragbarH - sessionH*1.2)/2 + techSpace - ticksPadding},
      newStart, el, oldx;

      var scale = d3.time.scale()
          .range([0, w]);

      var timebar = d3.select("#timebar");
      
      var svg = timebar.append("svg")
              .attr("width", w)
              .attr("height", h-11);

      function changeWidth(){
        var a = new Date(scale.domain()[0]).getTime();
        var b = new Date(scale.domain()[1]).getTime();        
        w = $(document).width();
        svg.attr("width", w);
        scale = d3.time.scale().range([0, w]);
        xaxis = d3.svg.axis()
        .scale(scale)
        .tickSize(h-ticksPadding)
        .tickFormat(customTimeFormat)
        .orient("bottom");
        setInitialZoom(a, b);
      }

      var lazyLayout = _.debounce(changeWidth, 150);
      jQuery(window).on('resize', lazyLayout);              

      var customTimeFormat = d3.time.format.multi([
        [".%L", function(d) { return d.getMilliseconds(); }],
        [":%S", function(d) { return d.getSeconds(); }],
        ["%H:%M", function(d) { return d.getMinutes(); }],
        ["%H", function(d) { return d.getHours(); }],
        ["%a %d", function(d) { return d.getDay() && d.getDate() != 1; }],
        ["%b %d", function(d) { return d.getDate() != 1; }],
        ["%B", function(d) { return d.getMonth(); }],
        ["%Y", function() { return true; }]
      ]);

      var xaxis = d3.svg.axis()
        .scale(scale)
        .tickSize(h-ticksPadding)
        .tickFormat(customTimeFormat)
        .orient("bottom");

      var zoom = null;

      var applyZoom = function(){
          // Re-apply the zoom behavior & recalc desirable zoomExtent.
          var diff = new Date(scale.domain()[1]).getTime()-new Date(scale.domain()[0]).getTime();           
          var Ex = [diff/32400000000, diff/162000];
          zoom = d3.behavior.zoom()
            .scaleExtent(Ex)
            .x(scale)
            .on("zoom", zoomed);
          svg.call(zoom);
      }

      setInitialZoom = function(a, b){
        scale.domain([a, b]);
        applyZoom();
        zoomed();
      }

      var filterByPeriod = function(collection, startPoint, endPoint){
        return collection.filter(function(s){
              return (s.start >= startPoint && s.start <= endPoint || // Started in specified period 
            s.end >= startPoint && s.end <= endPoint || // Ended in specified period
            s.start <= startPoint && s.end >= endPoint || // Started before & ended after the specified period
            s.start <= endPoint && !s.end); // Started before end point & is still active
        });
      }

      function redrawSubset(){
        var startPoint = scale.domain()[0].getTime()
        var endPoint = scale.domain()[1].getTime()

        var sessionsSubset = filterByPeriod(sessions, startPoint, endPoint);
        var eventsSubset = filterByPeriod(events, startPoint, endPoint);

        drawSessions(sessionsSubset);
        drawEvents(eventsSubset) ;
        updateSessions();
        updateDragBars();
        updateClock();        
      }

      function zoomed(){
        redrawSubset()
        svg.select("g.xaxis").call(xaxis).selectAll("text").style("font-size", "10px");
        updateClock();        
      };

      var drag = d3.behavior.drag()
          .origin(function(d) { return d; })
          .on("dragstart", dragstarted)
          .on("drag", dragged)
          .on("dragend", dragended);

      function dragstarted(d) {
        d3.event.sourceEvent.stopPropagation();
        if (!d.end) return;
        d3.select("#id-"+d.id).classed("dragging", true);
      }

      function dragged(d) {
        if (!d.end) return;
        d3.select(this).attr("x", d.x = Math.max(0 - d.width + 1, Math.min(w - 1, d3.event.x)));
        newStart = (scale.invert(d.x)).getTime(); 
        d.end = d.end - d.start + newStart;
        d.start = newStart;
        updateDragBars();
      }

      function dragended(d) {
        d3.select("#id-"+d.id).classed("dragging", false); saveSession(d);
      }

      function saveSession(d){
        var s = self.sessions.get(d.id);
        if (s.get("start") == d.start && s.get("end") == d.end) return;
        s.save({start: d.start, end: d.end}, {validate: true})
      }

      var dragleft = d3.behavior.drag()
        .origin(function(d) { return d; })
        .on("dragstart", function(d){d3.event.sourceEvent.stopPropagation(); d3.select("#id-"+d.id).classed("dragging", true);})
        .on("drag", ldragresize)
        .on("dragend", function(d){d3.select("#id-"+d.id).classed("dragging", false); saveSession(d)});

      var dragright = d3.behavior.drag()
        .origin(function(d) { return d; })
        .on("dragstart", function(d){d3.event.sourceEvent.stopPropagation(); d3.select("#id-"+d.id).classed("dragging", true);})
        .on("drag", rdragresize)
        .on("dragend", function(d){d3.select("#id-"+d.id).classed("dragging", false); saveSession(d)});

      function ldragresize(d) {
        el = d3.select("#id-"+ d.id +"> .session-body");
        oldx = d.x, end = d.end || Date.now();
        // Never below parent boundaries, never go higher than endpoint:
        d.x = Math.max(0, w > d.width+oldx ? Math.min(oldx + d.width - 1, d3.event.x) : Math.min(d3.event.x, w-1));
        d.start = Math.min((scale.invert(d.x)).getTime(), end-1000);
        el.attr("x", d.x);
        d3.select(this).attr("x", function(d) { return d.x - (dragbarw / 2); });
        d.width = Math.max(1, d.width + (oldx - d.x));
        el.attr("width", d.width);
      }    

      function rdragresize(d) {
        el = d3.select("#id-"+ d.id +"> .session-body");
        d.width = Math.max(Math.min(w - d.x, d3.event.sourceEvent.pageX - d.x), d.x > 0 ? 1 : Math.min(0 - d.x, w - d.x));
        d.end = Math.max((scale.invert(d.x + d.width)).getTime(), d.start+1000);
        d3.select(this).attr("x",  d.x + d.width - (dragbarw/2));
        el.attr("width", d.width);
      }    


      svg.append("g")
          .attr("class", "xaxis")
          .call(xaxis)
          .selectAll("text")
          .style("font-size", "10px");

      var sessionsContainer = svg.append("g").attr("class", "sessions");


      var tooltipW = 168, tooltipL, tooltipMargin = 10, tooltipIsFlipped = false;

      var tooltip = d3.select("#bl-app").append("div")
        .attr("class", "tooltip")
        .style("width", tooltipW+"px");      

      var setTooltipL = function(eventX){
          if (tooltipIsFlipped) {
            tooltipL = eventX - tooltipW - tooltipMargin;
            if (tooltipL < tooltipMargin) {tooltipIsFlipped = false; return setTooltipL(eventX);}
          } else {
            tooltipL = eventX + tooltipMargin;
            if (w - eventX - tooltipMargin < tooltipW) {tooltipIsFlipped = true; return setTooltipL(eventX);}
          }
      };

      function compareDates(start, end){
        var a = new Date(start);
        var b = new Date(end);

        var format = d3.time.format('%H:%M');

        if (a.getYear() !== b.getYear()) {
          format = d3.time.format('%b %d %Y %H:%M');
        } else if (a.getMonth() !== b.getMonth()) {
          format = d3.time.format('%b %d %H:%M');
        } else if (a.getDate() !== b.getDate()) {
          format = d3.time.format('%a %d %H:%M');
        }

        return [format(a), format(b)];

      }

      function drawSessions(data){
        var sessionbars = sessionsContainer.selectAll(".session-item").data(data, function(d){return d.id});

        var sessionbarsEnter = sessionbars.enter()
            .append("g")
                .attr("class", "session-item")
                .attr("id", function(d){return "id-"+d.id})
                .on("mouseover", function(){return tooltip.style("display","block");})
                .on("mousemove", function(d){
                  setTooltipL(d3.event.pageX);
                  var end = d.end || Date.now();
                  var datePoints = compareDates(d.start, end);
                  tooltip.html('<b>' + d.title + '</b><br/>' + utils.dhms(end-d.start, true) + '<br/><small>'+ datePoints[0] + ' — ' + datePoints[1] +'</small>');
                  return tooltip.style("top", (d3.event.pageY+sessionH)+"px").style("left",tooltipL+"px");
                })
                .on("mouseout", function(){tooltipIsFlipped = false; return tooltip.style("display", "none");})
                .on("dblclick", function(d){d3.event.stopPropagation(); self.sessionRemoveDialog(self.sessions.get(d.id), d.title)});                

        sessionbarsEnter.append("rect")
              .attr("class", function(d){return "session-body "+"act_id-"+d.act})
              .call(drag)
              .attr("y", function(d, i){return d.y = calcSessY(d.track);})
              .attr("height", sessionH)
              .style("fill", function(d){return d.color;});

        sessionbarsEnter.append("rect")
            .attr("class", "dragleft")
            .attr("height", dragbarH)
            .attr("width", dragbarw)
            .attr("y", function(d) { return d.y+(sessionH-dragbarH)/2; })
            .attr("cursor", "ew-resize")
            .call(dragleft);

        sessionbarsEnter.append("rect")
            .attr("class", "dragright")
            .attr("display", function(d){if (!d.end) return "none"})
            .attr("height", dragbarH)
            .attr("width", dragbarw)
            .attr("y", function(d) { return d.y+(sessionH-dragbarH)/2; })
            .attr("cursor", "ew-resize")
            .call(dragright);

        sessionbars.exit()
          .remove();
      }

      function drawEvents(data){
        var sessionbars = sessionsContainer.selectAll(".event-item").data(data, function(d){return d.id});

        sessionbars.enter()
            .append("circle")
                .attr("class", "event-item")
                .attr("cy", function(d, i){return calcSessY(tracksMax) + 6;})
                .attr("id", function(d){return "id-"+d.id})
                .attr("r", function(d){return sessionH/2 -2})
                .on("mouseover", function(){return tooltip.style("display","block");})
                .on("mousemove", function(d){
                  var left = (d3.event.pageX+10);
                  if (w - d3.event.pageX < tooltipW) {
                    left = d3.event.pageX - tooltipW - 10;
                  }
                  var end = d.end || Date.now(), content;
                  tooltip.html(d.tweet + '<br/><i>'+ (new Date(d.start).toLocaleString()) + '</i>');
                  return tooltip.style("top", (d3.event.pageY+sessionH -35)+"px").style("left",left+"px");
                })
                .on("mouseout", function(){return tooltip.style("display","none");});                

        sessionbars.exit()
          .remove();
      }



      function updateSessions(){
        svg.selectAll(".session-body")
          .attr("x", function(d){return d.x = scale(d.start);})
          .attr("width", function(d){return d.width = Math.max(0, scale(d.end || Date.now()-1000)-scale(d.start))});

        svg.selectAll(".event-item")
          .attr("cx", function(d){return d.x = scale(d.start) - sessionH/2;})
      }

      function updateRunningSessions(end){
        if (runningSessions.length == 0) return;

        runningSessions.forEach(function(s){
          svg.select("#id-"+s +" > .session-body")
            .attr("width", function(d){return d.width = Math.max(scale(end)-scale(d.start) +1, 1);})

        })
      }

      function updateDragBars(){
        svg.selectAll(".dragleft")
          .attr("x", function(d) { return d.x - (dragbarw/2); })      

        svg.selectAll(".dragright")
          .attr("display", function(d){if (d.end) {return " "} else {return "none"}})
          .attr("x", function(d) { return d.x + d.width - (dragbarw/2); })
      }


      /* Clock */
      svg.append("rect")
        .attr("y", 0)
        .attr("width", 2)
        .attr("height", 7)
        .attr("class", "clock-line")
        .attr("id", "clock-line-1");

      svg.append("rect")
        .attr("y", 21)
        .attr("width", 2)
        .attr("height", h)
        .attr("class", "clock-line")
        .attr("id", "clock-line-2");

      svg.append("text")
        .attr("y", 17)
        .style("font-size", 10)
        .attr("text-anchor", "middle")
        .attr("id", "clock-text");

      function updateClock(){
        now = Date.now();

        pos = scale(now);
        // svg.select("#clock-bg").attr("x", pos);
        svg.select("#clock-line-1").attr("x", pos);
        svg.select("#clock-line-2").attr("x", pos);
        svg.select("#clock-text")
          .text(utils.time24(now))
          .attr("x", pos);

        updateRunningSessions(now);
      };

      /* Fix to clock */
      var domainOffset = 0, paddingR = 20, pointR;
      function posBar(){
        pointR = Date.now() + scale.invert(w).getTime() - scale.invert(w-paddingR).getTime();
        domainOffset = new Date(scale.domain()[1]).getTime() - new Date(scale.domain()[0]).getTime();
        scale.domain([pointR-domainOffset, pointR]);
        applyZoom();
        zoomed();
      }

      setInterval(updateClock, 1000)
      // setInterval(posBar, 600)
      
      // last 8 hours 8*60*60*1000 = 28800000, 1 h right padding
      return setInitialZoom(Date.now()-28800000, Date.now()+3600000);
    }

  });

}); 