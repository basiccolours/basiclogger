define([
    'utils'
  , 'templates'
  , 'marionette'
  , 'handlebars'
], function(utils, templates, marionette, Handlebars) {
  return Marionette.ItemView.extend({
    
    tagName: 'div',
    
    // className: 'timebar-session-wr',

    template: Handlebars.compile(templates.timebarActivity),

    initialize: function(options) {
      // this.collection = this.options.sessions;
      this.zoomFactor = this.options.zoomFactor || 1;
      this.activity = this.options.activities.get(this.model.get("activity"));
      this.days = this.options.days;
      if (!this.activity) {
        this.render = function(){return}; // Seems hackish?
        return;
      }
      this.color = this.activity.get("color") || utils.color(this.activity.id);
      this.title = this.activity.get('title');
      this.listenTo(this.activity, "change:color", this.colorChanged);
      this.listenTo(this.activity, "change:track", this.trackChanged);
      this.listenTo(Backbone, "zoom", this.zoom);
    },

    ui: {
      session: '.timebar-session',
      parentEl: '#timebar-sessions'
    },    

    events: {
      'click .timebar-session .body': 'makeEditable',
      'click .timebar-session .remove': 'removeSession',
      // 'mouseover .timebar-session': 'enableStats'
    },

    serializeData: function(){
      return {
        model: this.model,
        title: this.title,
        color: this.color, 
        width: this.getWidth(),
        left: this.getLeft(),
        id: this.model.id,
        track: this.activity.get("track") || '0'
      }
    },

    modelEvents: {
      "change:end": "changedDuration",
      "change:start": "changedDuration"
    },
    
    trackChanged: function(model, track){
      this.ui.session.removeClass('track-0 track-1 track-2 track-3 track-4').addClass('track-'+track);
    },

    colorChanged: function(model, color){
      this.ui.session.find(".body").css({"background-color": color});
      this.ui.session.find(".drag-indicator").css({"background-color": color});
    }, 

    changedDuration: function(){
      var animationDuration = 450;
      if (this.ui.session.hasClass("editable")) {
        animationDuration = 0;
        if (this.hasDraggable) this.setDraggable();
      }
      this.ui.session.animate({"left": this.getLeft(), "width": this.getWidth()}, animationDuration).css('overflow', 'visible');
    },

    getWidth: function(){
      var width = this.zoomFactor*this.model.duration()/60000;
      return (width > 1 ? width: 1)
    },

    getLeft: function(){
      var S = this.model.get("start");
      return this.zoomFactor*(S - (new Date().setHours(0,0,0,0)) + (this.days-1)*86400000)/60000;
    },

    zoom: function(z) {
      var self = this;
      this.zoomFactor = z;
      this.ui.session.css({width: self.getWidth(), left: self.getLeft()});
    },

    onRender: function(){

      /* Render running indicator and update session width */
      if (this.model.get("end")) return;
      var el = $(this.el).children(), self = this, interval, stopRunning, updWidth;
      el.addClass("running");

      stopRunning = function(){
        clearInterval(interval);
        el.removeClass("running");
      };
      this.listenToOnce(this.model, "change:end", stopRunning);

      updWidth = function(){return el.width(self.getWidth())};
      interval = setInterval(updWidth, 8000);
    },

    onDomRefresh: function(){
      this.enableStats();
    },
    
    enableStats: function(){
      if (this.ui.session.hasClass("editable")) return;
      var self = this, runningUpdInt, content;
      
      content = function(){
        var dot = '';
        if (!self.model.get("end")){
          dot = "<div class='red-dot'></div>";
          var updDur = function(){
            $("#tooltip-"+self.model.id+" b").html(utils.dhms(self.model.duration(), true, true));
            if(self.model.get("end")) {clearInterval(runningUpdInt); $("#tooltip-"+self.model.id+" .red-dot").remove();}
          };
          runningUpdInt = setInterval(updDur, 200);
        };
        return "<div id='tooltip-"+self.model.id+"'><h3>"+self.activity.get('title')+ dot +"</h3><p>Duration: <b>"+utils.dhms(self.model.duration(), true, false)+"</b></p></div>"
      };
      
      this.ui.session
      .tooltip({ 
        track: true, 
        content: content, 
        position: { 
          my: "left top+40",
          at: "center", 
          collision: 'fit'
        }, 
        items: ".body",
        close: function() {return clearInterval(runningUpdInt)}
      });

    },

    pxToTime: function(left, right){
      var days = this.days;
      var calcT = function(val){return Math.round(val*60000 + (new Date().setHours(0,0,0,0)) - (days-1)*86400000);};

      // compensating deriviations or maybe be to make snap_to_zero?
      var accuracyEnd = -71000;  
      var accuracyStart = -11000; 

      var start = calcT(left) + accuracyStart;
      var end = calcT(right) + accuracyEnd;

      // var previousStart = this.model.get("start");
      // start = start + parseFloat(previousStart.toString().slice(-5));
      // var previousEnd = this.model.get("end");
      // if (previousEnd) end = end + parseFloat(previousEnd.toString().slice(-5));

      return [start, end];
    },

    makeEditable: function(){
      var el = this.ui.session;
      if (el.hasClass("running")) return;

      el.toggleClass("editable");

      if (el.hasClass("editable")) {
        el.tooltip( "disable" );
        this.setDraggable();
      } else {
        el.tooltip( "enable" );
        scrl.enable();
      }

    },

    setDraggable: function(){
      this.hasDraggable = true;

      var el = this.ui.session
        , self = this
        , startEnd
        , containmentDiv = $('<div id="div_containment"></div>')
        , parentEl = $('#timebar-sessions')
        , left = el.find(".left")
        , right = el.find(".right")
        , leftTime = el.find(".left-time")
        , rightTime = el.find(".right-time")
        , totalTime = el.find(".total-time")
        , totalTimeDisplay = el.find(".total-time-display");

      var elWidth = el.width()
        , elLeft = el.position().left;

    
      var convT = function(t){
        var o, res, i;
        o = new Date(t);
        res = [o.getHours(), o.getMinutes()/*, o.getSeconds()*/];
        for (i = 0; i < res.length; i++){
          if (res[i] < 10) res[i] = '0' + res[i];
        };
        return res.join(":");
      };

      var showTime = function(startEnd, left, width, right, leftDrag){
        var elMinWidth = 36, marginLeft = 0, marginRight = 0, t;
        if (!startEnd) startEnd = [self.model.get("start"), self.model.get("end")];
        t = [convT(startEnd[0]), convT(startEnd[1]), utils.dhms(startEnd[1]-startEnd[0], true, false)];

        if (width < elMinWidth) {marginLeft = width - elMinWidth/2; width = elMinWidth; if (leftDrag) {marginRight=marginLeft}} 
        leftTime.html(t[0]).css({"left": left});
        rightTime.html(t[1]).css({"right": right});
        totalTimeDisplay.html(t[2]);
        totalTime.css({"width": width, "margin-left": marginLeft});
        if (leftDrag) {totalTime.css({right: 0, left: '', "margin-right": marginRight})} else {totalTime.css({left: 0, right: ''})};
      };

      showTime(false, 0, elWidth, 0, true);

      left.draggable({ 
        axis: "x",
        containment: '#div_containment',
        helper: function( event, ui ) {
          containmentDiv.css({'left': 0, 'width': elLeft+elWidth-2})
          containmentDiv.appendTo(parentEl);
          return $(this).clone()
        },
        start: function( event, ui ) {
          $(this).addClass("transp")
        },
        drag: function( event, ui ) {
          var left = 0, width = Math.abs(ui.position.left);
          if (ui.position.left < 0) {left = ui.position.left;}
          $(this).find(".drag-indicator").css({width: width, left: left});
          startEnd = self.pxToTime(elLeft+ui.position.left, elWidth+elLeft);
          showTime(startEnd, ui.position.left, elWidth-ui.position.left, 0, true);
        },
        stop: function( event, ui ) {
          $(this).removeClass("transp")
          $(this).find(".drag-indicator").css({width: 0});
          scrl.enable();
          elLeft = elLeft+ui.position.left;
          elWidth = elWidth-ui.position.left;
          el.css({left: elLeft, width: elWidth});
          $('#div_containment').remove();
          leftTime.css({"left": 0});
          self.model.save({start: startEnd[0], end: startEnd[1]});
        }
      });

      right.draggable({ 
        axis: "x",
        containment: '#div_containment',
        helper: function( event, ui ) {
          containmentDiv.css({'left': elLeft+2, 'width': parentEl.width()-elLeft-2});
          containmentDiv.appendTo(parentEl);
          return $(this).clone()
        },
        start: function( event, ui ) {$(this).addClass("transp")},
        drag: function( event, ui ) {
          var left = 0, width = ui.position.left-ui.helper.context.offsetLeft;
          if (ui.position.left < ui.helper.context.offsetLeft) {
            width = ui.helper.context.offsetLeft-ui.position.left; 
            left = -width;
          }
          $(this).find(".drag-indicator").css({width: width, left: left});
          startEnd = self.pxToTime(elLeft, elLeft+elWidth+ui.position.left-ui.helper.context.offsetLeft);
          showTime(startEnd, 0, elWidth+ui.position.left-ui.helper.context.offsetLeft, elWidth-ui.position.left);
        },
        stop: function( event, ui ) {
          $(this).removeClass("transp");
          $(this).find(".drag-indicator").css({width: 0});
          elWidth = elWidth+ui.position.left-ui.helper.context.offsetLeft;
          scrl.enable();
          el.css({width: elWidth});
          $('#div_containment').remove();
          rightTime.css({"right": 0});

          self.model.save({start: startEnd[0], end: startEnd[1]});

        }
      });


    },

    removeSession: function(){
      if (confirm("The session will be deleted")) {
        // if ($(this.ui.parentEl).find(".editable").length == 0) scrl.enable();
        this.model.destroy();
      }
    }



  });

});
