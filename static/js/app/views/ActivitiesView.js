/*global define */

define([
  'marionette',
  'templates',
  'views/ActivityView'
], function (Marionette, templates, ActivityView) {

  return Marionette.CompositeView.extend({
    template: Handlebars.compile(templates.activitiesList),
    
    templateTags:  Handlebars.compile(templates.tagsList),

    itemView: ActivityView,

    itemViewContainer: '#acts-container',

    initialize: function() {
      this.sessions = this.options.sessions;
      this.activities = this.options.activities;
      this.selectedTags = [];
      this.collection = new Backbone.VirtualCollection(this.activities, {
        filter: function (activity) {
          return true;
        }
      }); 

      this.updFilter();

      this.activities.on("change:tags", this.refreshTags, this);
      this.activities.on("destroy", this.refreshTags, this);
    },

    serializeData: function(){
      return {tagsList: this.tagsList}
    },

    ui: {
      input: '#act-input',
      tags: '#tags-list',
    },

    events: {
      'keypress #act-input': 'onInputKeypress',
      'click #act-input-btn': 'onInputKeypress',
      'click .switch-tag': 'sortByTag',
      'click #clear-tags': 'clearTags',
      'change #show-archived': 'toggleShowAlsoArchived',
      'change #show-archived-only': 'toggleShowOnlyArchived'
    },

    itemViewOptions: function() {
        return {sessions: this.sessions, active: this.sessions.active(), collection: this.collection}
    },

    onRender: function(){
      this.refreshTags()
    },

    refreshTags: function(model, tags){
      this.tagsList = _.union(_.flatten(this.activities.map(function(s){return s.get("tags") || []})));
      var tags = this.templateTags({tagsList: this.tagsList}), tagsui = this.ui.tags;
      tagsui.html(tags);
      if (!this.selectedTags) {this.selectedTags = []; return;}
      if (this.selectedTags.length !== 0 && _.intersection(this.selectedTags, this.tagsList).length == 0 ) {this.clearTags(); return}
      this.selectedTags.forEach(function(t){
        tagsui.find("#"+t).addClass("selected");
      });
      if (this.selectedTags.length !== 0) {this.ui.tags.addClass("has-selected")} else {this.ui.tags.removeClass("has-selected")};
    },

    sortByTag: function(e){
      if (e) {
        $(e.target).toggleClass("selected");
        var tag = e.target.innerText;
        if (this.selectedTags.indexOf(tag) !== -1) {
          this.selectedTags = _.without(this.selectedTags, tag);
        } else {
          this.selectedTags.push(tag)
        }
        if (_.intersection(this.selectedTags, this.tagsList).length == 0) {this.selectedTags = [];}
      }

      this.updFilter();

      if (this.selectedTags.length !== 0) {
        this.ui.tags.addClass("has-selected");
      } else {this.ui.tags.removeClass("has-selected")};
    },

    updFilter: function(){
      var self = this,
          filterHasTags = (self.selectedTags.length == 0);

      this.collection.updateFilter(function (model) {
        var tagCondition = _.intersection(model.attributes.tags, self.selectedTags).length != 0,
            archivedCondition = model.attributes.archived;

        if (self.showOnlyArchived) {
          if (filterHasTags) return archivedCondition;
          return tagCondition && archivedCondition;
        } else if (self.showAlsoArchived) {  
          if (filterHasTags) return true;
          return tagCondition;
        } else {  
          if (filterHasTags) return !archivedCondition;
          return tagCondition && !archivedCondition;
        }
      });
      
      if (this.selectedTags.length !== 0) {
        this.selectedTags.forEach(function(tag){
          $("."+tag).addClass("selected");
        });
      }
    },

    toggleShowAlsoArchived: function(e){
      $(e.target).toggleClass("selected");
      this.showAlsoArchived = $(e.target).hasClass("selected");
      if (this.showAlsoArchived) {
        $("#show-archived-only-b").show();
      } else {
        this.showOnlyArchived = false;
        $("#show-archived-only").removeClass("selected").attr("checked", false);
        $("#show-archived-only-b").hide();
      }
      this.updFilter();
    },

    toggleShowOnlyArchived: function(e){
      $(e.target).toggleClass("selected");
      this.showOnlyArchived = $(e.target).hasClass("selected");
      this.updFilter();
    },


    clearTags: function(){
      $(".switch-tag").removeClass("selected");
      this.selectedTags = [];
      this.sortByTag();
    },

    onInputKeypress: function (event) {
      var ENTER_KEY = 13
        , title = this.ui.input.val().trim() || "New activity"
        ;

      if (event.which === ENTER_KEY || event.type === 'click') {
        this.activities.create({
          title: title
        }, {validate:true});

        this.ui.input.val('');
      }
    },

	
	});
});
