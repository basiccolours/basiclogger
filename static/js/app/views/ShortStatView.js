define([
        'marionette',
        'handlebars',
        'templates',
        "userConfig",
        "utils",
        "d3.v3",
        "nv.d3",
        "legend",
        "pie",
        "pieChart",
        "chart-utils"
    ],
    function(Marionette, Handlebars, templates, config, utils) {
        return Marionette.ItemView.extend({

            template: Handlebars.compile('<p id="menu"><b>Choose</b><br>Age: <select></select><p id="chart"><div class="with-3d-shadow with-transitions"><div id="calc-data">#calc-data</div> <div id="show-tags-donut">#showTags</div> <div id="showActs">#showActs</div><svg id="test2" class="mypiechart"></svg></div>'),

            // el: $("#header-menu"),

            initialize: function() {
                this.sessions = this.options.sessions;
                this.activities = this.options.activities;
            },

            events: {
                'click #calc-data': 'drawBars',
                'click #show-tags-donut': 'showTags',
                'click #showActs': 'calcData2'
            },

            calcData: function() {
                var self = this;

                var activitiesByTags = function() {
                    return _.reduce(self.activities.models, function(result, a) {
                        _.forEach(a.attributes.tags, function(tag) {
                            result[tag] && result[tag].length > 0 ? result[tag].push(a.id) : result[tag] = [a.id];
                        });
                        return result;
                    }, {});
                }

                var uncountTime = 24 * 60;

                var sessionsCount = function() {
                    return _.reduce(activitiesByTags(), function(result, tag, key) {
                        var val = _.reduce(tag, function(result, act) {
                            result += parseInt(self.sessions.today(act, true) / 1000 / 60, 10);
                            return result;
                        }, 0);
                        uncountTime -= val;
                        if (val > 0) {
                            result.push({
                                key: key,
                                y: val
                            })
                        };
                        return result;
                    }, []);
                };

                var sessArr = sessionsCount();

                sessArr.push({
                    key: "not count time",
                    y: uncountTime
                });
                // console.debug(JSON.stringify(sessArr));
                return sessArr;
            },

            calcData2: function(period) {
                var self = this,
                    dayOffset = 24 * 60 * 60 * 1000,
                    startPoint = new Date().setHours(0, 0, 0, 0);

                if (period == "yesterday") {
                    startPoint = startPoint - dayOffset
                } else if (period == "the day before yesterday") {
                    startPoint = startPoint - dayOffset * 2
                }

                var endPoint = startPoint + dayOffset;
                var sessions = this.sessions.byPeriod(startPoint, endPoint);

                var timeCalc = function(session) {
                    var start = session.attributes.start < startPoint ? startPoint : session.attributes.start;
                    var end = session.attributes.end ? (session.attributes.end > endPoint ? endPoint : session.attributes.end) : Date.now();

                    return end - start;
                };

                var actsCount = function() {
                    return _.reduce(sessions, function(result, s) {
                        var actId = s.get("activity");
                        if (self.activities.get(actId) !== void 0) {
                            if (!result[actId]) result[actId] = 0;
                            result[actId] += timeCalc(s);
                        }

                        return result;
                    }, {});
                };

                var buildResult = function() {
                    var a;
                    return _.reduce(actsCount(), function(result, ms, act) {
                        var a = self.activities.get(act);
                        result.push({
                            title: a.get("title") || '',
                            ms: ms/1000/60/60,
                            h: utils.dhms(ms, true),
                            id: act,
                            color: a.get("color") || utils.color(act)
                        });
                        return result;
                    }, [])

                };

                // var res = buildResult();
                // res.push({key: "uncountTime", y: uncountTime})
                // console.debug(JSON.stringify(res))

var map = BL.sessions.models.map(function(a){
  var act = BL.activities.get(a.attributes.activity);
  if (act) var title = act.get("title");
  var track = act.get("track") || 0;
  return {track: track, start: a.attributes.start, end: a.attributes.end, title: title || 'New', color: act.get("color") || utils.color(a.attributes.activity), time: utils.dhms(a.attributes.end-a.attributes.start, true)}
}); console.log(JSON.stringify(map), 111);

                return buildResult();

            },

            showTags: function() {
                var sessionsCount = this.calcData();

                this.drawDonut(sessionsCount);
            },

            showActs: function() {
                var sessionsCount = this.calcData2();
                // console.debug(JSON.stringify(sessionsCount));
                this.drawDonut(sessionsCount);
            },

            drawBars: function() {
                var self = this;

                var acts = this.calcData2("today").sort(function(a, b) {
                    return b.ms - a.ms;
                });

                var labelLeftMax = acts[0].title.length,
                    labelRightMax = acts[0].h.length;

                acts.forEach(function(a) {
                    labelLeftMax = labelLeftMax > a.title.length ? labelLeftMax : a.title.length;
                    labelRightMax = labelRightMax > a.h.length ? labelRightMax : a.h.length;
                });

                var margin = {
                    top: 20,
                    right: 40 + labelRightMax * 10,
                    bottom: 10,
                    left: 40 + labelLeftMax * 10
                },
                    width = 560,
                    height = acts.length * 11 + 150 - margin.top - margin.bottom;

                // var format = d3.format("f")
                //     age;

                var x = d3.scale.linear()
                    .range([0, width]);

                var y = d3.scale.ordinal()
                    .rangeRoundBands([0, height], .1);

                var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("top")
                    .tickSize(-height - margin.bottom);
                // .tickFormat(format);

                var svg = d3.select("#chart").append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                // .style("margin-left", -margin.left + "px")
                .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                svg.append("g")
                    .attr("class", "x axis");

                svg.append("g")
                    .attr("class", "y axis")
                    .append("line")
                    .attr("class", "domain")
                    .attr("y2", height);

                var menu = d3.select("#menu select")
                    .on("change", change);

                // d3.csv("http://localhost:18081/static/tmp/states-age.csv", function(data) {
                // var states = data;
                // console.log(JSON.stringify(states),age)


                // var ages = d3.keys(states[0]).filter(function(key) {
                //   return key != "State" && key != "Total";
                // });

                // states.forEach(function(state) {
                //   ages.forEach(function(age) {
                //     state[age] = state[age] / state.Total;
                //   });
                // });
                var ages = ["today", "yesterday", "the day before yesterday"];
                menu.selectAll("option")
                    .data(ages)
                    .enter().append("option")
                    .text(function(d) {
                        return d;
                    });

                menu.property("value", "today");

                redraw();
                // });

                var altKey;
                d3.select(window)
                    .on("keydown", function() {
                        altKey = d3.event.altKey;
                    })
                    .on("keyup", function() {
                        altKey = false;
                    });

                function change() {
                    // clearTimeout(timeout);

                    d3.transition()
                        .duration(altKey ? 7500 : 750)
                        .each(redraw);
                }

                function redraw() {
                    var period = menu.property("value");
                    acts = self.calcData2(period);
                    console.log(period, JSON.stringify(acts))
                    // top = states.sort(function(a, b) { return b[age1] - a[age1]; }).slice(0, 24);

                    // y.domain(top.map(function(d) { return d.title; }));
                    y.domain(acts.map(function(d) {return d.id;}));
                    // x.domain([0, top[0][age = age1]]);
                    x.domain([0, d3.max(acts, function(d) {return d.ms;})]);

                    var bar = svg.selectAll(".bar")
                        .data(acts);

                    var barEnter = bar.enter().insert("g", ".axis")
                        .attr("class", "bar")
                        .attr("transform", function(d) {return "translate(0," + (y(d.id) + height) + ")";})
                        .style("fill-opacity", 0);

                    barEnter.append("rect")
                        .attr("width", acts && function(d) {return x(d.ms);})
                        .attr("fill", acts && function(d) {return d.color;})
                        .attr("height", y.rangeBand());

                    barEnter.append("text")
                        .attr("class", "label")
                        .attr("x", -3)
                        .attr("y", y.rangeBand() / 2)
                        .attr("dy", ".35em")
                        .attr("text-anchor", "end")
                        .text(function(d) {return d.title;});

                    barEnter.append("text")
                        .attr("class", "h-label")
                        .attr("x", acts && function(d) {return x(d.ms) + 5;})
                        .attr("y", y.rangeBand() / 2)
                        .attr("dy", ".35em")
                        .attr("text-anchor", "start")
                        .text(function(d) {return d.h;});

                    var barUpdate = d3.transition(bar)
                        .attr("transform", function(d) {return "translate(0," + (y(d.id)) + ")";})
                        // .attr("transform", function(d) {return "translate(0," + (d.y0 = y(d.id)) + ")";})
                        .style("fill-opacity", 1);

                    barUpdate.select("rect")
                        .attr("width", function(d) {return x(d.ms);});

                    barUpdate.select(".label")
                        .attr("width", function(d) {return x(d.ms);});


                    var barExit = d3.transition(bar.exit())
                        // .attr("transform", function(d) {return "translate(0," + (d.y0 + height) + ")";})
                        .attr("transform", function(d) {return "translate(0," + height + ")";})
                        .style("fill-opacity", 0)
                        .remove();

                    barExit.select("rect")
                        .attr("width", function(d) {return x(d.ms);})

                    barExit.select(".h-label")
                        .attr("x", function(d) {return x(d.ms) + 5;})
                        .text(function(d) {return d.h;});

                    d3.transition(svg).select(".x.axis")
                        .call(xAxis);
                }

                // var timeout = setTimeout(function() {
                //   menu.property("value", "65 Years and Over").node().focus();
                //   change();
                // }, 5000);

            },

            drawDonut: function(sessionsCount) {
                return nv.addGraph(function() {

                    var width = 500,
                        height = 500;

                    var chart = nv.models.pieChart()
                        .x(function(d) {
                            return d.key
                        })
                    // .labelThreshold(.08)
                    .showLabels(true)
                        .labelType("percent") //Can be "key", "value" or "percent"        
                    .color(d3.scale.category10().range())
                        .width(width)
                        .height(height)
                        .donut(true);

                    chart.pie
                        .startAngle(function(d) {
                            return d.startAngle - Math.PI
                        })
                        .endAngle(function(d) {
                            return d.endAngle - Math.PI
                        });

                    // chart.pie.donutLabelsOutside(true).donut(true);

                    d3.select("#test2")
                        .datum(sessionsCount)
                        .transition().duration(1200)
                        .attr('width', width)
                        .attr('height', height)
                        .call(chart);

                    return chart;
                });
            }

        });
    });