define(["jquery","backbone","models/ActivityModel", "collections/ApiBaseCollection"],
  function($, Backbone, Model, ApiBaseCollection) {

    return ApiBaseCollection.extend({
      model: Model,
      
      dataType: function(){
        return 'activity';
      },

      initialize: function(){
      },

      comparator: function(activity) {
        return activity.get("created");
      }


  });

  });