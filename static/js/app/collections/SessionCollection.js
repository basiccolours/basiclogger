define(["jquery", "backbone", "models/SessionModel", "collections/ApiBaseCollection"],
    function($, Backbone, Session, ApiBaseCollection) {

        return ApiBaseCollection.extend({
            dataType: function() {
                return 'session';
            },

            model: Session,

            initialize: function() {
                Backbone.on("actRemoved", function(actId) {
                    this.removeByAct(actId)
                }, this);
            },

            comparator: function(session) {
                return session.get("start");
            },

            active: function(actId) {
                if (actId === void 0) {
                    return this.filter(function(session) {
                        return !session.get("end")
                    });
                } else {
                    return this.filter(function(session) {
                        return session.get("activity") == actId && !session.get("end")
                    });
                }
            },

            // All sessions since today midnight.
            // Filtered by an activity ID, if we pass it.
            // Otherwise, if @actId is null, counts sessions of all activities.
            // Returns either calced time or session objects array.
            today: function(actId, calced) {
                var startPoint = new Date().setHours(0, 0, 0, 0);
                var endPoint = startPoint + 24 * 60 * 60 * 1000;
                return this.byPeriod(startPoint, endPoint, actId, calced);
            },

            removeByAct: function(actId) {
                var arr = this.filter(function(session) {
                    return session.get("activity") === actId
                });
                for (var i = 0; i < arr.length; i++) {
                    //set 'neverRemote' option as remote DB itself destroys all corresponding sessions
                    arr[i].destroy({remote: false, neverRemote: true});
                }
                console.log("Removed " + arr.length + " sessions.");
            },

            allByAct: function(actId, calced) {
                var arr, start, end, time, _i, _len;

                arr = this.filter((function(session) {
                    return session.get("activity") == actId;
                }));

                if (!calced) {
                    return arr;
                } else {
                    time = 0;
                    for (_i = 0, _len = arr.length; _i < _len; _i++) {
                        start = arr[_i].get("start");
                        end = arr[_i].get("end") ? arr[_i].get("end") : Date.now();
                        time += end - start;
                    }

                    return time;
                }
            },

            byPeriod: function(startPoint, endPoint, actId, calced) {
                var now = Date.now();
                var _start, _end;
                var array = this.select(function(session) {
                    _start = session.attributes.start;
                    _end = session.attributes.end || Date.now();
                    return (actId ? session.get("activity") == actId : true) &&
                        (_start >= startPoint && _start <= endPoint ||  // Started in specified period 
                        _end >= startPoint && _end <= endPoint ||       // Ended in specified period
                        _start <= startPoint && _end >= endPoint)       // Started before & ended after the specified period
                });

                if (calced) {
                    var time = 0,
                        start, end, session, _i, _len;
                    for (_i = 0, _len = array.length; _i < _len; _i++) {
                        session = array[_i];
                        start = session.attributes.start < startPoint ? startPoint : session.attributes.start;
                        end = session.attributes.end ? (session.attributes.end > endPoint ? endPoint : session.attributes.end) : Date.now();
                        time += end - start;
                    }
                    return time;
                } else {
                    return array;
                }
            }

        });

    });