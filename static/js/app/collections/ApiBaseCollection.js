define(["jquery","backbone", "userConfig"],
  function($, Backbone, config) {

	return Collection = Backbone.Collection.extend({

    url: function(){return "/api/bulk/"+this.dataType()},

		fetch: function(options) {
			options || (options = {});
			options.data = { include_docs : true };
			options.method = 'POST';

			return Backbone.Collection.prototype.fetch.call(this, options);
		},

		storeName: function(){
			return config.userId()+'::'+this.dataType().toUpperCase()
		},

		parseBeforeLocalSave: function(resp, options) {
			// console.debug("collection parseBeforeLocalSave: ", this.url)
			if (resp.rows) resp = _.map(resp.rows, function(n) { return n.doc; });
			return resp;
		},

		parse: function(resp, options) {
			// console.debug("collection parse: ", resp)
			if (resp.rows) resp = _.map(resp.rows, function(n) { return n.doc; });
			return resp;
		}

    });

  });