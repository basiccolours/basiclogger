define([], function() {
    return {

        colorSet: [
            "rgb(107,18,41)", 
            "rgb(255, 223, 53)", 
            "rgb(228, 30, 30)", 
            "rgb(228, 160, 14)", 
            "rgb(199,97,183)", 
            "rgb(250,154,212)", 
            "rgb(18,60,107)", 
            "rgb(97,110,199)", 
            "rgb(164,154,250)", 
            "rgb(18,107,84)", 
            "rgb(97,189,199)", 
            "rgb(154,219,250)"
        ],

        userId: function() {
            return window.BasicLoggerCurrentUserID
        },

        appName: function() {
            return "Basic_Logger"
        }

    };
});
