define([
    'notifier',
    'jquery',
    'marionette',
    'collections/ActivityCollection',
    'collections/SessionCollection',
    'views/ActivitiesView',
    'views/TimeBarView2',
    // 'views/ShortStatView',
    'views/HeaderView'
], 

function (Notifier, $, Marionette, Activities, Sessions, ActivitiesView, TimeBarView, /*ShortStatView,*/ HeaderView) {

      var notifier = new Notifier({
        el: 'body',
        baseCls: 'notifier',
        theme: 'clean',
        types: ['warning', 'error', 'info', 'success'],
        type: null,
        dialog: false,
        modal: false,
        message: '',
        title: undefined,
        closeBtn: false,
        ms: 3000,
        'class': null,
        hideOnClick: true,
        loader: false,
        destroy: false,
        opacity: 1,
        // offsetY: -500,  
        fadeInMs: 500,
        fadeOutMs: 500,
        position: 'top',
        zIndex: 10000,
        screenOpacity: 0.5,
        width: undefined,
        // css: {'text-align': 'left'},
        modules: {}
    });

    var App = new Marionette.Application();
    var activities = App.activities = new Activities();
    var sessions = App.sessions = new Sessions();

    var viewOptions = {
        activities: activities,
        sessions: sessions
    };

    App.addRegions({
        header: '.header',
        activitiesList: '#acts-container-box',
        shortStat: '#stat-daily'
    });

    // FETCH CHANGES
    var last_seq = localStorage.getItem('last_seq');
    var changesURL = "/api/changes";
    var fOpts = {include_docs : true}; 
    var fetchOpts = function(){if (last_seq) {fOpts.since = last_seq}; return fOpts};
    var silentOpts = {remote: false, neverRemote: true, validate: false};
    var SYNCING = false;
    var SYNC_INTERVAL = 13000;
    var LAST_SYNC = 0;
    var syncIndicator = $("#sync-indicator");
    var syncIndicatorCleared = true;

    function parseChanges(arr) {
        var item, model, collection;
        for (var i = 0; i < arr.length; i++){
          item = arr[i].doc;
          model = activities.get(item._id) || sessions.get(item._id);
          if (item._deleted) {
            if (model) model.destroy(silentOpts);
          } else {
            if (model && model.get('_rev') !== item._rev) {
              model.save(item, silentOpts);
            } else if (!model) {
              if (item.type === 'session') {sessions.create(item, silentOpts)};
              if (item.type === 'activity') {activities.create(item, silentOpts)};
            }
          }
        };
    };

    function indicateDirty(){
      if (App.hasUnsynced()) {
        if (!syncIndicatorCleared) return;
        syncIndicatorCleared = false;
        // notifier.warning('There is unsynced local data');
        syncIndicator.css('opacity', '1');
      } else {
        if (syncIndicatorCleared) return;
        syncIndicatorCleared = true;
        // notifier.success('All local data send to server');
        syncIndicator.css('opacity', '0');
      }
    };

    function pushLocal (collection){
      if (collection.dirtyModels().length > 0) {collection.syncDirty()};
      if (collection.destroyedModelIds().length > 0) {collection.syncDestroyed()};
    }

    App.pushAllLocal = function(){
      $.when.apply($, [pushLocal(activities)]).then(function() {pushLocal(sessions);});
    }

    App.hasUnsynced = function(){
      return (sessions.dirtyModels().length > 0 || sessions.destroyedModelIds().length > 0 
          || activities.dirtyModels().length > 0 || activities.destroyedModelIds().length > 0);
    }

    App.fetchChanges = function () {
        // if (App.hasUnsynced()) return SYNCING = false;
        
        $.get(changesURL, fetchOpts())
            .success(function(resp) {
              if (!resp.last_seq || last_seq == resp.last_seq) return;
              last_seq = resp.last_seq;
              localStorage.setItem('last_seq', last_seq);
              return parseChanges(resp.results);
            });
    };

    function doSync(){
      if (Date.now() - LAST_SYNC < SYNC_INTERVAL) return indicateDirty();
      if (!SYNCING) {
        SYNCING = true;
        $.when.apply($, [App.pushAllLocal()]).then(function() {
          $.when.apply($, [App.fetchChanges()]).then(function() {LAST_SYNC = Date.now(); return SYNCING = false});
        });
      }
    }


    App.addInitializer(function () {
        var deferreds = [
            activities.fetch(), 
            sessions.fetch()
        ];

        $.when.apply($, deferreds).then(function() {

            var header = new HeaderView(viewOptions);
            var activitiesList = new ActivitiesView(viewOptions);
            var timeBar = new TimeBarView(viewOptions);
            // var shortStat = new ShortStatView(viewOptions);

            App.header.attachView(header);
            timeBar.render();
            // App.timeBar.show(timeBar);
            // App.shortStat.show(shortStat);
            App.activitiesList.show(activitiesList);
            
            setInterval(doSync, 500);
        });
        
    });


    return window.BL = App;
});
