require.config({
    baseUrl: "/static/js/app",
    // 3rd party script alias names (Easier to type "jquery" than "libs/jquery, etc")
    // probably a good idea to keep version numbers in the file names for updates checking
    paths: {
        // Core Libraries
        "requireLib": '../libs/almond',
        "jquery": "../libs/jquery",
        "underscore": "../libs/lodash",
        "backbone": "../libs/backbone",
        "marionette": "../libs/backbone.marionette",
        "handlebars": "../libs/handlebars-v1.3.0",
        "FileSaver": "../libs/FileSaver",
        "utils": "../app/utils",
        "userConfig": "../app/userConfig",
        "d3": "../libs/d3",
        "xsrf": "xsrf",
        "backbone.sync": "backbone.sync",
        "backbone.virtual-collection": "../libs/plugins/backbone.virtual-collection",
        "backbone.dualstorage": "../libs/plugins/backbone.dualstorage.fork",
        "notifier": "../libs/plugins/backbone.notifier",
        "bootstrap": "../libs/plugins/bootstrap",
        "text": "../libs/plugins/text",
        "tagging": "../libs/plugins/tagging.min"
    },
    // Sets the configuration for your third party scripts that are not AMD compatible
    shim: {
        "bootstrap": ["jquery"],
        "backbone": {
            "deps": ["underscore"],
            "exports": "Backbone"
        },
        "marionette": {
            "deps": ["underscore", "backbone", "jquery"],
            "exports": "Marionette"
        },
        "handlebars": {
            "exports": "Handlebars"
        },
        "backbone.sync": ["backbone"],
        "backbone.virtual-collection": ["backbone"],
        "backbone.dualstorage": ["backbone", 'backbone.sync', 'notifier'],
        "notifier": ["backbone", "jquery", "underscore"],
        "xsrf": ["jquery"],
        "tagging": ["jquery"]

    }
    // For easier development, disable browser caching
    // Of course, this should be removed in a production environment
    , urlArgs: 'bust=' +  (new Date()).getTime()    
});


require([
    'App',
    'backbone',
    'routers/AppRouter',
    'controllers/Controller',
    'backbone.sync',
    'backbone.dualstorage',
    'notifier',
    'backbone.virtual-collection',
    'xsrf',
    'FileSaver',
    'tagging'
], function(App, Backbone, Router, Controller) {
    'use strict';

    App.start();

    new Router({
        controller: new Controller()
    });

    Backbone.history.start();
});
