define(['jquery', 'backbone','utils', 'models/ApiBaseModel', 'd3'],
  function($, Backbone, utils, ApiBaseModel, d3) {
   return ApiBaseModel.extend({
      dataType: function(){
        return 'session';
      },

      initialize: function(attributes){
      },

      defaults: function(){ 
          return {
            _id: utils.guid(),
            start: Date.now(),
            type: this.dataType()
            // , end: null
          }
      },

      vDialog: function(model, msg, callback, callback2){
        this.notifier = this.notifier || new Backbone.Notifier({
          el: 'body',
          baseCls: 'notifier',
          theme: 'clean',
          types: ['warning', 'error', 'info', 'success'],
          type: null,
          dialog: false,
          modal: false,
          message: '',
          title: undefined,
          closeBtn: false,
          ms: 4000,
          'class': null,
          hideOnClick: true,
          loader: false,
          destroy: false,
          opacity: 1,
          // offsetY: -500,  
          fadeInMs: 500,
          fadeOutMs: 500,
          position: 'top',
          zIndex: 10000,
          screenOpacity: 0.5,
          width: undefined,
          // css: {'text-align': 'left'},
          modules: {}
        });

        var buttons = [{
          'data-role': 'closeThis', text: 'Ok'
        }];

        if (callback){
          buttons[0].text = 'Cancel';
          buttons[0].css = {margin: '0 20px'};
          
          if (callback2){
            buttons.unshift({'data-role': 'applyCallback2', text: 'Cut', css: {margin: '0 20px'}});
          }

          buttons.unshift({'data-role': 'applyCallback', text: 'Unite', css: {margin: '0 20px'}});
        }

        return (this.notifier.notify({
            message: msg,
            'type': 'error',
            modal: true,
            ms: null,
            destroy: false,
            buttons: buttons,
            title: "Session management conflict"
        }).on('click:closeThis', function() {
            if (model) {
              model.trigger("change:start", model);
              model.trigger("change:end", model);
            };
            this.destroy()
        }).on('click:applyCallback', function() {
            callback();
            this.destroy();
        }).on('click:applyCallback2', function() {
            callback2();
            this.destroy();
        }))
      },

      validate: function(attrs, options) {
        var msg = '';
        var end = attrs.end || Date.now();
        var start = attrs.start;
        var sessions = this.collection;

        if (end < start) {
          msg = "can't end before it starts";
          console.debug(msg); return msg;
        };
        
        if (!attrs.activity) {
          msg = "no activity is specified for this session";
          console.debug(msg); return msg;
        };


        var overlapped = _.filter(sessions.byPeriod(start, end, attrs.activity), function(s){return s.id !== attrs._id});
        var _m = sessions.get(attrs._id);

        if (overlapped.length > 0) {
          var maxEnd = d3.max(overlapped, function(d) { return d.attributes.end || Date.now();} );
          var minStart = d3.min(overlapped, function(d) { return d.attributes.start;} );


          var _pS = Math.min(start, minStart);
          var _pE = Math.max(end, maxEnd);

          msg = "Session '"+utils.time24(start)+"…"+utils.time24(end)+"' "
              + "tries to overlap "+overlapped.length+" session"+(overlapped.length > 1 ? "s" : "")
              + " (bounded by "+utils.time24(minStart)+" and "+utils.time24(maxEnd)+").<br /><br />— Unite them to one session '"+utils.time24(_pS)+"…"+utils.time24(_pE)+"'.";
          // msg += ' changed: '+JSON.stringify(attrs) + ' options: '+ JSON.stringify(options);
          // msg += '\n new start: '+start;
          // msg += '\n new end: '+end;
          // msg += '\n prev start: '+ (_m ? _m.attributes.start : "");

          function uniteSessions(){
            var newAttrs = {};
            newAttrs.start = _pS;
            if (attrs.end && _.every(overlapped, function(s){return s.attributes.end})) {
              newAttrs.end = _pE;
            } else if (_.some(overlapped, function(s){return !s.attributes.end})) {
              overlapped.push(_m);
              _m = _.find(overlapped, function(s){return !s.attributes.end});
              overlapped = _.without(overlapped, _m);
            }
            $.when(overlapped.forEach(function(model){model.destroy()})).then(_m.save(newAttrs));
          };

          if (start > minStart && end < maxEnd && (overlapped.length !== 1 || attrs.end)) {
            return this.vDialog(_m, msg, uniteSessions);
          }

          if (start < minStart && end > maxEnd) {
            // unite
            this.vDialog(_m, msg, uniteSessions);
            return true;
          } else if (start < minStart) {
            attrs.end = minStart-1000;
            msg += "<br />— End session before the next one starts (up to "+utils.time24(attrs.end)+")";
            // unite or set end as minStart-1
            return this.vDialog(_m, msg, uniteSessions, function(){_m.save(attrs)});
          } else if (end > maxEnd) {
            attrs.start = maxEnd+1000;
            msg += "<br />— Start session in a free area (from "+utils.time24(attrs.start)+")"
            // unite or set start as maxEnd+1
            return this.vDialog(_m, msg, uniteSessions, function(){_m.save(attrs)});
          }

            return this.vDialog(_m, "Another session in this timeframe exists.");
        }

        // TODO: check that given activity exists
      },

      duration: function(){
        if (this.get("end")) {
          return this.get("end") - this.get("start");
        } else {
          return Date.now() - this.get("start");
        }
      }

  });

});
