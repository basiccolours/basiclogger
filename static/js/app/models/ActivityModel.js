define(['jquery', 'backbone','utils', 'models/ApiBaseModel'],
function($, Backbone, utils, ApiBaseModel) {

  return ApiBaseModel.extend({
      dataType: function(){
        return 'activity';
      },

      initialize: function(){
        this.on("change:title", function(model, title) {
          // console.log("Changed title from " + this.previous("title") + " to " + title);
        });
        this.on("remove", function(model){
          console.log("Activity '" +model.get("title")+"' removed from collection.");
          Backbone.trigger("actRemoved", model.get("_id"));
        });
      },
      
      defaults: function(){ 
          return {
            _id: utils.guid(),
            created: Date.now(),
            type: this.dataType()
          }
      }
      
      


  });

});
