define(["jquery", "backbone",'utils', "userConfig"],
    function ($, Backbone, utils, config) {

        return Backbone.Model.extend({
            idAttribute: "_id",

            url: function(){return "/api/"+this.dataType()+"/"+this.id},

            storeName: function(){
                return config.userId()+'::'+this.dataType().toUpperCase()
            },

            parseBeforeLocalSave: function(resp, options) {
                // console.log("model parseBeforeLocalSave: ", this.url())
                if (resp.rev) {resp._rev = resp.rev; delete(resp.rev)};
                if (resp.id) {resp._id = resp.id; delete(resp.id)};
                return resp;
            },

            parse: function(resp, options) {
                // console.log("model parse: ", this.id, this.dataType())
                if (resp.rev) {resp._rev = resp.rev; delete(resp.rev)};
                if (resp.id) {resp._id = resp.id; delete(resp.id)};
                return resp;
            },

            isNew: function(){
                if (Backbone.dualsyncEnabled) {
                    return false;
                } else {
                    return !this.get("_rev");
                }
            }
        });

        return Model;
    }
);