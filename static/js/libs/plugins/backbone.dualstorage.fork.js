/*
FORK of Backbone dualStorage Adapter v1.2.0

TODO:
— merge conflicts, maybe store lastSync to check the preferred values or show Modal dialog, play with .set() attributes and changedAttributes
— check storage quota
— better parse errors
— sync with polling interval checkin updates
— show net status indicator: when success is green, when fetched collections message bubble, when there dirty — smth else

 */

(function() {
	 
		var prettifyOutput = function(obj){
			var output = '';
			if(_.isArray(obj) || typeof(obj) == 'object') {
					for(var i in obj) {
							output += i + ': ' + prettifyOutput(obj[i]) + '<br/>';
					}
			} else {
					output += obj;
			};

			return output;
		};

		var checkDIff = function(local, remote){
				var localDiff = {}, remoteDiff = {}, val, noChanges = true;

				var compareObj = function(obj){
						for (var attr in obj) {
							if (!obj.hasOwnProperty(attr) || _.isEqual(local[attr], remote[attr])) continue;
							localDiff[attr] = local[attr];
							remoteDiff[attr] = remote[attr];
							noChanges = false;
					}
				};

				compareObj(local);     
				compareObj(remote);

				if (noChanges) return [];
				return [localDiff, remoteDiff];
		};

		var notifier = new Backbone.Notifier({
				el: 'body',
				baseCls: 'notifier',
				theme: 'clean',
				types: ['warning', 'error', 'info', 'success'],
				type: null,
				dialog: false,
				modal: false,
				message: '',
				title: undefined,
				closeBtn: false,
				ms: 4000,
				'class': null,
				hideOnClick: true,
				loader: false,
				destroy: false,
				opacity: 1,
				// offsetY: -500,  
				fadeInMs: 500,
				fadeOutMs: 500,
				position: 'top',
				zIndex: 10000,
				screenOpacity: 0.5,
				width: undefined,
				// css: {'text-align': 'left'},
				modules: {}
		});

		function errorDialog(msg) {
	    return (notifier.notify({
	        message: msg,
	        'type': 'error',
	        modal: true,
	        ms: null,
	        destroy: false,
	        buttons: [{
	            'data-role': 'closeThis',
	            text: 'Ok'
	        }],
	        title: "Unable to resolve data conflict"
	    }).on('click:closeThis', function() {
	        this.destroy()
	    }))
		};

		function mergeDialog(model, method) {
				return (notifier.notify({
								title: "Resolve data conflict",
								message: (function() {
										var msg = "Unable to "+method+" "+model.get("type")+"."
															+' Remove now or change it later.';
															// +'<br /> Probably it was removed from another device';
										return msg;
								})(),
								buttons: [{
												'data-role': 'destroyModel',
												text: 'Remove '+model.get("type") /*, 'class': 'default', css: {width: 120}*/
										}, {
												'data-role': 'myOk',
												text: 'Cancel'
										},
										// 'data-role' - an identifier for binding event using notification.on('click:myOk', function(){...});
										// {'data-handler': 'destroy', text: 'No', 'class': 'link'}
										// 'data-handler' - calls a function of notification object,
										// i.g.: 'data-handler': 'destroy' => calls notification.destroy() upon clicking the button
								],
								modal: true,
								ms: null,
								modal: true,
								destroy: false,
								'type': 'warning'
						})
						.on('click:destroyModel', function() {
							model.destroy();
							return this.destroy();
						})
						.on('click:myOk', function() {
								return this.destroy();
								notifier.notify({
										'type': 'info',
										title: "Information",
										message: "This is an 'info' styled dialog notification.",
										buttons: [{
												'data-role': 'ok',
												text: 'Show More',
												'class': 'default'
										}, {
												'data-handler': 'destroy',
												text: 'Cancel',
												'class': 'link'
										}],
										closeBtn: true,
										modal: false,
										ms: null,
										destroy: false
								})
										.on('click:ok', function() {
												notifier.warning({
														destroy: true, // will hide all existing notification
														title: "Warning!",
														message: "This is a warning!",
														buttons: [{
																'data-handler': 'destroy',
																text: 'Errrr'
														}]
												})
														.on('destroy', function() {
																notifier.error({
																		title: "Error Dialog",
																		message: "That's our error dialog notification",
																		buttons: [{
																				'data-handler': 'destroy',
																				'class': 'default',
																				text: 'Ok'
																		}]
																})
																		.on('destroy', function() {
																				notifier.success({
																						title: "Success!",
																						message: "You have successfully completed this example. Well done!",
																						buttons: [{
																								'data-handler': 'destroy',
																								'class': 'default',
																								text: 'Finish'
																						}]
																				});
																		});
														});
										});
						}))
		};


		var backboneSync, dualsync, getStoreName, localsync, modelUpdatedWithResponse, onlineSync, parseRemoteResponse, result;

		var REPEAT_REQ_INT = 5000;
		var clear_repeatRequestTimeout = {};

		getStoreName = function(collection, model) {
				// console.debug("(collection, model)",collection, model, (collection && collection.model))
				model || (collection && collection.model && collection.model.prototype && (model = collection.model.prototype));
				return result(collection, 'storeName') || result(model, 'storeName') || result(collection, 'url') || result(model, 'urlRoot') || result(model, 'url');
		};

		Backbone.Collection.prototype.syncDirty = function() {
				var id, ids, store, _i, _len, _ref, _results;
				store = localStorage.getItem("" + (getStoreName(this)) + "_dirty");
				ids = (store && store.split(',')) || [];
				_results = [];
				for (_i = 0, _len = ids.length; _i < _len; _i++) {
						id = ids[_i];
						_results.push((_ref = this.get(id)) != null ? _ref.save() : void 0);
				}
				return _results;
		};

		Backbone.Collection.prototype.dirtyModels = function() {
				var id, ids, models, store;
				store = localStorage.getItem("" + (getStoreName(this)) + "_dirty");
				ids = (store && store.split(',')) || [];
				models = (function() {
						var _i, _len, _results;
						_results = [];
						for (_i = 0, _len = ids.length; _i < _len; _i++) {
								id = ids[_i];
								_results.push(this.get(id));
						}
						return _results;
				}).call(this);
				return _(models).compact();
		};

		Backbone.Collection.prototype.syncDestroyed = function() {
				var id, ids, model, store, _i, _len, _results;
				store = localStorage.getItem("" + (getStoreName(this)) + "_destroyed");
				ids = (store && store.split(',')) || [];
				_results = [];
				for (_i = 0, _len = ids.length; _i < _len; _i++) {
						id = ids[_i];
						model = new this.model;
						model.set({
								"_id": id
						});
						model.collection = this;
						_results.push(model.destroy());
				}
				return _results;
		};

		Backbone.Collection.prototype.destroyedModelIds = function() {
				var ids, store;
				store = localStorage.getItem("" + (getStoreName(this)) + "_destroyed");
				return ids = (store && store.split(',')) || [];
		};

		Backbone.Collection.prototype.syncDirtyAndDestroyed = function() {
				this.syncDirty();
				return this.syncDestroyed();
		};


		window.Store = (function() {
				Store.prototype.sep = '::';

				function Store(name) {
						this.name = name;
						this.records = this.recordsOn(this.name);
				}

				Store.prototype.save = function() {
						return localStorage.setItem(this.name, this.records.join(','));
				};

				Store.prototype.recordsOn = function(key) {
						var store;
						store = localStorage.getItem(key);
						return (store && store.split(',')) || [];
				};

				Store.prototype.dirty = function(model) {
						var dirtyRecords;
						dirtyRecords = this.recordsOn(this.name + '_dirty');
						if (!_.include(dirtyRecords, model.id.toString())) {
								dirtyRecords.push(model.id);
								localStorage.setItem(this.name + '_dirty', dirtyRecords.join(','));
						}
						return model;
				};

				Store.prototype.clean = function(model, from) {
						var dirtyRecords, store;
						store = "" + this.name + "_" + from;
						dirtyRecords = this.recordsOn(store);
						if (_.include(dirtyRecords, model.id.toString())) {
								localStorage.setItem(store, _.without(dirtyRecords, model.id.toString()).join(','));
						}
						return model;
				};

				Store.prototype.destroyed = function(model) {
						var destroyedRecords;
						destroyedRecords = this.recordsOn(this.name + '_destroyed');
						if (!_.include(destroyedRecords, model.id.toString())) {
								destroyedRecords.push(model.id);
								localStorage.setItem(this.name + '_destroyed', destroyedRecords.join(','));
						}
						return model;
				};

				Store.prototype.create = function(model) {
						if (!_.isObject(model)) {
								return model;
						}
						localStorage.setItem(this.name + this.sep + model.id, JSON.stringify(model));
						if (!_.include(this.records, model.id.toString())) {
								this.records.push(model.id.toString());
						}
						this.save();
						return model;
				};

				Store.prototype.update = function(model) {
						localStorage.setItem(this.name + this.sep + model.id, JSON.stringify(model));
						if (!_.include(this.records, model.id.toString())) {
								this.records.push(model.id.toString());
						}
						this.save();
						return model;
				};

				Store.prototype.clear = function() {
						var id, _i, _len, _ref;
						_ref = this.records;
						for (_i = 0, _len = _ref.length; _i < _len; _i++) {
								id = _ref[_i];
								localStorage.removeItem(this.name + this.sep + id);
						}
						this.records = [];
						return this.save();
				};

				Store.prototype.hasDirtyOrDestroyed = function() {
						return !_.isEmpty(localStorage.getItem(this.name + '_dirty')) || !_.isEmpty(localStorage.getItem(this.name + '_destroyed'));
				};

				Store.prototype.find = function(model) {
						var modelAsJson;
						modelAsJson = localStorage.getItem(this.name + this.sep + model.id);
						if (modelAsJson === null) {
								return null;
						}
						return JSON.parse(modelAsJson);
				};

				Store.prototype.findAll = function() {
						var id, _i, _len, _ref, _results;
						_ref = this.records;
						_results = [];
						for (_i = 0, _len = _ref.length; _i < _len; _i++) {
								id = _ref[_i];
								_results.push(JSON.parse(localStorage.getItem(this.name + this.sep + id)));
						}
						return _results;
				};

				Store.prototype.destroy = function(model) {
						localStorage.removeItem(this.name + this.sep + model.id);
						this.records = _.reject(this.records, function(record_id) {
								return record_id === model.id.toString();
						});
						this.save();
						return model;
				};

				return Store;

		})();


		localsync = function(method, model, options) {
				var isValidModel, preExisting, response, store;
				isValidModel = (method === 'clear') || (method === 'hasDirtyOrDestroyed');
				isValidModel || (isValidModel = model instanceof Backbone.Model);
				isValidModel || (isValidModel = model instanceof Backbone.Collection);
				if (!isValidModel) {
						throw new Error('model parameter is required to be a backbone model or collection.');
				}
				if (options.neverRemote) {
						options.dirty = false;
				}
				store = new Store(options.storeName);
				response = (function() {
						switch (method) {
								case 'read':
										if (model instanceof Backbone.Model) {
												return store.find(model);
										} else {
												return store.findAll();
										}
										break;
								case 'hasDirtyOrDestroyed':
										return store.hasDirtyOrDestroyed();
								case 'clear':
										return store.clear();
								case 'create':
										if (options.add && !options.merge && (preExisting = store.find(model))) {
												return preExisting;
										} else {
												model = store.create(model);
												if (options.dirty) {
														store.dirty(model);
												} else {                                // Carefull: these lines
														return store.clean(model, 'dirty'); // were added as a fix
												}                                       // while not cleaning dirty entry
												return model;
										}
										break;
								case 'update':
										store.update(model);
										if (options.dirty) {
												return store.dirty(model);
										} else {
												return store.clean(model, 'dirty');
										}
										break;
								case 'delete':
										store.destroy(model);
										if (options.dirty) {
												return store.destroyed(model);
										} else {
												store.clean(model, 'dirty');
												return store.clean(model, 'destroyed');
										}

						}
				})();

				if (response != null ? response.attributes : void 0) {
						response = response.attributes;
				}
				if (!options.ignoreCallbacks) {
						if (response) {
								options.success(response);
						} else {
								options.error('Record not found');
						}
				}
				return response;
		};

		result = function(object, property) {
				var value;
				if (!object) {
						return null;
				}
				value = object[property];
				if (_.isFunction(value)) {
						return value.call(object);
				} else {
						return value;
				}
		};

		parseRemoteResponse = function(object, response) {
				if (!(object && object.parseBeforeLocalSave)) {
						return response;
				}
				if (_.isFunction(object.parseBeforeLocalSave)) {
						return object.parseBeforeLocalSave(response);
				}
		};

		modelUpdatedWithResponse = function(model, response) {
				var modelClone;
				modelClone = new model.collection.model;
				modelClone.idAttribute = model.idAttribute;
				modelClone.set(model.attributes);
				modelClone.set(modelClone.parse(response));
				return modelClone;
		};

		backboneSync = Backbone.sync;

		onlineSync = function(method, model, options) {
				return backboneSync(method, model, options);
		};


		var errorLog = {}, _m;
		// var errorLogCounter = {};

		function clearErrorLog(id){
  			delete errorLog[id];
  			// return delete errorLogCounter[id];
		}

		function setErrorLogRecord(model, method){
				errorLog[model.id] = model.toJSON();
				errorLog[model.id].method = method;
				// errorLogCounter[model.id] ? errorLogCounter[model.id]++ : errorLogCounter[model.id] = 1;
		}

		function isNewError(model, method){
			_m = model.toJSON(); 
			_m.method = method;
			// console.debug("errorLogCounter[model.id] ", errorLogCounter[model.id])
			// return !errorLogCounter[model.id] || !errorLog[model.id] || errorLogCounter[model.id] === 1 || checkDIff(model.toJSON(), errorLog[model.id]).length !== 0;
			return !errorLog[model.id] || checkDIff(_m, errorLog[model.id]).length !== 0;
		}

		var parseError = function(resp, model, options, method) {
				var respStatus = (resp.status || options.xhr.status).toString();
				if (!model || !(model instanceof Backbone.Model) || !respStatus || respStatus === "0" || !method) return;

				// Keep trace of the error Log, if already fired with the same values then return
				// On some auto fixes then unregister those entities.
				if (!isNewError(model, method)) return console.log("not a new error ", model.id);
				setErrorLogRecord(model, method);

				var errMsg = respStatus+" "+ (resp.responseText || options.xhr.statusText || '') + ' \n';

				var actionMap = {
						'delete': 'destroy',
						create: 'create',
						update: 'save',
						read: 'fetch'
				};

				if (respStatus === '409') {
				    $.get(model.url())
				        .success(function(data) {
				        		if (!data._rev) return errorDialog("No revision remotely. An absurd error.");
				        		if (!model.get("_rev") || model.get("_rev") !== data._rev) {
		            			model.set('_rev', data._rev);
				        		} else {
					            if (method !== 'delete') {
						            var diff = checkDIff(model.toJSON(), data);
						            if (diff.length !== 0) {
						                for (attr in diff[1]) {
						                    model.set(attr, diff[1][attr]);
						                }
					            			clearErrorLog(model.id);
						            } else {
						            	errMsg += '<br/>Model: '+ model.id +', type: '+model.get("type")+'<br/>';
						            	errMsg += '<br/><b>Please, resolve the conflict manually.</b>'
						            	return errorDialog(errMsg);
						            };
					            };
				            };

		                notifier.warning({message: 'Resolving data conflict...', opacity: 0.7, ms: 2000, loader: true});
		                return model[actionMap[method]]();
				        })
				        .error(function(xhr, status, error) {
				        		if (xhr.status.toString() !== '404') return;
				        		if (model.get("_rev")){clearErrorLog(model.id); return model.destroy({remote: false, neverRemote: true})};
				        		if (model.get("type") === 'session'){return mergeDialog(model, method)};
				            return errorDialog("Something went wrong. Error: "+error);
				        });

				} else if (respStatus === '404') {
				    if (method === 'delete' || method === 'update') {
				    		var dur = 0;
                if (model.get('_rev')) {dur = 3000; notifier.error({message: model.get("type")+' was destroyed from another device.', ms: dur})};
				        return setTimeout(function(){clearErrorLog(model.id); return model.destroy({remote: false, neverRemote: true})}, dur+300)
				    };
				} else {
				    return console.debug(errMsg);
				}


		};

		var delayedStack = {};
		var delayAction = function(action, id){
			clearTimeout(delayedStack[id]);
			return delayedStack[id] = setTimeout(action, 600);
		}

		dualsync = function(method, model, options) {
				var error, local, success;
				options.storeName = getStoreName(model.collection, model);
				if (result(model, 'remote') || result(model.collection, 'remote')) {
						return onlineSync(method, model, options);
				}
				
				local = result(model, 'local') || result(model.collection, 'local');
				options.dirty = options.remote === false && !local;
				if (options.remote === false || local) {
						return localsync(method, model, options);
				}
				options.ignoreCallbacks = true;
				success = options.success;
				error = options.error;

				options.add = true; //Not to clear storage for empty result

				switch (method) {
						case 'read':
								if (localsync('hasDirtyOrDestroyed', model, options)) {
										console.debug("Storage has Dirty Or Destroyed '" + getStoreName(this));
										success(localsync(method, model, options));
										return model.syncDirtyAndDestroyed();
								} else {
										options.success = function(resp, status, xhr) {
												var collection, idAttribute, modelAttributes, responseModel, _i, _len;
												resp = parseRemoteResponse(model, resp);
												if (model instanceof Backbone.Collection) {
														collection = model;
														idAttribute = collection.model.prototype.idAttribute;
														if (!options.add) {
																localsync('clear', collection, options);
														}
														for (_i = 0, _len = resp.length; _i < _len; _i++) {
																modelAttributes = resp[_i];
																model = collection.get(modelAttributes[idAttribute]);
																if (model) {
																		responseModel = modelUpdatedWithResponse(model, modelAttributes);
																} else {
																		responseModel = new collection.model(modelAttributes);
																}
																localsync('update', responseModel, options);
														}
												} else {
														responseModel = modelUpdatedWithResponse(model, resp);
														localsync('update', responseModel, options);
												}
												return success(resp, status, xhr);
										};
										options.error = function(resp) {
												success(localsync(method, model, options));
												return parseError(resp, model, options, method);
										};
										return onlineSync(method, model, options);
								}
								break;
						case 'create':
								options.success = function(resp, status, xhr) {
										var updatedModel = modelUpdatedWithResponse(model, resp);
										localsync(method, updatedModel, options);
										return success(resp, status, xhr);
								};
								options.error = function(resp) {
										options.dirty = true;
										success(localsync(method, model, options));
										return parseError(resp, model, options, method);
								};
								return onlineSync(method, model, options);
						case 'update':
								options.success = function(resp, status, xhr) {
										var updatedModel;
										updatedModel = modelUpdatedWithResponse(model, resp);
										localsync(method, updatedModel, options);
										return success(resp, status, xhr);
								};
								options.error = function(resp) {
										options.dirty = true;
										success(localsync(method, model, options));
										return parseError(resp, model, options, method);
								};
								return delayAction(function(){return onlineSync(method, model, options)}, model.id);
								break;
						case 'delete':
								options.success = function(resp, status, xhr) {
										localsync(method, model, options);
										return success(resp, status, xhr);
								};
								options.error = function(resp) {
										options.dirty = true;
										success(localsync(method, model, options));
										return parseError(resp, model, options, method);
								};
								clearTimeout(delayedStack[model.id]);
								return onlineSync(method, model, options);
				}
		};

		Backbone.sync = dualsync;
		Backbone.dualsyncEnabled = true;

}).call(this);


//   ************************************************

//   ====== BASIC ERROR SCENARIOS ======

//   409:
//   Update activity with wrong revision → Scenario A
//   Update session with wrong revision → Scenario A
//   Delete activity with wrong revision → Scenario C
//   Delete session with wrong revision → Scenario C
//   Create conflicting sessions (also 400) → Scenario A

//   404:
//   Get activity with invalid or nonexistent id → DO NOTHING
//   Delete activity/session with nonexistent id → Scenario B
//   Update session with a rev but nonexistent id → Scenario B
//   Create session for missing activity → Scenario E

//   400:
//   Create session with no/null/wrong format start date
//   Create session with wrong/null end date format
//   Create conflicting sessions (also 409)

//   ALL OTHER CODES:
//   → Scenario D

//   ----------------------------------------------

//   Scenario A
//     → Scenario С    
//     TODO: Show local/remote diff and make the user decide.

//   Scenario B
//     Silently delete the model locally.

//   Scenario С
//     Get the right revision and repeat request.

//   Scenario D
//     Most common error is no connection to server. We just persist locally.
//     But if we got response status rather than 0, we also show an alert.
//     ( TODO: Hide this hypothetical alert on production )

//   Scenario E
// 		 DO NOTHING! Why: on the next SYNC all gets on the right place!
//     If activity doesn't exist locally → Scenario B.
//     If activity exists locally and has revision 
//     (which means it was saved remotely and then removed from another device) 
//     — then destroy both.
//     If activity exists locally and has no revision, first save it remotely and on success repeat saving the session, on error → Scenario B.

//   ************************************************

//   TODO: Keep track of errors (with conditions: scope, time, etc) & information about their successful resolving.

//   ************************************************
