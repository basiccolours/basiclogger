#!/usr/bin/env python
import os

from raven.contrib.tornado import AsyncSentryClient

from tornado import web
from tornado.ioloop import IOLoop
from tornado.options import (options, define, parse_config_file,
                             parse_command_line)

from basiclogger import Application, uimethods, pid
from basiclogger.handlers import pages, admin, testing, api

# CouchDB, Mandrill, Sentry options are in basiclogger/opts.py
define("debug", default=None, type=bool,
       help="Turn on debug mode")
define("pdb", default=None, type=bool,
       help="Run pdb.post_mortem() on exceptions")
define("port", default=8081, type=int,
       help="TCP port to listen to")
define("address", default="127.0.0.1",
       help="IP address to listen to")
define("pid", default="app.pid",
       help="Pid file path")
define("cookie_secret", default="5LwgO6TrRNGFEt/NRG0tt+wQaIVT50CAuQHeMzbcwNQ=",
       help="Cookie secret to sign secure cookies")


if __name__ == "__main__":
    APP_ROOT = os.path.dirname(__file__)
    parse_config_file(os.path.join(APP_ROOT, "settings.py"))
    parse_command_line()

    settings = {
        "static_path": os.path.join(APP_ROOT, "static"),
        "login_url": "/account/login",
        "xsrf_cookies": True,
        "template_path": "templates"
    }
    for option in ('debug', 'cookie_secret', 'twitter_consumer_key',
                   'twitter_consumer_secret'):
        settings[option] = getattr(options, option)

    # TODO: use separate "site" and "API" handlers, choose by command line
    # switch. Will make possible to run them separately one day.
    url_handlers = [
        web.url(r"/", pages.Index, name='index'),
        web.url(r"/app", pages.UI, name='ui'),

        web.url(r"/account/signup", pages.AccountSignup,
                name='signup'),
        web.url(r"/account/reset", pages.PasswordReset,
                name='password_reset'),
        web.url(r"/account/changepass", pages.PasswordChange,
                name='password_change'),
        web.url(r"/account/signup/google", pages.GoogleAccount,
                name='signup_g'),
        web.url(r"/account/signup/twitter", pages.TwitterAccount,
                name='signup_t'),
        web.url(r"/account/login", pages.Login,
                name='login'),
        web.url(r"/account/logout", pages.Logout,
                name='logout'),
        web.url(r"/account/delete", pages.AccountRemoval,
                name='delete'),
        web.url(r"/account/activate", pages.AccountActivation,
                name='activate'),

        # optional parts in URL require more flexible hendlers with optional
        # arguments handling anyway
        #web.url(r"/api/(session)(?:/([a-f0-9]{32}))?", api.SessionAPI),
        web.url(r"/api/(activity)", api.ActivityAPI),
        web.url(r"/api/(activity)/([a-f0-9]{32})", api.ActivityAPI),
        web.url(r"/api/(session)", api.SessionAPI),
        web.url(r"/api/(session)/([a-f0-9]{32})", api.SessionAPI),
        web.url(r"/api/account", api.AccountAPI),
        web.url(r"/api/settings", api.SettingsAPI),
        web.url(r"/api/priv/account", api.PrivilegedAccountAPI),
        web.url(r"/api/bulk/(session)", api.SessionBulkAPI),
        web.url(r"/api/bulk/(activity)", api.ActivityBulkAPI),
        web.url(r"/api/bulk/(tag)", api.TagBulkAPI),
        web.url(r"/api/changes", api.ChangesAPI),
        web.url(r"/api/changes/(activity|session)", api.ChangesAPI),

        web.url(r"/admin", admin.AdminIndex, name='admin'),
        web.url(r"/t/api", testing.APITest, name='api_test'),
        web.url(r"/t/cors", testing.CORSTest, name='cors_test'),

        web.url(r"/.*", pages.NotFound, name='not_found')
    ]
    app = Application(url_handlers, ui_methods=uimethods, **settings)
    app.sentry_client = AsyncSentryClient(options.sentry_dsn)


    def start():
        app.listen(options.port, options.address, xheaders=True)
        ioloop = IOLoop.instance()
        ioloop.start()

    # We do not want to commit a suicide on autoreload, and nobody should run
    # in debug mode under supervisor, right?
    if not options.debug:
        pid.check(options.pid)
        pid.write(options.pid)
        try:
            start()
        finally:
            pid.remove(options.pid)
    else:
        start()
