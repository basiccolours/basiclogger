function(newDoc, oldDoc, userCtx, secObj){
    /* shortcut functions */
    function require(field, message){
        if (!newDoc[field]){
            message = message || "Document must have a non-empty \"" + field + "\" field set.";
            throw({forbidden: message});
        }
    };

    function is_timestamp(field, message){
        //if (!newDoc[field]){
        // if the field is not present at all
        if (!(field in newDoc)){
            return
        }
        // if the field value does not evaluate to false
        if (newDoc[field]){
            // older spidermonkey versions can't do this
            // (/^[0-9]{1,14}$/.test(newDoc[field]))
            if (!! newDoc[field].toString().match(/^[0-9]{1,14}$/)){
                return
            }
        }
        // TODO: how to check if the field is really an integer?!
        message = message || "Document field \"" + field + "\" should follow date format.";
        throw({forbidden: message});
    };

    function less_or_equal(field1, field2, message){
        if (!newDoc[field1] || !newDoc[field2]){
            return
        }
        if (newDoc[field1] <= newDoc[field2]){
            return
        }
        message = message || "Document field \"" + field1 + "\" is greater than \"" + field2 + "\".";
        throw({forbidden: message});
    }

    function is_id(field, message){
        //if (!newDoc[field]){
        if (!(field in newDoc)){
            return
        }
        // if the field value does not evaluate to false
        if (newDoc[field]){
            // (/^[a-f0-9]{32}$/.test(newDoc[field]))
            if (!! newDoc[field].toString().match(/^[a-f0-9]{32}$/)){
                return
            }
        }
        message = message || "Document field \"" + field + "\" should follow id format.";
        throw({forbidden: message});
    };

    function is_array_of_tags(field, message){
        message = message || "Document field \"" + field + "\" should be a list of non-empty words (without spaces, commas and special characters) up to 32 symbols.";
        if (field in newDoc){
            if (Object.prototype.toString.call(newDoc[field]) == '[object Array]'){
                for (var i = 0; i < newDoc[field].length; i++) {
                    if ((typeof(newDoc[field][i]) != 'string') ||
                        (newDoc[field][i].length == 0 ) ||
                        (newDoc[field][i].length > 32) ||
                        (!! newDoc[field][i].toString().match(/[\s,'"\x07\x1B\f\n\r\t\v]+/))){
                        throw({forbidden: message});
                    }
                }
            } else {
                throw({forbidden: message});
            }
        }
    };

    function unchanged(field){
        if (oldDoc && !newDoc._deleted && toJSON(oldDoc[field]) != toJSON(newDoc[field])){
            throw({forbidden: "Field \"" + field + "\" can't be modified."});
        }
    }

    /* debug
    log('[ oldDoc ]')
    log(oldDoc);
    log('[ newDoc ]')
    log(newDoc);
    log('[ secObj ]');
    log(secObj);
    */

    /* common checks */
    if (oldDoc && oldDoc._deleted){
        throw {forbidden: "You can not modify deleted documents."};
    }
    unchanged('type');

    if (!newDoc._deleted){
        /* document-specific checks */
        switch (newDoc.type){
            case 'activity':
                require('title');
                is_array_of_tags('tags');
                break
            case 'session':
                require('activity');
                is_id('activity');
                require('start');
                is_timestamp('start');
                /* we might want to adjust existing sessions
                unchanged('start');*/
                is_timestamp('end');
                less_or_equal('start', 'end')
                break
            case 'settings':
                break
            default:
                throw({forbidden: "Unknown document type"})
        }
    }

}
