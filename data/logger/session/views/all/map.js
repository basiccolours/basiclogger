function(doc) {
	if (doc.type == 'session'){
		emit(doc.end, [doc._id, doc._rev]);
	}
}
