function(doc) {
    if (doc.type == 'session' && !doc.end){
        emit(doc.activity, {start: doc.start});
    }
}
