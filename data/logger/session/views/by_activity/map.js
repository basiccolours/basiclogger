function(doc) {
	if (doc.type == 'session'){
		emit(doc.activity, [doc._id, doc._rev]);
	}
}
