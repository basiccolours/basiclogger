function(doc){
    if (doc.type == 'session' && doc.start && doc.end){
        emit(
            {
                type: "Point",
                // omit coordinates, and provide the bounding box itself
                //coordinates: [doc.start, doc.end]
                bbox: [0, doc.start, 0, doc.end]
            },
            doc.activity);
    }
}
