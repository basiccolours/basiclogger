function(doc) {
    if (doc.type == 'activity' && doc.tags){
        emit(doc.tags, [doc._id, doc._rev]);
    }
}
