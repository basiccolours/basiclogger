function(doc) {
    if (doc.type == 'activity' && doc.tags){
        doc.tags.forEach(function(tag) {
            emit(tag, 1);
        });
    }
}
