#!/usr/bin/env python
import os
from tornado import gen
from tornado.options import (define, options, parse_config_file,
                             parse_command_line)
from tornado.ioloop import IOLoop
from basiclogger import Application, TwitterWorker, pid

# TODO: move soem common options and PID/startup code to reuse in app.py

define("debug", default=None, type=bool,
       help="Turn on debug mode")
define("pid", default="periodic.pid",
       help="Pid file path")
define("twitter_sync_interval", default=120, type=int,
       help="Twitter data sync interval in seconds")

if __name__ == "__main__":
    APP_ROOT = os.path.dirname(__file__)
    parse_config_file(os.path.join(APP_ROOT, "settings.py"))
    parse_command_line()
    settings = {}
    for option in ('twitter_consumer_key', 'twitter_consumer_secret'):
        settings[option] = getattr(options, option)

    app = Application([], **settings)

    def start():
        ioloop = IOLoop.instance()
        # check for tweets not more frequently than every 5 seconds! (180/15min)
        twitter = TwitterWorker(app, interval=options.twitter_sync_interval)
        ioloop.run_sync(twitter.fetch)

        ioloop.start()

    if not options.debug:
        pid.check(options.pid)
        pid.write(options.pid)
        try:
            start()
        finally:
            pid.remove(options.pid)
    else:
        start()
