#!/usr/bin/env python
import os
import sys

from tornado.options import (options, define, parse_config_file,
                             parse_command_line)
from tornado.ioloop import IOLoop
from couchdbkit.client import Server
from couchdbkit.loaders import FileSystemDocsLoader

from basiclogger import Application

define("sync", default=None, type=bool,
       help="Sync 'logger' DB documents")
define("sync_dirs", default=['data', ], metavar="data,someotherdir",
       multiple=True, help="Directories to sync")
define("rereplicate", default=None, type=bool,
       help="DEPRECATED: is part of a 'sync' now")


# we use couchdbkit to ease doc synchronization
def sync_design_docs(sync_dirs):
    if options.couchdb_admin_user and options.couchdb_admin_password:
        basic_auth_string = '{}:{}@'.format(
            options.couchdb_user,
            options.couchdb_password)
    else:
        basic_auth_string = ''
    couchdb_url = ('{}://{}{}:{}'.format(
                   options.couchdb_protocol,
                   basic_auth_string,
                   options.couchdb_host,
                   options.couchdb_port))
    server = Server(couchdb_url)


    for sync_dir in sync_dirs:
        sync_dir_path = os.path.join(os.path.dirname(__file__), sync_dir)
        if os.path.isdir(sync_dir_path):
            # TODO: decide how to handle flexible logger db name
            for dbname in os.listdir(sync_dir_path):
                dbpath = os.path.join(sync_dir_path, dbname)
                if os.path.isdir(dbpath):
                    db = server.get_or_create_db(dbname)
                    # TODO: allow to sync non-design docs too (as second arg)
                    loader = FileSystemDocsLoader(dbpath)
                    loader.sync(db, verbose=True)
        else:
            # TODO: log this
            pass


def execute_commands():
    if options.rereplicate:
        sys.stdout.write("'--rereplicate' option is deprecated\n")
    if options.sync:
        sync_design_docs(options.sync_dirs)
        app = Application([])
        app.rereplicate_usrdb_design_docs(finish)
    else:
        sys.stdout.write("'--sync' option is required\n")
        finish()


def finish(data=None):
    loop.stop()

if __name__ == "__main__":
    APP_ROOT = os.path.dirname(__file__)
    parse_config_file(os.path.join(APP_ROOT, "settings.py"))
    parse_command_line()

    loop = IOLoop.instance()
    loop.add_callback(execute_commands)
    loop.start()
