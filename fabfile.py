from fabric.state import env
from fabric.context_managers import warn_only
from fabric.tasks import execute
from fabric.decorators import task
from fabric.operations import run
from fabric.contrib.files import append, exists, cd

from webdev_fab import set_project_defaults
from webdev_fab.tasks import *
from webdev_fab.tasks.python import (
    setup_virtualenv, install_reqs, recompile_py)
from webdev_fab.utils import generate_tornado_cookie_secret

env.use_ssh_config = True
env.user = 'basiclogger'
if not env.hosts:
    env.hosts = ['logger.basiccolours.com.ua']
set_project_defaults(env.user)
# Defaults to local system's user running fabric
# env.poweruser = 'mike'
# Defaults to git remote origin fetch url
# env.repo = 'git@bitbucket.org:basiccolours/basiclogger.git'


@task
def set_cookie_secret(settings_file='settings_local.py'):
    """Set generated cookie_secret in settings_local.py file."""
    with cd('project'), warn_only():
        if not exists(settings_file):
            run('touch {}'.format(settings_file))
            run('chmod 600 {}'.format(settings_file))
        else:
            run('sed s"|^\s*cookie_secret\s*=\s*|'
                '#cookie_secret = |" {}'.format(settings_file))
        append(settings_file, "\ncookie_secret = '{0}'".format(
               generate_tornado_cookie_secret()))


@task
def restart_project():
    """Restart Tornado instances.

    Kill the Tornado processes, assuming they was stared by supervisor,
    which should respawn it. Currently implemented in a dirty way just sending
    SIGTERM to every Tornado process.

    """
    signal = 'SIGTERM'
    with cd('project'), warn_only():
        run('for pidfile in tmp/*.pid; '
            'do kill -s {0} `cat $pidfile`; '
            'done'.format(signal))
#    # Assuming we run Tornado under supervisor, which should respawn it.
#    # TODO: implement gracefull restart:
#    # http://codemehanika.org/blog/2011-10-28-graceful-stop-tornado.html
#    with settings(hide('warnings'), warn_only=True):
#        run('kill -{} `cat project/path/to/pids`'.format(signal))


@task
def manage(command):
    """Run arbitrary Basiclogger management command."""
    with cd('project'):
        run('python manage.py {0}'.format(command))


@task
def provision():
    """Create system user, upload public key, setup virtualenv."""
    # TODO
    # create configs: nginx, couchdb
    # execute(create_db)
    execute(create_user)
    execute(upload_local_public_key, use_poweruser=True)
    execute(generate_keypair)
    execute(show_public_key)
    execute(setup_virtualenv)


@task
def deploy(syncdb=True, requirements=True, code=True, essential=False,
           upgrade=False):
    """Deploy a project, doing all the required magick.

    If 'push_local' is True, use git to push local env.branch to remote host,
    otherwise fetch from env.repo (remember to add deployment key to the
    repository, see generate_keypair/show_public_key tasks). On the host:
    checkout env.branch in non-bare repository, update requirements, run
    essential management commands, restart project.

    """
    if essential:
        syncdb = requirements = code = False

    if not exists('project'):
        run('git clone -q {} project'.format(env.repo))
        execute(set_cookie_secret)

    with cd('project'):
        run('git checkout -q {}'.format(env.branch))
        run('git pull -q --ff-only origin {}'.format(env.branch))
        run('/bin/mkdir -p tmp')
        run('/bin/chmod 750 tmp')

    if requirements:
        execute(install_reqs, upgrade=upgrade)

    if syncdb:
        execute(manage, '--sync')
        # execute(manage, '--rereplicate')

    if code:
        execute(recompile_py)
        execute(restart_project)
