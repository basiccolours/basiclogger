Feature: Site navigation and account actions
  As a user I want to be able to signup, sign in, delete my account and logout

  Background: Set server name and headers
    Given I am using server "$SERVER"
    And I set "Accept" header to "text/html"
    And I set "Content-Type" header to "application/x-www-form-urlencoded"
  @wip
  Scenario: User logs in
    When I make a GET request to "account/login"
    Then the response status should be 200
    # TODO: test _xsrf cookie
    When I make a POST request to "account/login"
    """
    email=featuretester@example.com&password=featuretester
    """
    #When I submit a form to "account/login":
    #    """
    #    {"email": "featuretester@example.com", "password": "featuretester"}
    #    """
    Then the response status should be 200

# What is not tested
#
# * we have no shared state to actually log out anyone
# * signup process is implicitly tested in environment.before_all()
