import json
import os
import logging
import math
import requests
import time
from purl import URL
from behave_http.environment import before_scenario

from tornado.options import options, parse_config_file
from basiclogger.utils import dbname_for_username

APP_ROOT = os.path.normpath(
    os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
parse_config_file(os.path.join(APP_ROOT, "settings.py"))

# these values are used as default for essential env variables
default_env = {
    'SERVER': 'http://localhost:8081',
    'API': 'api',
    'COUCHDB': '{}://{}:{}'.format(
        options.couchdb_protocol, options.couchdb_host, options.couchdb_port),
    'COUCHDB_ADMIN_USER': options.couchdb_admin_user,
    'COUCHDB_ADMIN_PASSWORD': options.couchdb_admin_password
}

credentials = {
    'password': 'featuretester',
    'email': 'featuretester@example.com'
}

def _bootstrap_account(app_url, api_url, api_auth, couchdb_url, couchdb_auth):
    """Re-create user account"""
    r = requests.get(api_url.add_path_segment('account').as_string(),
                     auth=api_auth)
    if r.status_code == 200:
        r = requests.delete(
            api_url.add_path_segment('account').append_query_param(
                'ignore_errors', 'true').as_string(), auth=api_auth)
        assert r.status_code == 200
    else:
        # We have a user with locked account password or account really does
        # not exist. Try to silently delete it with db admin privileges.
        assert r.status_code in (404, 401)
        r = requests.delete(
                api_url.add_path_segment('priv/account').query_param(
                    'ignore_errors', 'true').query_param(
                        'account', credentials['email']).as_string(),
                auth=couchdb_auth)
        assert r.status_code == 200

    # Assume the database does not exist if we've made thus far.
    # Use web form for sign up, as we have no real API for that yet.
    # App should redirect to a valid activation url in debug mode.
    # TODO: add API to signup?
    r = requests.post(app_url.add_path_segment('account/signup').as_string(),
                      data=credentials)
    assert r.status_code == 200

    while True:
        r = requests.get(
            couchdb_url.add_path_segment('_active_tasks').as_string(),
            auth=couchdb_auth)
        assert r.status_code == 200
        active_tasks = r.json()
        estimate = 0
        for task in active_tasks:
            if ('target' in task and task['target']
                == dbname_for_username(credentials['email'])):
                logging.debug("active task " + task["doc_id"])
                if task['progress'] < 100:
                    logging.debug(
                        "active_tasks progress = {}".format(task['progress']))
                    estimate += (100 - task['progress'])
                    logging.debug(
                        "active_tasks estimate = {}".format(estimate))
        if not estimate:
            break
        #estimate_seconds = math.log(float(estimate)) + 1
        #logging.debug(
        #    "active_tasks estimate = {} seconds".format(estimate_seconds))
        #time.sleep(estimate_seconds)
        logging.debug("sleeping for a while")
        time.sleep(0.8)


def _add_account_data(api_url, api_auth):
    # Create a few activities to work with
    for i in range(1, 5):
        r = requests.put(
            api_url.add_path_segment(
                'activity/ddddddddddddddddddddddddddddddd{}'.format(i)).as_string(),
            auth=api_auth,
            data=json.dumps({
                'title': 'activity {}'.format(i),
                'tags': ['tag{}'.format(i), 'foo']})
        )
        assert r.status_code == 201
        for k in range(1, 3):
            r = requests.put(
                api_url.add_path_segment(
                    'session/'
                    'eeeeeeeeeeeeeeeeeeeeeeeeeeeeee{0}{1}'.format(i, k)).as_string(),
                auth=api_auth,
                data=json.dumps(
                    {'activity': 'ddddddddddddddddddddddddddddddd{}'.format(i),
                     'start': k * 10,
                     'end': k * 10 + 5}
                )
            )
            assert r.status_code == 201


def before_all(context):
    #if not context.config.log_capture:
    #    logging.basicConfig(level=logging.INFO)

    for k,v in default_env.iteritems():
        os.environ.setdefault(k, v)

    app_url = URL(os.environ['SERVER'])
    api_url = app_url.add_path_segment(os.environ['API'])
    api_auth = (credentials['email'], credentials['password'])
    couchdb_url = URL(os.environ['COUCHDB'])
    couchdb_auth = (os.environ['COUCHDB_ADMIN_USER'],
                    os.environ['COUCHDB_ADMIN_PASSWORD'])

    _bootstrap_account(app_url, api_url, api_auth, couchdb_url, couchdb_auth)
    _add_account_data(api_url, api_auth)


def after_tag(context, tag):
    if tag == 'modifies':
        before_all(context)
