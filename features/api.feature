Feature: API
  As an API client
  I want to be able to manage activities and sessions


  Background: Set server name, headers and reset test user's database
    Given I am using server "$SERVER"
    And I set base URL to "$API"
    And I set "Accept" header to "application/json"
    And I set "Content-Type" header to "application/json"
    And I set BasicAuth username to "featuretester@example.com" and password to "featuretester"


  Scenario: Ensure account exists
    When I make a GET request to "account"
    Then the response status should be 200

  @modifies
  Scenario: Delete account, ensure it does not exist
    When I make a DELETE request to "account"
    Then the response status should be 200
    When I make a GET request to "account"
    Then the response status should be 401

  @modifies
  Scenario: Create activity with server-generated id and ensure it is accessible
    When I make a POST request to "activity"
      """
      {"title": "server-generated id"}
      """
    Then the response status should be 201
    When I store the JSON at path "id" in "id"
    And I make a GET request to "activity/{{ id }}"
    Then the response status should be 200
    And the JSON at path "title" should be "server-generated id"

  @modifies
  Scenario: Create activity with user-supplied id and ensure it is accessible
    When I make a PUT request to "activity/fffffffffffffffffffffffffffffff1"
      """
      {"title": "user-supplied id"}
      """
    Then the response status should be 201
    When I store the JSON at path "id" in "id"
    And I make a GET request to "activity/{{ id }}"
    Then the response status should be 200
    And the JSON at path "title" should be "user-supplied id"

  @modifies
  Scenario: Get and update activity
    When I make a GET request to "activity/ddddddddddddddddddddddddddddddd1"
    Then the response status should be 200
    When I store the JSON at path "_id" in "id"
    When I store the JSON at path "_rev" in "rev"
    When I make a PUT request to "activity/{{ id }}?rev={{ rev }}"
      """
      {"title": "baseball"}
      """
    Then the response status should be 201
    When I make a GET request to "activity/{{ id }}"
    Then the response status should be 200
    And the JSON at path "title" should be "baseball"

  @modifies
  Scenario: Start, end, update, delete session
    When I make a POST request to "session"
      """
      {"activity": "ddddddddddddddddddddddddddddddd1", "start": 1}
      """
    Then the response status should be 201
    When I store the JSON at path "id" in "id"
    When I store the JSON at path "rev" in "rev"
    When I make a PUT request to "session/{{ id }}?rev={{ rev }}"
      """
      {
        "activity": "ddddddddddddddddddddddddddddddd1",
        "start": 1, "end": 2
      }
      """
    Then the response status should be 201
    When I store the JSON at path "rev" in "rev"
    When I make a DELETE request to "session/{{ id }}?rev={{ rev }}"
    Then the response status should be 200

  @modifies
  Scenario: Delete session without providing a revision
    When I make a DELETE request to "session/eeeeeeeeeeeeeeeeeeeeeeeeeeeeee11"
    Then the response status should be 200

  @modifies
  Scenario: Delete activity
    When I make a GET request to "activity/ddddddddddddddddddddddddddddddd2"
    When I store the JSON at path "_id" in "id"
    When I store the JSON at path "_rev" in "rev"
    When I make a DELETE request to "activity/{{ id }}?rev={{ rev }}"
    Then the response status should be 200

  @modifies
  Scenario: Delete activity without providing a revision
    When I make a DELETE request to "activity/ddddddddddddddddddddddddddddddd3"
    Then the response status should be 200


  ### Session timerange validation checks ###

  @modifies
  Scenario: Create conflicting sessions
    When I make a POST request to "session"
      """
      {
        "activity": "ddddddddddddddddddddddddddddddd1",
        "start": 2,
        "end": 1
      }
      """
    Then the response status should be 400
    # Start session
    When I make a POST request to "session"
      """
      {"activity": "ddddddddddddddddddddddddddddddd1", "start": 2}
      """
    Then the response status should be 201
    When I store the JSON at path "id" in "id"
    When I store the JSON at path "rev" in "rev"
    When I make a POST request to "session"
      """
      {"activity": "ddddddddddddddddddddddddddddddd1", "start": 1}
      """
    Then the response status should be 409
    When I make a POST request to "session"
      """
      {"activity": "ddddddddddddddddddddddddddddddd1", "start": 2}
      """
    Then the response status should be 409
    When I make a POST request to "session"
      """
      {"activity": "ddddddddddddddddddddddddddddddd1", "start": 3}
      """
    Then the response status should be 409
    When I make a POST request to "session"
      """
      {
        "activity": "ddddddddddddddddddddddddddddddd1",
        "start": 2,
        "end": 5
      }
      """
    #Then the response status should be 409
    # End started session to check against in later steps
    When I make a PUT request to "session/{{ id }}?rev={{ rev }}"
      """
      {
        "activity": "ddddddddddddddddddddddddddddddd1",
        "start": 2,
        "end": 5
      }
      """
    Then the response status should be 201
    When I make a POST request to "session"
      """
      {
        "activity": "ddddddddddddddddddddddddddddddd1",
        "start": 2,
        "end": 5
      }
      """
    Then the response status should be 409
    When I make a POST request to "session"
      """
      {
        "activity": "ddddddddddddddddddddddddddddddd1",
        "start": 3,
        "end": 7
      }
      """
    Then the response status should be 409
    When I make a POST request to "session"
      """
      {
        "activity": "ddddddddddddddddddddddddddddddd1",
        "start": 5,
        "end": 7
      }
      """
    Then the response status should be 409


  ### Bulk ###

  Scenario: Get multiple activities
    When I make a GET request to "bulk/activity"
    Then the response status should be 200

  Scenario: Get multiple activities with limit
    When I make a GET request to "bulk/activity?limit=2"
    Then the response status should be 200
    And the JSON array length at path "rows" should be 2

  Scenario: Get multiple activities with different ordering
    When I make a GET request to "bulk/activity"
    Then the response status should be 200
    Then the JSON at path "rows[0].id" should be "ddddddddddddddddddddddddddddddd1"
    When I make a GET request to "bulk/activity?descending=true"
    Then the response status should be 200
    Then the JSON at path "rows[-1].id" should be "ddddddddddddddddddddddddddddddd1"

  Scenario: Get multiple activities with documents
    When I make a GET request to "bulk/activity?include_docs=true"
    Then the response status should be 200
    And the JSON array length at path "rows" should be 4
    And the JSON at path "total_rows" should be 4
    And the JSON array length at path "rows[0].doc" should be 5
    And the JSON at path "rows[0].doc.tags" should be
    """
    [
        "tag1",
        "foo"
    ]
    """
    And the JSON at path "rows[0].doc.title" should be "activity 1"

  Scenario: Get multiple activities with any of the given tags
    When I make a GET request to "bulk/activity?tags=tag1,tag2&any_tag=true&include_docs=true"
    Then the response status should be 200
    And the JSON array length at path "rows" should be 2
    And the JSON at path "total_rows" should be 2
    And the JSON at path "rows[0].doc.tags" should be
    """
    [
        "tag1",
        "foo"
    ]
    """
    And the JSON at path "rows[1].doc.tags" should be
    """
    [
        "tag2",
        "foo"
    ]
    """

  Scenario: Get multiple activities with every of the given tags
    When I make a GET request to "bulk/activity?tags=tag2,foo&include_docs=true"
    Then the response status should be 200
    And the JSON array length at path "rows" should be 1
    And the JSON at path "total_rows" should be 1
    And the JSON at path "rows[0].doc.tags" should be
    """
    [
        "tag2",
        "foo"
    ]
    """

  Scenario: Get multiple activities with ordering
    When I make a GET request to "bulk/activity?descending=true"
    Then the response status should be 200

  Scenario: Get multiple activities with startkey and endkey
    When I make a GET request to "bulk/activity?startkey=ddddddddddddddddddddddddddddddd1&endkey=ddddddddddddddddddddddddddddddd3"
    Then the response status should be 200
    And the JSON at path "rows[0].id" should be "ddddddddddddddddddddddddddddddd1"
    And the JSON at path "rows[-1].id" should be "ddddddddddddddddddddddddddddddd3"

  Scenario: Get all sessions
    When I make a GET request to "bulk/session"
    Then the response status should be 200

  Scenario: Get sessions for the given activity
    When I make a GET request to "bulk/session?activity=ddddddddddddddddddddddddddddddd1"
    Then the response status should be 200

  Scenario: Get all tags (assigned to activities)
    When I make a GET request to "bulk/tag"
    Then the response status should be 200

  #TODO: SessionAPIBulkRequestHandler.get()



  ### Settings api ###
  @modifies
  Scenario: Create, get and update settings
    When I make a GET request to "settings"
    Then the response status should be 200
    And the "Content-Type" header should be "application/json; charset=UTF-8"
    When I make a PUT request to "settings"
      """
      {
        "numbers": [],
        "foo": "bar"
      }
      """
    Then the response status should be 201
    When I store the JSON at path "rev" in "rev"
    And I make a PUT request to "settings?rev={{ rev }}"
      """
      {
        "numbers": [42, 0]
      }
      """
    When I make a GET request to "settings"
    Then the response status should be 200
    And the JSON at path "numbers" should be
      """
      [42, 0]
      """


  ### Explicitly wrong requests ###

  @err
  Scenario: API request with wrong credentials
    Given I set BasicAuth username to "featuretester@example.com" and password to "wrong"
    When I make a GET request to "account"
    Then the response status should be 401

  @err
  Scenario: Get invalid URL
    When I make a GET request to "does-not-exist"
    Then the response status should be 404

  @err
  Scenario: Get activity with invalid id
    When I make a GET request to "activity/wrong-id"
    Then the response status should be 404

  @err
  Scenario: Get activity with valid but nonexistent id
    When I make a GET request to "activity/ddddddddddddddddddddddddddddddff"
    Then the response status should be 404

  @err
  @modifies
  Scenario: Create session with no start date
    When I make a POST request to "session"
      """
      {
        "activity": "ddddddddddddddddddddddddddddddd1",
        "end": 10
      }
      """
    Then the response status should be 400

  @err
  @modifies
  Scenario: Create session with null start date
    When I make a POST request to "session"
      """
      {
        "activity": "ddddddddddddddddddddddddddddddd1",
        "start": null,
        "end": 10
      }
      """
    Then the response status should be 400

  @err
  @modifies
  Scenario: Create session with wrong start date format
    When I make a POST request to "session"
      """
      {
        "activity": "ddddddddddddddddddddddddddddddd1",
        "start": "100"
      }
      """
    Then the response status should be 400

  @err
  @modifies
  Scenario: Create session with wrong end date format
    When I make a POST request to "session"
      """
      {
        "activity": "ddddddddddddddddddddddddddddddd1",
        "start": 100,
        "end": "200"
      }
      """
    Then the response status should be 400

  @err
  @modifies
  Scenario: Create session with null end date
    When I make a POST request to "session"
      """
      {
        "activity": "ddddddddddddddddddddddddddddddd1",
        "start": 100,
        "end": null
      }
      """
    Then the response status should be 400

  @err
  @modifies
  Scenario: Create session for missing activity
    When I make a POST request to "session"
      """
      {
        "activity": "dddddddddddddddddddddddddddddddz",
        "start": 100,
        "end": 200
      }
      """
    Then the response status should be 404

  @err
  @modifies
  Scenario: Update activity with wrong revision
    When I make a PUT request to "activity/ddddddddddddddddddddddddddddddd1?rev=1-c335089eb3dc7074ad6d64c4e3ebffff"
    """
      {"title": "Some title"}
    """
    Then the response status should be 409

  @err
  @modifies
  Scenario: Update activity with wrong revision format
    When I make a PUT request to "activity/ddddddddddddddddddddddddddddddd1?rev=z"
    """
      {"title": "Some title"}
    """
    Then the response status should be one of "400,409"

  @err
  @modifies
  Scenario: Delete activity with valid but nonexistent id
    When I make a DELETE request to "activity/ddddddddddddddddddddddddddddddff?rev=1-c335089eb3dc7074ad6d64c4e3ebffff"
    Then the response status should be 404

  @err
  @modifies
  Scenario: Delete activity with wrong revision
    When I make a DELETE request to "activity/ddddddddddddddddddddddddddddddd1?rev=1-c335089eb3dc7074ad6d64c4e3ebffff"
    Then the response status should be 409

  @err
  @modifies
  Scenario: Delete activity with wrong revision format
    When I make a DELETE request to "activity/ddddddddddddddddddddddddddddddd1?rev=z"
    Then the response status should be one of "400,409"

  @err
  @modifies
  Scenario: Update session with a rev and valid but nonexistent id
    When I make a PUT request to "session/eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeff?rev=1-c335089eb3dc7074ad6d64c4e3ebffff"
    """
      {"activity": "ddddddddddddddddddddddddddddddd1", "start": 1, "end": 2}
    """
    Then the response status should be 404

  @err
  @modifies
  Scenario: Update session with wrong revision
    When I make a PUT request to "session/eeeeeeeeeeeeeeeeeeeeeeeeeeeeee12?rev=1-c335089eb3dc7074ad6d64c4e3ebffff"
    """
      {"activity": "ddddddddddddddddddddddddddddddd1", "start": 1, "end": 2}
    """
    Then the response status should be 409

  @err
  @modifies
  Scenario: Update session with wrong revision format
    When I make a PUT request to "session/eeeeeeeeeeeeeeeeeeeeeeeeeeeeee12?rev=z"
    """
      {"activity": "ddddddddddddddddddddddddddddddd1", "start": 1, "end": 2}
    """
    Then the response status should be one of "400,409"

  @err
  @modifies
  Scenario: Delete session with wrong revision
    When I make a DELETE request to "session/eeeeeeeeeeeeeeeeeeeeeeeeeeeeee12?rev=1-c335089eb3dc7074ad6d64c4e3ebffff"
    Then the response status should be 409

  @err
  @modifies
  Scenario: Delete session with wrong revision format
    When I make a DELETE request to "session/eeeeeeeeeeeeeeeeeeeeeeeeeeeeee12?rev=z"
    Then the response status should be 400

  @err
  Scenario: Make a request supplying wrong authentification information
    Given I set BasicAuth username to "mobster@example.com" and password to "featuretester"
    When I make a GET request to "session/eeeeeeeeeeeeeeeeeeeeeeeeeeeeee12"
    Then the response status should be 401

  @err
  Scenario: Make a request supplying username in wrong format
    Given I set BasicAuth username to "feature tester@example.com" and password to "featuretester"
    When I make a GET request to "session/eeeeeeeeeeeeeeeeeeeeeeeeeeeeee12"
    Then the response status should be 400

  @err
  Scenario: Get multiple activities with wrong limit format
    When I make a GET request to "bulk/activity?limit=z"
    Then the response status should be 400

  @err
  Scenario: Get multiple activities with negative limit
    When I make a GET request to "bulk/activity?limit=-2"
    Then the response status should be 400
